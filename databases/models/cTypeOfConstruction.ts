import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';

export const TypeOfConstructionFields = {
    id: 'id',
    value: 'value',
    contractorId: 'contractorId',
};
export const TypeOfConstruction = sequelize.define('cTypeOfConstruction',
    {

        [TypeOfConstructionFields.id]: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        [TypeOfConstructionFields.value]: {
            type: DataTypes.TEXT
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [TypeOfConstructionFields.contractorId]
            }
        ]
    }
);
