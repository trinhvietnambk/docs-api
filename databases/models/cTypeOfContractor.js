"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
exports.TypeOfContractorFields = {
    id: 'id',
    value: 'value',
    contractorId: 'contractorId',
};
exports.TypeOfContractor = _Base_1.sequelize.define('cTypeOfContractor', {
    [exports.TypeOfContractorFields.id]: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    [exports.TypeOfContractorFields.value]: {
        type: sequelize_1.DataTypes.TEXT
    }
}, {
    indexes: [
        {
            unique: false,
            fields: [exports.TypeOfContractorFields.contractorId]
        }
    ]
});
//# sourceMappingURL=cTypeOfContractor.js.map