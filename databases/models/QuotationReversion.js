"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
const Quotation_1 = require("./Quotation");
exports.QuotationReversionFields = {
    id: 'id',
    createdByUserId: 'createdByUserId',
    fromId: 'fromId',
    fromType: 'fromType',
    toId: 'toId',
    toType: 'toType',
    contentLetter: 'contentLetter',
    timeSendBidding: 'timeSendBidding',
    note: 'note',
    isSentBidding: 'isSentBidding',
    biddingStatus: 'biddingStatus',
    totalMoney: 'totalMoney',
    currency: 'currency',
    totalTime: 'totalTime',
    unitTime: 'unitTime',
    version: 'version',
    projectId: 'projectId',
    receiverId: 'receiverId',
    documents: 'documents',
    works: 'works',
    materials: 'materials',
    alternativeBid: 'alternativeBid',
    totalMoneyWorks: 'totalMoneyWorks',
    totalTimeWorks: 'totalTimeWorks',
    totalMoneyMaterials: 'totalMoneyMaterials',
    originId: 'originId',
};
exports.QuotationReversionAssociations = {};
exports.QuotationReversion = _Base_1.sequelize.define('quotationReversion', {
    [exports.QuotationReversionFields.id]: {
        type: sequelize_1.DataTypes.CHAR(50),
        primaryKey: true
    },
    [exports.QuotationReversionFields.createdByUserId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.QuotationReversionFields.fromId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.QuotationReversionFields.toId]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.QuotationReversionFields.fromType]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.QuotationReversionFields.toType]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.QuotationReversionFields.contentLetter]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.QuotationReversionFields.timeSendBidding]: {
        type: sequelize_1.DataTypes.DATE
    },
    [exports.QuotationReversionFields.note]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.QuotationReversionFields.isSentBidding]: {
        type: sequelize_1.DataTypes.BOOLEAN
    },
    [exports.QuotationReversionFields.biddingStatus]: {
        type: sequelize_1.DataTypes.CHAR(20)
    },
    [exports.QuotationReversionFields.totalMoney]: {
        type: sequelize_1.DataTypes.BIGINT
    },
    [exports.QuotationReversionFields.currency]: {
        type: sequelize_1.DataTypes.CHAR(20)
    },
    [exports.QuotationReversionFields.totalTime]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [exports.QuotationReversionFields.unitTime]: {
        type: sequelize_1.DataTypes.CHAR(20)
    },
    [exports.QuotationReversionFields.version]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [exports.QuotationReversionFields.projectId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.QuotationReversionFields.receiverId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.QuotationReversionFields.documents]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.QuotationReversionFields.documents, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.QuotationReversionFields.documents)) {
                return JSON.parse(this.getDataValue(exports.QuotationReversionFields.documents));
            }
        }
    },
    [exports.QuotationReversionFields.works]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.QuotationReversionFields.works, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.QuotationReversionFields.works)) {
                return JSON.parse(this.getDataValue(exports.QuotationReversionFields.works));
            }
        }
    },
    [exports.QuotationReversionFields.materials]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.QuotationReversionFields.materials, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.QuotationReversionFields.materials)) {
                return JSON.parse(this.getDataValue(exports.QuotationReversionFields.materials));
            }
        }
    },
    [exports.QuotationReversionFields.alternativeBid]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.QuotationReversionFields.alternativeBid, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.QuotationReversionFields.alternativeBid)) {
                return JSON.parse(this.getDataValue(exports.QuotationReversionFields.alternativeBid));
            }
        }
    },
    [exports.QuotationReversionFields.totalMoneyWorks]: {
        type: sequelize_1.DataTypes.BIGINT
    },
    [exports.QuotationReversionFields.totalTimeWorks]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [exports.QuotationReversionFields.totalMoneyMaterials]: {
        type: sequelize_1.DataTypes.BIGINT
    },
    [exports.QuotationReversionFields.originId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    }
}, {
    indexes: [
        {
            unique: false,
            fields: [
                Quotation_1.QuotationFields.fromId,
            ]
        },
        {
            unique: false,
            fields: [
                Quotation_1.QuotationFields.fromType,
            ]
        },
        {
            unique: false,
            fields: [
                Quotation_1.QuotationFields.toId,
            ]
        },
        {
            unique: false,
            fields: [
                Quotation_1.QuotationFields.toType,
            ]
        },
        {
            unique: false,
            fields: [
                Quotation_1.QuotationFields.receiverId,
            ]
        },
        {
            unique: false,
            fields: [
                exports.QuotationReversionFields.originId
            ]
        },
    ]
});
//# sourceMappingURL=QuotationReversion.js.map