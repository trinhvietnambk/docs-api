import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';

export const ActiveCodeFields = {
    id: 'id',
    code: 'code',
    type: 'type',
    email: 'email',
    extraInfo: 'extraInfo'
};
export const ActiveCodeType = {
    'INVITE_COWORKER': 'INVITE_COWOKER'
}
export const ActiveCodeAssociations = {};
export const ActiveCode = sequelize.define('activecode',
    {
        [ActiveCodeFields.id]: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        [ActiveCodeFields.code]: {
            type: DataTypes.CHAR(100)
        },
        [ActiveCodeFields.type]: {
            type: DataTypes.CHAR(100)
        },
        [ActiveCodeFields.email]: {
            type: DataTypes.CHAR(100)
        },
        [ActiveCodeFields.extraInfo]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(ActiveCodeFields.extraInfo, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(ActiveCodeFields.extraInfo)) {
                    return JSON.parse(this.getDataValue(ActiveCodeFields.extraInfo));
                }
            }
        }
    },
    {
        indexes: [
            {unique: true, fields: [ActiveCodeFields.code]},
            {unique: false, fields: [ActiveCodeFields.email]},
            {unique: false, fields: [ActiveCodeFields.type]}
        ]
    }
);
