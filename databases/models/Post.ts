import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';

export const PostFields = {
    id: 'id',
    ownerId: 'ownerId',
    ownerType: 'ownerType',
    content: 'content',
    images: 'images',
    videos: 'videos',
    audios: 'audios',
    files: 'files',
    createdByUserId: 'createdByUserId',
    numberOfLikes: 'numberOfLikes',
    numberOfComments: 'numberOfComments',
    visibility:'visibility'
};
export const PostAssociations = {};
export const Post = sequelize.define('post',
    {
        [PostFields.id]: {
            type: DataTypes.CHAR(50),
            primaryKey: true
        },
        [PostFields.ownerId]: {
            type: DataTypes.CHAR(100)
        },
        [PostFields.ownerType]: {
            type: DataTypes.CHAR(100)
        },
        [PostFields.content]: {
            type: DataTypes.TEXT
        },
        [PostFields.images]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(PostFields.images, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(PostFields.images)) {
                    return JSON.parse(this.getDataValue(PostFields.images));
                }
            }
        },
        [PostFields.videos]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(PostFields.videos, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(PostFields.videos)) {
                    return JSON.parse(this.getDataValue(PostFields.videos));
                }
            }
        },
        [PostFields.files]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(PostFields.files, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(PostFields.files)) {
                    return JSON.parse(this.getDataValue(PostFields.files));
                }
            }
        },
        [PostFields.audios]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(PostFields.audios, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(PostFields.audios)) {
                    return JSON.parse(this.getDataValue(PostFields.audios));
                }
            }
        },
        [PostFields.createdByUserId]: {
            type: DataTypes.CHAR(50)
        },
        [PostFields.numberOfLikes]: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
        },
        [PostFields.numberOfComments]: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
        },
        [PostFields.visibility]: {
            type: DataTypes.INTEGER
        },
    },
    {
        indexes: [
            {
                unique: false,
                fields: [
                    PostFields.ownerId,
                ]
            },
            {
                unique: false,
                fields: [
                    PostFields.ownerType
                ]
            },
            {
                method:'BTREE',
                fields: ['createdAt']
            },
            {
                method:'BTREE',
                fields: ['updatedAt']
            },
            {
                method:'BTREE',
                fields: [PostFields.visibility]
            }
        ]
    }
);
