import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';

export const CoWorkerFields = {
    id: 'id',
    companyId: 'companyId',
    memberId: 'memberId',
    role: 'role',
    status: 'status'
};
export const CoWorkerAssociations = {};
export const CoWorker = sequelize.define('coworker',
    {
        [CoWorkerFields.id]: {
            type: DataTypes.CHAR(50),
            primaryKey: true
        },
        [CoWorkerFields.companyId]: {
            type: DataTypes.CHAR(100)
        },
        [CoWorkerFields.memberId]: {
            type: DataTypes.CHAR(100)
        },
        [CoWorkerFields.role]: {
            type: DataTypes.CHAR(100)
        },
        [CoWorkerFields.status]: {
            type: DataTypes.CHAR(100),
            defaultValue: 'active'
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [CoWorkerFields.companyId]
            },
            {
                unique: false,
                fields: [CoWorkerFields.status]
            },
            {
                unique: false,
                fields: [CoWorkerFields.role]
            }
        ]
    }
);
