import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';
export const SubBusinessFields = {
    id: 'id',
    value: 'value',
    contractorId: 'contractorId',
};
export const SubBusiness = sequelize.define('cSubBusiness',
    {

        [SubBusinessFields.id]: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        [SubBusinessFields.value]: {
            type: DataTypes.TEXT
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [SubBusinessFields.contractorId]
            }
        ]
    }
);
