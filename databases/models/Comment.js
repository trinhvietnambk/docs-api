"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
const Post_1 = require("./Post");
exports.CommentFields = {
    id: 'id',
    subjectId: 'subjectId',
    subjectType: 'subjectType',
    objectId: 'objectId',
    objectType: 'objectType',
    createdByUserId: 'createdByUserId',
    content: 'content',
    images: 'images',
    videos: 'videos',
    audios: 'audios',
    files: 'files',
    numberOfLikes: 'numberOfLikes',
    numberOfComments: 'numberOfComments'
};
exports.CommentAssociations = {};
exports.Comment = _Base_1.sequelize.define('comment', {
    [exports.CommentFields.id]: {
        type: sequelize_1.DataTypes.CHAR(50),
        primaryKey: true
    },
    [exports.CommentFields.subjectId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.CommentFields.subjectType]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.CommentFields.objectId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.CommentFields.objectType]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.CommentFields.createdByUserId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    }, [Post_1.PostFields.content]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [Post_1.PostFields.content]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.CommentFields.images]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.CommentFields.images, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.CommentFields.images)) {
                return JSON.parse(this.getDataValue(exports.CommentFields.images));
            }
        }
    },
    [exports.CommentFields.videos]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.CommentFields.videos, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.CommentFields.videos)) {
                return JSON.parse(this.getDataValue(exports.CommentFields.videos));
            }
        }
    },
    [exports.CommentFields.files]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.CommentFields.files, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.CommentFields.files)) {
                return JSON.parse(this.getDataValue(exports.CommentFields.files));
            }
        }
    },
    [exports.CommentFields.audios]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.CommentFields.audios, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.CommentFields.audios)) {
                return JSON.parse(this.getDataValue(exports.CommentFields.audios));
            }
        }
    },
    [exports.CommentFields.numberOfLikes]: {
        type: sequelize_1.DataTypes.INTEGER,
        defaultValue: 0,
    },
    [exports.CommentFields.numberOfComments]: {
        type: sequelize_1.DataTypes.INTEGER,
        defaultValue: 0,
    }
}, {
    indexes: [
        { unique: false, fields: [exports.CommentFields.objectId] },
        { unique: false, fields: [exports.CommentFields.subjectId] },
        {
            method: 'BTREE',
            fields: ['createdAt', 'updatedAt']
        }
    ]
});
//# sourceMappingURL=Comment.js.map