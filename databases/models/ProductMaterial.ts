import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';

export const ProductMaterialFields = {
    id: 'id',
    value: 'value',
    productId: 'productId',
};
export const ProductMaterial = sequelize.define('productMaterial',
    {

        [ProductMaterialFields.id]: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        [ProductMaterialFields.value]: {
            type: DataTypes.CHAR(50)
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [ProductMaterialFields.productId]
            }
        ]
    }
);
