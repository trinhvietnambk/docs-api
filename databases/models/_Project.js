"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
const TextUtil_1 = require("../utities/TextUtil");
exports.ProjectFields = {
    id: 'id',
    name: 'name',
    about: 'about',
    info: 'info',
    noSignName: 'noSignName',
    createdByUserId: 'createdByUserId',
    supportorId: 'supportorId',
};
exports.ProjectAsociations = {
    createdByUser: 'createdByUser'
};
exports.Project = _Base_1.sequelize.define('project', {
    [exports.ProjectFields.name]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.ProjectFields.about]: {
        type: sequelize_1.DataTypes.TEXT,
    },
    [exports.ProjectFields.info]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            console.log('aaaaaaaaaaa ', val);
            this.setDataValue(exports.ProjectFields.info, JSON.stringify(val));
        },
        get() {
            console.log('bbbbbbbbbbbbbbbbbbb ', this.getDataValue(exports.ProjectFields.noSignName));
            if (this.getDataValue(exports.ProjectFields.info)) {
                return JSON.parse(this.getDataValue(exports.ProjectFields.info));
            }
        }
    },
    [exports.ProjectFields.noSignName]: {
        type: sequelize_1.DataTypes.TEXT
    }
});
exports.Project.beforeSave((project, options) => {
    if (project[exports.ProjectFields.name]) {
        project.noSignName = TextUtil_1.TextUtil.changeAlias(project[exports.ProjectFields.name]);
    }
});
exports.Project.beforeBulkUpdate((query, instanse) => {
    if (query.attributes[exports.ProjectFields.name]) {
        query.fields.push(exports.ProjectFields.noSignName);
        query.attributes[exports.ProjectFields.noSignName] = TextUtil_1.TextUtil.changeAlias(query.attributes[exports.ProjectFields.name]);
    }
});
//# sourceMappingURL=_Project.js.map