import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';

export const ProductTagFields = {
    id: 'id',
    value: 'value',
    productId: 'productId',
};
export const ProductTag = sequelize.define('productTag',
    {

        [ProductTagFields.id]: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        [ProductTagFields.value]: {
            type: DataTypes.CHAR(100)
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [ProductTagFields.productId]
            }
        ]
    }
);
