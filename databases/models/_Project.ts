import {sequelize} from "./_Base";
import {DataTypes, Op} from 'sequelize';
import {TextUtil} from "../utities/TextUtil";

export const ProjectFields = {
    id: 'id',
    name: 'name',
    about: 'about',
    info: 'info',
    noSignName: 'noSignName',
    createdByUserId: 'createdByUserId',
    supportorId: 'supportorId',
};
export const ProjectAsociations = {
    createdByUser: 'createdByUser'
}
export const Project = sequelize.define('project', {
        [ProjectFields.name]: {
            type: DataTypes.TEXT
        },
        [ProjectFields.about]: {
            type: DataTypes.TEXT,
        },
        [ProjectFields.info]: {
            type: DataTypes.TEXT,
            set(val) {
                console.log('aaaaaaaaaaa ', val);
                this.setDataValue(ProjectFields.info, JSON.stringify(val))
            },
            get() {
                console.log('bbbbbbbbbbbbbbbbbbb ', this.getDataValue(ProjectFields.noSignName));
                if (this.getDataValue(ProjectFields.info)) {
                    return JSON.parse(this.getDataValue(ProjectFields.info))
                }
            }
        },
        [ProjectFields.noSignName]: {
            type: DataTypes.TEXT
        }
    }
);

Project.beforeSave((project, options) => {
    if (project[ProjectFields.name]) {
        project.noSignName = TextUtil.changeAlias(project[ProjectFields.name]);
    }
});
Project.beforeBulkUpdate((query, instanse) => {
    if (query.attributes[ProjectFields.name]) {
        query.fields.push(ProjectFields.noSignName);
        query.attributes[ProjectFields.noSignName] = TextUtil.changeAlias(query.attributes[ProjectFields.name]);
    }
});