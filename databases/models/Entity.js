"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
exports.EntityFields = {
    id: 'id',
    type: 'type'
};
exports.EntityAssociations = {};
exports.Entity = _Base_1.sequelize.define('entity', {
    [exports.EntityFields.id]: {
        type: sequelize_1.DataTypes.CHAR(50),
        primaryKey: true
    },
    [exports.EntityFields.type]: {
        type: sequelize_1.DataTypes.CHAR(50)
    }
}, {
    indexes: [
        {
            unique: false,
            fields: [exports.EntityFields.type]
        }
    ]
});
//# sourceMappingURL=Entity.js.map