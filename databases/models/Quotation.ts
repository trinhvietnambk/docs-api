import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';
import {BIDDING_STATUS, RECEIVE_STATUS, SENDING_STATUS} from "../../constants/database/Biddings";
import {CURRENCY_UNIT_VND, TIME_UNIT} from "../../constants/database/Common";
import {MathHelper} from "../../utities/MathHelper";
import {ProjectFields} from "./Project";
import {FavoriteItemFields} from "./FavoriteItem";

export const QuotationFields = {
    id: 'id',
    createdByUserId: 'createdByUserId',
    fromId: 'fromId',
    fromType: 'fromType',
    toId: 'toId', // Đấu thầu cho thực thể nào, toId ở đây chính là ID hạng mục đấu thầu
    toType: 'toType', //hiện tại là hạng mục đấu thầu
    contentLetter: 'contentLetter',
    timeSendBidding: 'timeSendBidding',
    note: 'note',
    isSentBidding: 'isSentBidding',
    biddingStatus: 'biddingStatus',
    feelingStatus: 'feelingStatus',
    totalMoney: 'totalMoney',
    currency: 'currency',
    totalTime: 'totalTime',
    unitTime: 'unitTime',
    version: 'version',
    projectId: 'projectId',
    receiverId: 'receiverId',
    documents: 'documents',
    works: 'works',
    materials: 'materials',
    alternativeBid: 'alternativeBid',
    totalMoneyWorks: 'totalMoneyWorks',
    totalTimeWorks: 'totalTimeWorks',
    totalMoneyMaterials: 'totalMoneyMaterials',
};
export const QuotationAssociations = {};
export const Quotation = sequelize.define('quotation',
    {
        [QuotationFields.id]: {
            type: DataTypes.CHAR(50),
            primaryKey: true
        },
        [QuotationFields.createdByUserId]: {
            type: DataTypes.CHAR(50)
        },
        [QuotationFields.fromId]: {
            type: DataTypes.CHAR(50)
        },
        [QuotationFields.toId]: {
            type: DataTypes.CHAR(100)
        },
        [QuotationFields.fromType]: {
            type: DataTypes.CHAR(100)
        },
        [QuotationFields.toType]: {
            type: DataTypes.CHAR(100)
        },
        [QuotationFields.contentLetter]: {
            type: DataTypes.TEXT
        },
        [QuotationFields.timeSendBidding]: {
            type: DataTypes.DATE
        },
        [QuotationFields.note]: {
            type: DataTypes.TEXT
        },
        [QuotationFields.isSentBidding]: {
            type: DataTypes.BOOLEAN
        },
        [QuotationFields.biddingStatus]: {
            type: DataTypes.CHAR(20)
        },
        [QuotationFields.totalMoney]: {
            type: DataTypes.BIGINT
        },
        [QuotationFields.currency]: {
            type: DataTypes.CHAR(20)
        },
        [QuotationFields.totalTime]: {
            type: DataTypes.INTEGER
        },
        [QuotationFields.unitTime]: {
            type: DataTypes.CHAR(20)
        },
        [QuotationFields.version]: {
            type: DataTypes.INTEGER
        },
        [QuotationFields.projectId]: {
            type: DataTypes.CHAR(50)
        },
        [QuotationFields.receiverId]: {
            type: DataTypes.CHAR(50)
        },
        [QuotationFields.feelingStatus]: {
            type: DataTypes.CHAR(50)
        },
        [QuotationFields.documents]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(QuotationFields.documents, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(QuotationFields.documents)) {
                    return JSON.parse(this.getDataValue(QuotationFields.documents));
                }
            }
        },
        [QuotationFields.works]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(QuotationFields.works, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(QuotationFields.works)) {
                    return JSON.parse(this.getDataValue(QuotationFields.works));
                }
            }
        },
        [QuotationFields.materials]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(QuotationFields.materials, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(QuotationFields.materials)) {
                    return JSON.parse(this.getDataValue(QuotationFields.materials));
                }
            }
        },
        [QuotationFields.alternativeBid]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(QuotationFields.alternativeBid, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(QuotationFields.alternativeBid)) {
                    return JSON.parse(this.getDataValue(QuotationFields.alternativeBid));
                }
            }
        },
        [QuotationFields.totalMoneyWorks]: {
            type: DataTypes.BIGINT
        },
        [QuotationFields.totalTimeWorks]: {
            type: DataTypes.INTEGER
        },
        [QuotationFields.totalMoneyMaterials]: {
            type: DataTypes.BIGINT
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [
                    QuotationFields.fromId,

                ]
            },
            {
                unique: false,
                fields: [

                    QuotationFields.fromType,

                ]
            },
            {
                unique: false,
                fields: [

                    QuotationFields.toId,

                ]
            },
            {
                unique: false,
                fields: [

                    QuotationFields.toType,
                ]
            },
            {
                unique: false,
                fields: [

                    QuotationFields.receiverId,
                ]
            },
            {
                method:'BTREE',
                fields: ['createdAt']
            },
            {
                method:'BTREE',
                fields: ['updatedAt']
            }
        ]
    }
);