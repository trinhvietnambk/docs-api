"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
const TextHelper_1 = require("../../utities/TextHelper");
exports.ContractorFields = {
    id: 'id',
    name: "name",
    noSignName: "noSignName",
    type: "type",
    wallImage: "wallImage",
    logo: "logo",
    address: "address",
    city: "city",
    country: "country",
    district: "district",
    phone: "phone",
    fax: "fax",
    website: "website",
    email: "email",
    taxCode: "taxCode",
    origination: "origination",
    foundedYear: "foundedYear",
    description: "description",
    isHouseLinkMember: "isHouseLinkMember",
    isVerifiedLicense: "isVerifiedLicense",
    totalRanking: "totalRanking",
    typeOfCompany: "typeOfCompany",
    qualityManagement: "qualityManagement",
    language: "language",
    numberOfExpProject: "numberOfExpProject",
    numberOfEmployees: "numberOfEmployees",
    numberOfBiddedProject: "numberOfBiddedProject",
    numberOfRating: "numOfRating",
    revenue: "revenue",
    unitRevenue: "unitRevenue",
    prizes: "prizes",
    price: "price",
    unitPrice: "unitPrice",
    listVideos: "listVideos",
    documents: "documents",
    finances: "finances",
    imageFinances: "imageFinances",
    affiliations: "affiliations",
    createdByUserId: "createdByUserId",
    approved: "approved",
    verifyDocuments: "verifyDocuments",
    services: 'services',
    typeOfContractor: "typeOfContractor",
    typeOfConstruction: "typeOfConstruction",
    typeOfProject: "typeOfProject",
    typeOfBusiness: "typeOfBusiness",
    subBusiness: "subBusiness",
    mainMarkets: "mainMarkets",
    //counter
    numberOfFollower: 'numberOfLikes',
    numberOfLikes: 'numberOfLikes',
    houselinkRanking: "houselinkRanking",
    projectExperiences: "projectExperiences",
    products: "products",
    contacts: "contacts",
    ratingList: "ratingList",
    createdByUser: "createdByUser"
};
exports.ContractorAssociations = {};
exports.Contractor = _Base_1.sequelize.define('contractor', {
    [exports.ContractorFields.id]: {
        type: sequelize_1.DataTypes.CHAR(50),
        primaryKey: true
    },
    [exports.ContractorFields.name]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.ContractorFields.noSignName]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.ContractorFields.type]: {
        type: sequelize_1.DataTypes.CHAR(10)
    },
    [exports.ContractorFields.wallImage]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.ContractorFields.logo]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.ContractorFields.address]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.ContractorFields.city]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.ContractorFields.country]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.ContractorFields.district]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.ContractorFields.phone]: {
        type: sequelize_1.DataTypes.CHAR(30)
    },
    [exports.ContractorFields.fax]: {
        type: sequelize_1.DataTypes.CHAR(30)
    },
    [exports.ContractorFields.website]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.ContractorFields.email]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.ContractorFields.taxCode]: {
        type: sequelize_1.DataTypes.CHAR(30)
    },
    [exports.ContractorFields.origination]: {
        type: sequelize_1.DataTypes.CHAR(30)
    },
    [exports.ContractorFields.foundedYear]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [exports.ContractorFields.description]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.ContractorFields.isHouseLinkMember]: {
        type: sequelize_1.DataTypes.BOOLEAN
    },
    [exports.ContractorFields.isVerifiedLicense]: {
        type: sequelize_1.DataTypes.BOOLEAN
    },
    [exports.ContractorFields.totalRanking]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [exports.ContractorFields.houselinkRanking]: {
        type: sequelize_1.DataTypes.TEXT
    },
    /*Start field map to table
    [ContractorFields.typeOfBusiness]: {
        type: DataTypes.TEXT
    },
    [ContractorFields.subBusiness]: {
        type: DataTypes.TEXT
    },
    [ContractorFields.typeOfContractor]: {
        type: DataTypes.TEXT
    },
    [ContractorFields.typeOfConstruction]: {
        type: DataTypes.TEXT
    },
    [ContractorFields.typeOfProject]: {
        type: DataTypes.TEXT
    },
    [ContractorFields.mainMarkets]: {
        type: DataTypes.TEXT
    },
    End field map to table*/
    [exports.ContractorFields.typeOfCompany]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.ContractorFields.qualityManagement]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.ContractorFields.language]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.ContractorFields.numberOfExpProject]: {
        type: sequelize_1.DataTypes.INTEGER,
        defaultValue: 0
    },
    [exports.ContractorFields.numberOfEmployees]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.ContractorFields.numberOfBiddedProject]: {
        type: sequelize_1.DataTypes.INTEGER,
        defaultValue: 0
    },
    [exports.ContractorFields.numberOfRating]: {
        type: sequelize_1.DataTypes.INTEGER,
        defaultValue: 0
    },
    [exports.ContractorFields.revenue]: {
        type: sequelize_1.DataTypes.REAL
    },
    [exports.ContractorFields.unitRevenue]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.ContractorFields.price]: {
        type: sequelize_1.DataTypes.REAL
    },
    [exports.ContractorFields.unitPrice]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.ContractorFields.prizes]: {
        type: sequelize_1.DataTypes.REAL
    },
    [exports.ContractorFields.services]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.ContractorFields.services, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.ContractorFields.services)) {
                return JSON.parse(this.getDataValue(exports.ContractorFields.services));
            }
        }
    },
    [exports.ContractorFields.houselinkRanking]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.ContractorFields.houselinkRanking, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.ContractorFields.houselinkRanking)) {
                return JSON.parse(this.getDataValue(exports.ContractorFields.houselinkRanking));
            }
        }
    },
    [exports.ContractorFields.documents]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.ContractorFields.documents, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.ContractorFields.documents)) {
                return JSON.parse(this.getDataValue(exports.ContractorFields.documents));
            }
        }
    },
    [exports.ContractorFields.finances]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.ContractorFields.finances, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.ContractorFields.finances)) {
                return JSON.parse(this.getDataValue(exports.ContractorFields.finances));
            }
        }
    },
    [exports.ContractorFields.imageFinances]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.ContractorFields.imageFinances, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.ContractorFields.imageFinances)) {
                return JSON.parse(this.getDataValue(exports.ContractorFields.imageFinances));
            }
        }
    },
    [exports.ContractorFields.affiliations]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.ContractorFields.affiliations, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.ContractorFields.affiliations)) {
                return JSON.parse(this.getDataValue(exports.ContractorFields.affiliations));
            }
        }
    },
    [exports.ContractorFields.listVideos]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.ContractorFields.listVideos, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.ContractorFields.listVideos)) {
                return JSON.parse(this.getDataValue(exports.ContractorFields.listVideos));
            }
        }
    },
    [exports.ContractorFields.verifyDocuments]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.ContractorFields.verifyDocuments, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.ContractorFields.verifyDocuments)) {
                return JSON.parse(this.getDataValue(exports.ContractorFields.verifyDocuments));
            }
        }
    },
    [exports.ContractorFields.createdByUserId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.ContractorFields.approved]: {
        type: sequelize_1.DataTypes.BOOLEAN
    },
    [exports.ContractorFields.numberOfFollower]: {
        type: sequelize_1.DataTypes.INTEGER,
        defaultValue: 0,
    },
    [exports.ContractorFields.numberOfLikes]: {
        type: sequelize_1.DataTypes.INTEGER,
        defaultValue: 0,
    }
}, {
    indexes: [
        // add a FULLTEXT index
        { type: 'FULLTEXT', fields: [exports.ContractorFields.noSignName] },
        {
            method: 'BTREE',
            fields: [exports.ContractorFields.revenue]
        },
        {
            method: 'BTREE',
            fields: [
                exports.ContractorFields.price
            ]
        },
        {
            method: 'BTREE',
            fields: [
                exports.ContractorFields.foundedYear
            ]
        },
        {
            method: 'BTREE',
            fields: [
                exports.ContractorFields.totalRanking
            ]
        },
        {
            unique: false,
            fields: [exports.ContractorFields.approved]
        },
        {
            unique: false,
            fields: [exports.ContractorFields.createdByUserId]
        },
        {
            unique: false,
            fields: [
                exports.ContractorFields.typeOfCompany
            ]
        },
        {
            unique: false,
            fields: [
                exports.ContractorFields.origination
            ]
        },
        {
            unique: false,
            fields: [
                exports.ContractorFields.country
            ]
        },
        {
            unique: false,
            fields: [
                exports.ContractorFields.city,
                exports.ContractorFields.district
            ]
        },
        {
            unique: false,
            fields: [
                exports.ContractorFields.district
            ]
        },
        {
            unique: false,
            fields: [exports.ContractorFields.taxCode]
        },
        {
            method: 'BTREE',
            fields: ['createdAt']
        },
        {
            method: 'BTREE',
            fields: ['updatedAt']
        }
    ]
});
exports.Contractor.beforeSave((Contractor, options) => {
    if (Contractor[exports.ContractorFields.name]) {
        Contractor.noSignName = TextHelper_1.TextHelper.noSignViStringAndToLowerCase(Contractor[exports.ContractorFields.name]);
    }
});
exports.Contractor.beforeBulkUpdate((query, instanse) => {
    if (query.attributes[exports.ContractorFields.name]) {
        query.fields.push(exports.ContractorFields.noSignName);
        query.attributes[exports.ContractorFields.noSignName] = TextHelper_1.TextHelper.noSignViStringAndToLowerCase(query.attributes[exports.ContractorFields.name]);
    }
});
//# sourceMappingURL=Contractor.js.map