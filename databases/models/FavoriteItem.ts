import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';

export const FavoriteItemFields = {
    id: 'id',
    name: 'name',
    price: 'price',
    currency: 'currency',
    contractorId: 'contractorId',
};
export const FavoriteItemAssociations = {};
export const FavoriteItem = sequelize.define('activecode',
    {
        [FavoriteItemFields.id]: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        [FavoriteItemFields.name]: {
            type: DataTypes.CHAR(100)
        },
        [FavoriteItemFields.price]: {
            type: DataTypes.BIGINT
        },
        [FavoriteItemFields.currency]: {
            type: DataTypes.CHAR(50)
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [FavoriteItemFields.contractorId]
            }
        ]
    }
);
