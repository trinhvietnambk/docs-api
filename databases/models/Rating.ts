import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';
import {FavoriteItemFields} from "./FavoriteItem";

export const RatingFields = {
    id: 'id',
    subjectId: 'subjectId',
    subjectType: 'subjectType',
    objectId: 'objectId',
    objectType: 'objectType',
    content: 'content',
    star: 'star',
    createdByUserId: 'createdByUserId',
    role: 'role',
};
export const RatingAssociations = {};
export const Rating = sequelize.define('rating',
    {
        [RatingFields.id]: {
            type: DataTypes.CHAR(50),
            primaryKey: true
        },
        [RatingFields.subjectId]: {
            type: DataTypes.CHAR(100)
        },
        [RatingFields.subjectType]: {
            type: DataTypes.CHAR(100)
        },
        [RatingFields.objectId]: {
            type: DataTypes.CHAR(100)
        },
        [RatingFields.objectType]: {
            type: DataTypes.CHAR(100)
        },
        [RatingFields.content]: {
            type: DataTypes.TEXT
        },
        [RatingFields.star]: {
            type: DataTypes.INTEGER
        },
        [RatingFields.createdByUserId]: {
            type: DataTypes.CHAR(50)
        },
        [RatingFields.role]: {
            type: DataTypes.CHAR(50)
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [
                    RatingFields.objectId,
                ]
            },
            {
                unique: false,
                fields: [
                    RatingFields.objectType
                ]
            },
            {
                method:'BTREE',
                fields: ['createdAt']
            },
            {
                method:'BTREE',
                fields: ['updatedAt']
            }
        ]
    }
);
