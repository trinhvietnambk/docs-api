"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
exports.LocationFields = {
    id: 'id',
    country: 'country',
    province: 'province',
    district: 'district',
    village: 'village',
};
exports.LocationAssociations = {};
exports.Location = _Base_1.sequelize.define('Location', {
    [exports.LocationFields.id]: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    [exports.LocationFields.country]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.LocationFields.province]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.LocationFields.district]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.LocationFields.village]: {
        type: sequelize_1.DataTypes.CHAR(50)
    }
}, {
    indexes: [
        {
            unique: false,
            fields: [
                exports.LocationFields.country,
            ]
        },
        {
            unique: false,
            fields: [
                exports.LocationFields.province,
            ]
        },
        {
            unique: false,
            fields: [
                exports.LocationFields.district,
            ]
        },
        {
            unique: false,
            fields: [
                exports.LocationFields.village,
            ]
        }
    ]
});
async function initDataLocations() {
    var fs = require('fs');
    var tree = JSON.parse(fs.readFileSync(__dirname + '/../../data/data-tinh-thanh/tree.json'));
    var tinhs = Object.values(tree);
    async function processTinh(tinh) {
        var nameProvince = tinh['name_with_type'];
        var districts = Object.values(tinh['quan-huyen']);
        for (let district of districts) {
            console.log('AAAAAAAAAAAAAAAAA');
            await processDistrict(nameProvince, district);
        }
    }
    async function processDistrict(nameProvince, district) {
        var nameDistrict = district['name_with_type'];
        var villages = Object.values(district['xa-phuong']);
        // var array = villages.map(village=>{
        //    return processVillage(nameProvince, nameDistrict, village);
        // });
        // await Promise.all(array);
        console.log('11111111111111');
        for (const village of villages) {
            await processVillage(nameProvince, nameDistrict, village);
        }
        console.log(2222222);
    }
    var counter = 0;
    async function processVillage(nameProvince, nameDistrict, village) {
        var nameVillage = village['name_with_type'];
        console.log(nameVillage, nameDistrict, nameProvince);
        counter++;
        console.log(counter);
        await exports.Location.create({
            country: 'Việt Nam',
            district: nameDistrict,
            province: nameProvince,
            village: nameVillage
        });
    }
    exports.Location.sync({ force: true }).then(async () => {
        for (let tinh of tinhs) {
            await processTinh(tinh);
        }
        // var locations = await Location.findAll({
        //     attributes: [[sequelize.fn('DISTINCT', sequelize.col('village')), 'village']],
        //     where: {
        //         district: 'Huyện Quảng Xương'
        //     }
        // });
        // var villages = locations.map(l => {
        //     return l.village;
        // }).sort((a, b) => {
        //     return a.localeCompare(b);
        // });
        // // console.log(villages);
        // var record = await Location.findById(1);
        // console.log(record.toJSON());
        // return villages;
    });
}
// initDataLocations();
//# sourceMappingURL=Location.js.map