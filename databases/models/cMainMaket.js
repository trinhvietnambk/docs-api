"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
exports.MainMaketFields = {
    id: 'id',
    value: 'value',
    contractorId: 'contractorId',
};
exports.MainMaket = _Base_1.sequelize.define('cMainMaket', {
    [exports.MainMaketFields.id]: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    [exports.MainMaketFields.value]: {
        type: sequelize_1.DataTypes.TEXT
    }
});
//# sourceMappingURL=cMainMaket.js.map