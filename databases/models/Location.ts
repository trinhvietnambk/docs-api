import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';

export const LocationFields = {
    id: 'id',
    country: 'country',
    province: 'province',
    district: 'district',
    village: 'village',
};
export const LocationAssociations = {};
export const Location = sequelize.define('Location',
    {
        [LocationFields.id]: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        [LocationFields.country]: {
            type: DataTypes.CHAR(50)
        },
        [LocationFields.province]: {
            type: DataTypes.CHAR(50)
        },
        [LocationFields.district]: {
            type: DataTypes.CHAR(50)
        },
        [LocationFields.village]: {
            type: DataTypes.CHAR(50)
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [
                    LocationFields.country,
                ]
            },
            {
                unique: false,
                fields: [
                    LocationFields.province,
                ]
            },
            {
                unique: false,
                fields: [
                    LocationFields.district,
                ]
            },
            {
                unique: false,
                fields: [
                    LocationFields.village,
                ]
            }
        ]
    }
);

async function initDataLocations() {
    var fs = require('fs');
    var tree = JSON.parse(fs.readFileSync(__dirname + '/../../data/data-tinh-thanh/tree.json'));
    var tinhs = Object.values(tree);

    async function processTinh(tinh) {
        var nameProvince = tinh['name_with_type'];
        var districts = Object.values(tinh['quan-huyen']);
        for (let district of districts) {
            console.log('AAAAAAAAAAAAAAAAA');
            await processDistrict(nameProvince, district);
        }
    }

    async function processDistrict(nameProvince, district) {
        var nameDistrict = district['name_with_type'];
        var villages = Object.values(district['xa-phuong']);
        // var array = villages.map(village=>{
        //    return processVillage(nameProvince, nameDistrict, village);
        // });
        // await Promise.all(array);
        console.log('11111111111111');
        for (const village of villages) {
            await processVillage(nameProvince, nameDistrict, village);
        }
        console.log(2222222);
    }

    var counter = 0;

    async function processVillage(nameProvince, nameDistrict, village) {
        var nameVillage = village['name_with_type'];
        console.log(nameVillage, nameDistrict, nameProvince);
        counter++;
        console.log(counter);
        await Location.create({
            country: 'Việt Nam',
            district: nameDistrict,
            province: nameProvince,
            village: nameVillage
        });
    }

    Location.sync({force: true}).then(async () => {
        for (let tinh of tinhs) {
            await processTinh(tinh);
        }

        // var locations = await Location.findAll({
        //     attributes: [[sequelize.fn('DISTINCT', sequelize.col('village')), 'village']],
        //     where: {
        //         district: 'Huyện Quảng Xương'
        //     }
        // });
        // var villages = locations.map(l => {
        //     return l.village;
        // }).sort((a, b) => {
        //     return a.localeCompare(b);
        // });
        // // console.log(villages);
        // var record = await Location.findById(1);
        // console.log(record.toJSON());
        // return villages;
    });
}

// initDataLocations();