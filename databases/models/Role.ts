import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';
import {FavoriteItemFields} from "./FavoriteItem";

export const RoleFields = {
    id: 'id',
    subjectId: 'subjectId',
    subjectType: 'subjectType',
    objectId: 'objectId',
    objectType: 'objectType',
    role: 'role'
};
export const RoleValues = {
    admin: 'admin',
    member: 'member',
    createBiddingCategory: 'createBiddingCategory'
};
export const RoleAssociations = {};
export const Role = sequelize.define('role',
    {
        [RoleFields.subjectId]: {
            type: DataTypes.CHAR(50)
        },
        [RoleFields.subjectType]: {
            type: DataTypes.CHAR(50)
        },
        [RoleFields.objectId]: {
            type: DataTypes.CHAR(50),
        },
        [RoleFields.objectType]: {
            type: DataTypes.CHAR(50)
        },
        [RoleFields.role]: {
            type: DataTypes.CHAR(50)
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [
                    RoleFields.subjectId,

                ]
            },
            {
                unique: false,
                fields: [

                    RoleFields.subjectType,

                ]
            },
            {
                unique: false,
                fields: [

                    RoleFields.objectId,

                ]
            },
            {
                unique: false,
                fields: [

                    RoleFields.objectType,

                ]
            },
            {
                unique: false,
                fields: [

                    RoleFields.role,
                ]
            }
        ]
    }
);
