import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';
import {TextHelper} from "../../utities/TextHelper";
import {ProjectFields} from "./Project";

export const ProjectExperience = sequelize.define('projectExperience',
    {
        [ProjectFields.id]: {
            type: DataTypes.CHAR(50),
            primaryKey: true
        },
        [ProjectFields.name]: {
            type: DataTypes.TEXT
        },
        [ProjectFields.description]: {
            type: DataTypes.TEXT
        },
        [ProjectFields.documents]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(ProjectFields.documents, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(ProjectFields.documents)) {
                    return JSON.parse(this.getDataValue(ProjectFields.documents));
                }
            }
        },
        [ProjectFields.listImages]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(ProjectFields.listImages, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(ProjectFields.listImages)) {
                    return JSON.parse(this.getDataValue(ProjectFields.listImages));
                }
            }
        },
        [ProjectFields.listVideos]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(ProjectFields.listVideos, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(ProjectFields.listVideos)) {
                    return JSON.parse(this.getDataValue(ProjectFields.listVideos));
                }
            }
        },
        [ProjectFields.projectType]: {
            type: DataTypes.TEXT
        },
        [ProjectFields.projectSubType]: {
            type: DataTypes.TEXT
        },
        [ProjectFields.workType]: {
            type: DataTypes.TEXT
        },
        [ProjectFields.address]: {
            type: DataTypes.TEXT
        },
        [ProjectFields.village]: {
            type: DataTypes.TEXT
        },
        [ProjectFields.district]: {
            type: DataTypes.TEXT
        },
        [ProjectFields.city]: {
            type: DataTypes.TEXT
        },
        [ProjectFields.country]: {
            type: DataTypes.TEXT
        },
        [ProjectFields.minBudget]: {
            type: DataTypes.REAL
        },
        [ProjectFields.maxBudget]: {
            type: DataTypes.REAL
        },
        [ProjectFields.investmentType]: {
            type: DataTypes.TEXT
        },
        [ProjectFields.minConstructionArea]: {
            type: DataTypes.INTEGER
        },
        [ProjectFields.maxConstructionArea]: {
            type: DataTypes.INTEGER
        },
        [ProjectFields.minLandArea]: {
            type: DataTypes.INTEGER
        },
        [ProjectFields.maxLandArea]: {
            type: DataTypes.INTEGER
        },
        [ProjectFields.totalFloorArea]: {
            type: DataTypes.INTEGER
        },
        [ProjectFields.minStartTime]: {
            type: DataTypes.DATE
        },
        [ProjectFields.maxStartTime]: {
            type: DataTypes.DATE
        },
        [ProjectFields.minFinishTime]: {
            type: DataTypes.DATE
        },
        [ProjectFields.maxFinishTime]: {
            type: DataTypes.DATE
        },
        [ProjectFields.purpose]: {
            type: DataTypes.TEXT
        },
        [ProjectFields.noSignName]: {
            type: DataTypes.TEXT
        }
    },
    {
        indexes: [
            // add a FULLTEXT index
            {type: 'FULLTEXT', fields: [ProjectFields.noSignName]},
        ]
    }
);
ProjectExperience.beforeSave((Project, options) => {
    if (Project[ProjectFields.name]) {
        Project.noSignName = TextHelper.noSignViStringAndToLowerCase(Project[ProjectFields.name]);
    }
});
ProjectExperience.beforeBulkUpdate((query, instanse) => {
    if (query.attributes[ProjectFields.name]) {
        query.fields.push(ProjectFields.noSignName);
        query.attributes[ProjectFields.noSignName] = TextHelper.noSignViStringAndToLowerCase(query.attributes[ProjectFields.name]);
    }
});

