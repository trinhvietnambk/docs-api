import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';
export const CartFields = {
    id: 'id',
    productId: 'productId',
    amount: 'amount',
    userId: 'userId',
};
export const CartAssociations = {};
export const Cart = sequelize.define('cart',
    {
        [CartFields.id]: {
            type: DataTypes.CHAR(50),
            primaryKey: true
        },
        [CartFields.productId]: {
            type: DataTypes.CHAR(50)
        },
        [CartFields.userId]: {
            type: DataTypes.CHAR(50)
        },
        [CartFields.amount]: {
            type: DataTypes.INTEGER
        }
    },
    {
        indexes: [
            {unique: false, fields: [CartFields.productId]},
            {unique: false, fields: [CartFields.userId]},
            {method: 'BTREE', fields: [CartFields.amount]}
        ]
    }
);
