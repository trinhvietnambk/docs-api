"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
exports.RatingFields = {
    id: 'id',
    subjectId: 'subjectId',
    subjectType: 'subjectType',
    objectId: 'objectId',
    objectType: 'objectType',
    content: 'content',
    star: 'star',
    createdByUserId: 'createdByUserId',
    role: 'role',
};
exports.RatingAssociations = {};
exports.Rating = _Base_1.sequelize.define('rating', {
    [exports.RatingFields.id]: {
        type: sequelize_1.DataTypes.CHAR(50),
        primaryKey: true
    },
    [exports.RatingFields.subjectId]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.RatingFields.subjectType]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.RatingFields.objectId]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.RatingFields.objectType]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.RatingFields.content]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.RatingFields.star]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [exports.RatingFields.createdByUserId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.RatingFields.role]: {
        type: sequelize_1.DataTypes.CHAR(50)
    }
}, {
    indexes: [
        {
            unique: false,
            fields: [
                exports.RatingFields.objectId,
            ]
        },
        {
            unique: false,
            fields: [
                exports.RatingFields.objectType
            ]
        },
        {
            method: 'BTREE',
            fields: ['createdAt']
        },
        {
            method: 'BTREE',
            fields: ['updatedAt']
        }
    ]
});
//# sourceMappingURL=Rating.js.map