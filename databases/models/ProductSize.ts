import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';

export const ProductSizeFields = {
    id: 'id',
    value: 'value',
    productId: 'productId',
};
export const ProductSize = sequelize.define('productSize',
    {

        [ProductSizeFields.id]: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        [ProductSizeFields.value]: {
            type: DataTypes.INTEGER
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [ProductSizeFields.productId]
            }
        ]
    }
);
