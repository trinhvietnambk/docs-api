"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
exports.TypeOfBusinessFields = {
    id: 'id',
    value: 'value',
    contractorId: 'contractorId',
};
exports.TypeOfBusiness = _Base_1.sequelize.define('cTypeOfBusiness', {
    [exports.TypeOfBusinessFields.id]: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    [exports.TypeOfBusinessFields.value]: {
        type: sequelize_1.DataTypes.TEXT
    }
}, {
    indexes: [
        {
            unique: false,
            fields: [exports.TypeOfBusinessFields.contractorId]
        }
    ]
});
//# sourceMappingURL=cTypeOfBusiness.js.map