import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';

export const ProductCollectionFields = {
    id: 'id',
    value: 'value',
    productId: 'productId',
};
export const ProductCollection = sequelize.define('productCollection',
    {

        [ProductCollectionFields.id]: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        [ProductCollectionFields.value]: {
            type: DataTypes.CHAR(100)
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [ProductCollectionFields.productId]
            }
        ]
    }
);
