import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize'
export const EntityFields = {
    id: 'id',
    type: 'type'
};
export const EntityAssociations = {
};
export const Entity = sequelize.define('entity',
    {
        [EntityFields.id]: {
            type: DataTypes.CHAR(50),
            primaryKey: true
        },
        [EntityFields.type]: {
            type: DataTypes.CHAR(50)
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [EntityFields.type]
            }
        ]
    }
);
