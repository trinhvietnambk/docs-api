import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';
import {PostFields} from "./Post";
import {BiddingCategoryFields} from "./BiddingCategory";

export const CommentFields = {
    id: 'id',
    subjectId: 'subjectId',
    subjectType: 'subjectType',
    objectId: 'objectId',
    objectType: 'objectType',
    createdByUserId: 'createdByUserId',
    content: 'content',
    images: 'images',
    videos: 'videos',
    audios: 'audios',
    files: 'files',
    numberOfLikes: 'numberOfLikes',
    numberOfComments: 'numberOfComments'
};
export const CommentAssociations = {};
export const Comment = sequelize.define('comment',
    {
        [CommentFields.id]: {
            type: DataTypes.CHAR(50),
            primaryKey: true
        },
        [CommentFields.subjectId]: {
            type: DataTypes.CHAR(50)
        },
        [CommentFields.subjectType]: {
            type: DataTypes.CHAR(50)
        },
        [CommentFields.objectId]: {
            type: DataTypes.CHAR(50)
        },
        [CommentFields.objectType]: {
            type: DataTypes.CHAR(50)
        },
        [CommentFields.createdByUserId]: {
            type: DataTypes.CHAR(50)
        }, [PostFields.content]: {
            type: DataTypes.TEXT
        },
        [PostFields.content]: {
            type: DataTypes.TEXT
        },
        [CommentFields.images]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(CommentFields.images, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(CommentFields.images)) {
                    return JSON.parse(this.getDataValue(CommentFields.images));
                }
            }
        },
        [CommentFields.videos]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(CommentFields.videos, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(CommentFields.videos)) {
                    return JSON.parse(this.getDataValue(CommentFields.videos));
                }
            }
        },
        [CommentFields.files]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(CommentFields.files, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(CommentFields.files)) {
                    return JSON.parse(this.getDataValue(CommentFields.files));
                }
            }
        },
        [CommentFields.audios]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(CommentFields.audios, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(CommentFields.audios)) {
                    return JSON.parse(this.getDataValue(CommentFields.audios));
                }
            }
        },
        [CommentFields.numberOfLikes]: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
        },
        [CommentFields.numberOfComments]: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
        }
    },
    {
        indexes: [
            {unique: false, fields: [CommentFields.objectId]},
            {unique: false, fields: [CommentFields.subjectId]},
            {
                method:'BTREE',
                fields: ['createdAt','updatedAt']
            }
        ]
    }
);
