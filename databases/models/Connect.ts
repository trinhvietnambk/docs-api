import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize'
import {CompanyFields} from "./Company";
export const ConnectFields = {
    id: 'id',
    subjectId: 'subjectId',
    subjectType: 'subjectType',
    objectId: 'objectId',
    objectType: 'objectType',
    action: 'action',

    numberOfFollowing:'numberOfFollowing',
    numberOfFollower:'numberOfFollower',
    numberOfComments:'numberOfComments',
    numberOfLikes:'numberOfLikes',
};
export const ConnectAssociations = {
};
export const Connect = sequelize.define('connect',
    {
        [ConnectFields.id]: {
            type: DataTypes.CHAR(50),
            primaryKey: true
        },
        [ConnectFields.subjectId]: {
            type: DataTypes.CHAR(50)
        },
        [ConnectFields.objectId]: {
            type: DataTypes.CHAR(50)
        },
        [ConnectFields.action]: {
            type: DataTypes.CHAR(20)
        },
        [ConnectFields.subjectType]: {
            type: DataTypes.CHAR(20)
        },
        [ConnectFields.objectType]: {
            type: DataTypes.CHAR(20)
        }
    },
    {
        indexes: [
            {unique: false, fields: [ConnectFields.subjectId]},
            {unique: false, fields: [ConnectFields.subjectType]},
            {unique: false, fields: [ConnectFields.objectId]},
            {unique: false, fields: [ConnectFields.objectType]},
            {unique: false, fields: [ConnectFields.action]}
        ]
    }
);
