"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
exports.QuotationFields = {
    id: 'id',
    createdByUserId: 'createdByUserId',
    fromId: 'fromId',
    fromType: 'fromType',
    toId: 'toId',
    toType: 'toType',
    contentLetter: 'contentLetter',
    timeSendBidding: 'timeSendBidding',
    note: 'note',
    isSentBidding: 'isSentBidding',
    biddingStatus: 'biddingStatus',
    feelingStatus: 'feelingStatus',
    totalMoney: 'totalMoney',
    currency: 'currency',
    totalTime: 'totalTime',
    unitTime: 'unitTime',
    version: 'version',
    projectId: 'projectId',
    receiverId: 'receiverId',
    documents: 'documents',
    works: 'works',
    materials: 'materials',
    alternativeBid: 'alternativeBid',
    totalMoneyWorks: 'totalMoneyWorks',
    totalTimeWorks: 'totalTimeWorks',
    totalMoneyMaterials: 'totalMoneyMaterials',
};
exports.QuotationAssociations = {};
exports.Quotation = _Base_1.sequelize.define('quotation', {
    [exports.QuotationFields.id]: {
        type: sequelize_1.DataTypes.CHAR(50),
        primaryKey: true
    },
    [exports.QuotationFields.createdByUserId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.QuotationFields.fromId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.QuotationFields.toId]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.QuotationFields.fromType]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.QuotationFields.toType]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.QuotationFields.contentLetter]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.QuotationFields.timeSendBidding]: {
        type: sequelize_1.DataTypes.DATE
    },
    [exports.QuotationFields.note]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.QuotationFields.isSentBidding]: {
        type: sequelize_1.DataTypes.BOOLEAN
    },
    [exports.QuotationFields.biddingStatus]: {
        type: sequelize_1.DataTypes.CHAR(20)
    },
    [exports.QuotationFields.totalMoney]: {
        type: sequelize_1.DataTypes.BIGINT
    },
    [exports.QuotationFields.currency]: {
        type: sequelize_1.DataTypes.CHAR(20)
    },
    [exports.QuotationFields.totalTime]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [exports.QuotationFields.unitTime]: {
        type: sequelize_1.DataTypes.CHAR(20)
    },
    [exports.QuotationFields.version]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [exports.QuotationFields.projectId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.QuotationFields.receiverId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.QuotationFields.feelingStatus]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.QuotationFields.documents]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.QuotationFields.documents, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.QuotationFields.documents)) {
                return JSON.parse(this.getDataValue(exports.QuotationFields.documents));
            }
        }
    },
    [exports.QuotationFields.works]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.QuotationFields.works, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.QuotationFields.works)) {
                return JSON.parse(this.getDataValue(exports.QuotationFields.works));
            }
        }
    },
    [exports.QuotationFields.materials]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.QuotationFields.materials, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.QuotationFields.materials)) {
                return JSON.parse(this.getDataValue(exports.QuotationFields.materials));
            }
        }
    },
    [exports.QuotationFields.alternativeBid]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.QuotationFields.alternativeBid, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.QuotationFields.alternativeBid)) {
                return JSON.parse(this.getDataValue(exports.QuotationFields.alternativeBid));
            }
        }
    },
    [exports.QuotationFields.totalMoneyWorks]: {
        type: sequelize_1.DataTypes.BIGINT
    },
    [exports.QuotationFields.totalTimeWorks]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [exports.QuotationFields.totalMoneyMaterials]: {
        type: sequelize_1.DataTypes.BIGINT
    }
}, {
    indexes: [
        {
            unique: false,
            fields: [
                exports.QuotationFields.fromId,
            ]
        },
        {
            unique: false,
            fields: [
                exports.QuotationFields.fromType,
            ]
        },
        {
            unique: false,
            fields: [
                exports.QuotationFields.toId,
            ]
        },
        {
            unique: false,
            fields: [
                exports.QuotationFields.toType,
            ]
        },
        {
            unique: false,
            fields: [
                exports.QuotationFields.receiverId,
            ]
        },
        {
            method: 'BTREE',
            fields: ['createdAt']
        },
        {
            method: 'BTREE',
            fields: ['updatedAt']
        }
    ]
});
//# sourceMappingURL=Quotation.js.map