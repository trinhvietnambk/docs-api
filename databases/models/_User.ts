import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize'
import {TextHelper} from "../../utities/TextHelper";

export const UserFields = {
    id: 'id',
    name: 'name',
    info: 'info',
    noSignName: 'noSignName',
    createdProjects: 'createdProjects'
};
export const UserAssociations = {
    createdProjects: 'createdProjects'
};
export const User = sequelize.define('user',
    {
        [UserFields.id]: {
            type: DataTypes.CHAR,
            primaryKey: true
        },
        [UserFields.name]: {
            type: DataTypes.TEXT
        },
        [UserFields.info]: {
            type: DataTypes.TEXT,
            set(val) {
                console.log('aaaaaaaaaaa ', val);
                this.setDataValue(UserFields.info, JSON.stringify(val))
            },
            get() {
                console.log('bbbbbbbbbbbbbbbbbbb ', this.getDataValue(UserFields.noSignName));
                if (this.getDataValue(UserFields.info)) {
                    return JSON.parse(this.getDataValue(UserFields.info))
                }
            }
        },
        [UserFields.noSignName]: {
            type: DataTypes.TEXT
        }
    }
);
User.beforeSave((user, options) => {
    if (user[UserFields.name]) {
        user.noSignName = TextHelper.changeAlias(user[UserFields.name]);
    }
});
User.beforeBulkUpdate((query, instanse) => {
    if (query.attributes[UserFields.name]) {
        query.fields.push(UserFields.noSignName);
        query.attributes[UserFields.noSignName] = TextUtil.changeAlias(query.attributes[UserFields.name]);
    }
});

// User.sync({force: true}).then(() => {
//     // Table created
//     return User.create({
//         firstName: 'John',
//         lastName: 'Hancock',
//         info: {email: '123', phone: 'asg52354235'}
//     });
// });
// User.create({
//     firstName: 'John',
//     lastName: 'Hancock',
//     info: {email: '123', phone: 'asg52354235'}
// });
// User.update(
//     {[UserFields.firstName]: 'nam', [UserFields.lastName]: 'trinh viet'},
//     {where: {id: 1}}
// ).then((err) => {
//     console.log(err)
// });

// User.findOne({attributes: [UserFields.id, UserFields.firstName]}).then(users => {
//     console.log(users.dataValues);
// })