import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';
import {TextHelper} from "../../utities/TextHelper";

export const ProjectFields = {
    id: 'id',
    name: 'name',
    description: 'description',
    documents: 'documents',
    listImages: 'listImages',
    listVideos: 'listVideos',
    projectType: 'projectType',
    projectSubType: 'projectSubType',
    workType: 'workType',
    address: 'address',
    village: 'village',
    district: 'district',
    city: 'city',
    country: 'country',
    minBudget: 'minBudget',
    maxBudget: 'maxBudget',
    investmentType: 'investmentType',
    minConstructionArea: 'minConstructionArea',
    maxConstructionArea: 'maxConstructionArea',
    minLandArea: 'minLandArea',
    maxLandArea: 'maxLandArea',
    totalFloorArea: 'totalFloorArea',
    state: 'state',
    status: 'status',
    minStartTime: 'minStartTime',
    maxStartTime: 'maxStartTime',
    minFinishTime: 'minFinishTime',
    maxFinishTime: 'maxFinishTime',
    purpose: 'purpose',
    createdByUserId: 'createdByUserId',
    approved: 'approved',
    isHot: 'isHot',
    currencyUnit: 'currencyUnit',
    //noSign prefix for extra field to support search
    noSignName: 'noSignName',

    //counter
    numberOfFollower: 'numberOfLikes',
    numberOfLikes: 'numberOfLikes',
    //associations fields of Project
    createdByUser: 'createdByUser',
    participants: 'participants',
    biddingCategories: 'biddingCategories'
};
export const ProjectAssociations = {
    [ProjectFields.createdByUser]: ProjectFields.createdByUser,
    [ProjectFields.participants]: ProjectFields.participants,
};
export const Project = sequelize.define('project',
    {
        [ProjectFields.id]: {
            type: DataTypes.CHAR(50),
            primaryKey: true
        },
        [ProjectFields.name]: {
            type: DataTypes.TEXT
        },
        [ProjectFields.description]: {
            type: DataTypes.TEXT
        },
        [ProjectFields.documents]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(ProjectFields.documents, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(ProjectFields.documents)) {
                    return JSON.parse(this.getDataValue(ProjectFields.documents));
                }
            }
        },
        [ProjectFields.listImages]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(ProjectFields.listImages, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(ProjectFields.listImages)) {
                    return JSON.parse(this.getDataValue(ProjectFields.listImages));
                }
            }
        },
        [ProjectFields.listVideos]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(ProjectFields.listVideos, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(ProjectFields.listVideos)) {
                    return JSON.parse(this.getDataValue(ProjectFields.listVideos));
                }
            }
        },
        [ProjectFields.projectType]: {
            type: DataTypes.CHAR(50)
        },
        [ProjectFields.projectSubType]: {
            type: DataTypes.CHAR(50)
        },
        [ProjectFields.workType]: {
            type: DataTypes.CHAR(50)
        },
        [ProjectFields.address]: {
            type: DataTypes.TEXT
        },
        [ProjectFields.village]: {
            type: DataTypes.CHAR(50)
        },
        [ProjectFields.district]: {
            type: DataTypes.CHAR(50)
        },
        [ProjectFields.city]: {
            type: DataTypes.CHAR(50)
        },
        [ProjectFields.country]: {
            type: DataTypes.CHAR(50)
        },
        [ProjectFields.minBudget]: {
            type: DataTypes.REAL
        },
        [ProjectFields.maxBudget]: {
            type: DataTypes.REAL
        },
        [ProjectFields.investmentType]: {
            type: DataTypes.CHAR(100)
        },
        [ProjectFields.minConstructionArea]: {
            type: DataTypes.INTEGER
        },
        [ProjectFields.maxConstructionArea]: {
            type: DataTypes.INTEGER
        },
        [ProjectFields.minLandArea]: {
            type: DataTypes.INTEGER
        },
        [ProjectFields.maxLandArea]: {
            type: DataTypes.INTEGER
        },
        [ProjectFields.totalFloorArea]: {
            type: DataTypes.INTEGER
        },
        [ProjectFields.minStartTime]: {
            type: DataTypes.DATE
        },
        [ProjectFields.maxStartTime]: {
            type: DataTypes.DATE
        },
        [ProjectFields.minFinishTime]: {
            type: DataTypes.DATE
        },
        [ProjectFields.maxFinishTime]: {
            type: DataTypes.DATE
        },
        [ProjectFields.purpose]: {
            type: DataTypes.TEXT
        },
        [ProjectFields.noSignName]: {
            type: DataTypes.TEXT
        },
        [ProjectFields.numberOfFollower]: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
        },
        [ProjectFields.numberOfLikes]: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
        },
        [ProjectFields.status]: {
            type: DataTypes.CHAR(100)
        },
        [ProjectFields.state]: {
            type: DataTypes.CHAR(100)
        },
        [ProjectFields.approved]: {
            type: DataTypes.BOOLEAN
        },
        [ProjectFields.isHot]: {
            type: DataTypes.BOOLEAN
        }
    },
    {
        indexes: [
            // add a FULLTEXT index
            {type: 'FULLTEXT', fields: [ProjectFields.noSignName]},
            {
                method: 'BTREE', fields: [
                    ProjectFields.maxFinishTime
                ]
            },
            {
                method: 'BTREE', fields: [

                    ProjectFields.minFinishTime,

                ]
            },
            {
                method: 'BTREE', fields: [

                    ProjectFields.maxStartTime,

                ]
            },
            {
                method: 'BTREE', fields: [

                    ProjectFields.minStartTime,

                ]
            },
            {
                method: 'BTREE', fields: [

                    ProjectFields.minLandArea,

                ]
            },
            {
                method: 'BTREE', fields: [
                    ProjectFields.maxLandArea
                ]
            },
            {
                method: 'BTREE', fields: [

                    ProjectFields.maxBudget,

                ]
            },
            {
                method: 'BTREE', fields: [

                    ProjectFields.minBudget,

                ]
            },
            {
                method: 'BTREE', fields: [

                    ProjectFields.minConstructionArea,
                ]
            },
            {
                method: 'BTREE', fields: [

                    ProjectFields.maxConstructionArea,
                ]
            },
            {
                unique: false, fields: [
                    ProjectFields.createdByUserId
                ]
            },
            {
                unique: false, fields: [

                    ProjectFields.totalFloorArea
                ]
            }, {
                unique: false, fields: [

                    ProjectFields.country,

                ]
            },
            {
                unique: false, fields: [

                    ProjectFields.district,

                ]
            },
            {
                unique: false,
                fields: [

                    ProjectFields.isHot,

                ]
            },
            {
                unique: false, fields: [

                    ProjectFields.city,

                ]
            },
            {
                unique: false, fields: [

                    ProjectFields.projectSubType,

                ]
            },
            {
                unique: false, fields: [

                    ProjectFields.projectType
                ]
            },
            {
                unique: false, fields: [

                    ProjectFields.workType,

                ]
            },
            {
                unique: false, fields: [


                    ProjectFields.status,

                ]
            },
            {
                unique: false, fields: [

                    ProjectFields.state,

                ]
            },
            {
                unique: false, fields: [
                    ProjectFields.investmentType
                ]
            },
            {
                method: 'BTREE',
                fields: ['createdAt']
            },
            {
                method: 'BTREE',
                fields: ['updatedAt']
            }
        ]
    }
);
Project.beforeSave((Project, options) => {
    if (Project[ProjectFields.name]) {
        Project.noSignName = TextHelper.noSignViStringAndToLowerCase(Project[ProjectFields.name]);
    }
});
Project.beforeBulkUpdate((query, instanse) => {
    if (query.attributes[ProjectFields.name]) {
        query.fields.push(ProjectFields.noSignName);
        query.attributes[ProjectFields.noSignName] = TextHelper.noSignViStringAndToLowerCase(query.attributes[ProjectFields.name]);
    }
});


// User.sync({force: true}).then(() => {
//     // Table created
//     return User.create({
//         firstName: 'John',
//         lastName: 'Hancock',
//         info: {email: '123', phone: 'asg52354235'}
//     });
// });
// User.create({
//     firstName: 'John',
//     lastName: 'Hancock',
//     info: {email: '123', phone: 'asg52354235'}
// });
// User.update(
//     {[UserFields.firstName]: 'nam', [UserFields.lastName]: 'trinh viet'},
//     {where: {id: 1}}
// ).then((err) => {
//     console.log(err)
// });

// User.findOne({attributes: [UserFields.id, UserFields.firstName]}).then(users => {
//     console.log(users.dataValues);
// })