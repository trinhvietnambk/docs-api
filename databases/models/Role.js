"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
exports.RoleFields = {
    id: 'id',
    subjectId: 'subjectId',
    subjectType: 'subjectType',
    objectId: 'objectId',
    objectType: 'objectType',
    role: 'role'
};
exports.RoleValues = {
    admin: 'admin',
    member: 'member',
    createBiddingCategory: 'createBiddingCategory'
};
exports.RoleAssociations = {};
exports.Role = _Base_1.sequelize.define('role', {
    [exports.RoleFields.subjectId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.RoleFields.subjectType]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.RoleFields.objectId]: {
        type: sequelize_1.DataTypes.CHAR(50),
    },
    [exports.RoleFields.objectType]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.RoleFields.role]: {
        type: sequelize_1.DataTypes.CHAR(50)
    }
}, {
    indexes: [
        {
            unique: false,
            fields: [
                exports.RoleFields.subjectId,
            ]
        },
        {
            unique: false,
            fields: [
                exports.RoleFields.subjectType,
            ]
        },
        {
            unique: false,
            fields: [
                exports.RoleFields.objectId,
            ]
        },
        {
            unique: false,
            fields: [
                exports.RoleFields.objectType,
            ]
        },
        {
            unique: false,
            fields: [
                exports.RoleFields.role,
            ]
        }
    ]
});
//# sourceMappingURL=Role.js.map