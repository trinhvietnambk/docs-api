"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
const TextHelper_1 = require("../../utities/TextHelper");
exports.BiddingCategoryFields = {
    id: 'id',
    name: 'name',
    typeOfBid: 'typeOfBid',
    bidOpenTime: 'bidOpenTime',
    bidCloseTime: 'bidCloseTime',
    allowAlternativeBids: 'allowAlternativeBids',
    allowSealedBids: 'allowSealedBids',
    expectTypeOfContractor: 'expectTypeOfContractor',
    expectCountryOfContractor: 'expectCountryOfContractor',
    expectCityOfContractor: 'expectCityOfContractor',
    expectDistrictOfContractor: 'expectDistrictOfContractor',
    expectExperienceContractor: 'expectExperienceContractor',
    expectRatingOfContractor: 'expectRatingOfContractor',
    expectRevenueOfContractor: 'expectRevenueOfContractor',
    expectUnitRevenueOfContractor: 'expectUnitRevenueOfContractor',
    expectOtherRequirements: 'expectOtherRequirements',
    documents: 'documents',
    works: 'works',
    materials: 'materials',
    preferredCurrency: 'preferredCurrency',
    invitedEmails: 'invitedEmails',
    invitedFromFavorites: 'invitedFromFavorites',
    status: 'status',
    createdByUserId: 'createdByUserId',
    projectId: 'projectId',
    createdById: 'createdById',
    createdByType: 'createdByType',
    //counter
    numOfReceivedBids: 'numOfReceivedBids',
    numOfSentInvitations: 'numOfSentInvitations',
    numOfBiddingProcess: 'numOfBiddingProcess',
    //noSign prefix for extra field to support search
    noSignName: 'noSignName',
    //associations fields of BiddingCategory
    project: 'project'
};
exports.BiddingStatus = {
    open: 'open',
    bidded: 'bidded',
};
exports.BiddingCategoryAssociations = {
    [exports.BiddingCategoryFields.project]: exports.BiddingCategoryFields.project
};
exports.BiddingCategory = _Base_1.sequelize.define('biddingCategory', {
    [exports.BiddingCategoryFields.id]: {
        type: sequelize_1.DataTypes.CHAR(50),
        primaryKey: true
    },
    [exports.BiddingCategoryFields.name]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.BiddingCategoryFields.typeOfBid]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.BiddingCategoryFields.bidOpenTime]: {
        type: sequelize_1.DataTypes.DATE
    },
    [exports.BiddingCategoryFields.bidCloseTime]: {
        type: sequelize_1.DataTypes.DATE
    },
    [exports.BiddingCategoryFields.allowAlternativeBids]: {
        type: sequelize_1.DataTypes.BOOLEAN
    },
    [exports.BiddingCategoryFields.allowSealedBids]: {
        type: sequelize_1.DataTypes.BOOLEAN
    },
    [exports.BiddingCategoryFields.expectTypeOfContractor]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.BiddingCategoryFields.expectCountryOfContractor]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.BiddingCategoryFields.expectCityOfContractor]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.BiddingCategoryFields.expectDistrictOfContractor]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.BiddingCategoryFields.expectExperienceContractor]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [exports.BiddingCategoryFields.expectRatingOfContractor]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [exports.BiddingCategoryFields.expectRevenueOfContractor]: {
        type: sequelize_1.DataTypes.REAL
    },
    [exports.BiddingCategoryFields.expectUnitRevenueOfContractor]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.BiddingCategoryFields.expectOtherRequirements]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.BiddingCategoryFields.documents]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.BiddingCategoryFields.documents, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.BiddingCategoryFields.documents)) {
                return JSON.parse(this.getDataValue(exports.BiddingCategoryFields.documents));
            }
        }
    },
    [exports.BiddingCategoryFields.works]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.BiddingCategoryFields.works, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.BiddingCategoryFields.works)) {
                return JSON.parse(this.getDataValue(exports.BiddingCategoryFields.works));
            }
        }
    },
    [exports.BiddingCategoryFields.materials]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.BiddingCategoryFields.materials, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.BiddingCategoryFields.materials)) {
                return JSON.parse(this.getDataValue(exports.BiddingCategoryFields.materials));
            }
        }
    },
    [exports.BiddingCategoryFields.invitedFromFavorites]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.BiddingCategoryFields.invitedFromFavorites, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.BiddingCategoryFields.invitedFromFavorites)) {
                return JSON.parse(this.getDataValue(exports.BiddingCategoryFields.invitedFromFavorites));
            }
        }
    },
    [exports.BiddingCategoryFields.preferredCurrency]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.BiddingCategoryFields.numOfReceivedBids]: {
        type: sequelize_1.DataTypes.INTEGER,
        defaultValue: 0
    },
    [exports.BiddingCategoryFields.numOfSentInvitations]: {
        type: sequelize_1.DataTypes.INTEGER,
        defaultValue: 0
    },
    [exports.BiddingCategoryFields.numOfBiddingProcess]: {
        type: sequelize_1.DataTypes.INTEGER,
        defaultValue: 0
    },
    [exports.BiddingCategoryFields.invitedEmails]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.BiddingCategoryFields.status]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.BiddingCategoryFields.createdByUserId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.BiddingCategoryFields.projectId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.BiddingCategoryFields.createdById]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.BiddingCategoryFields.createdByType]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.BiddingCategoryFields.noSignName]: {
        type: sequelize_1.DataTypes.TEXT
    }
}, {
    indexes: [
        { unique: false, fields: [exports.BiddingCategoryFields.projectId] },
        {
            method: 'BTREE',
            fields: ['createdAt']
        },
        {
            method: 'BTREE',
            fields: ['updatedAt']
        }
    ]
});
exports.BiddingCategory.beforeSave((BiddingCategory, options) => {
    if (BiddingCategory[exports.BiddingCategoryFields.name]) {
        BiddingCategory.noSignName = TextHelper_1.TextHelper.noSignViStringAndToLowerCase(BiddingCategory[exports.BiddingCategoryFields.name]);
    }
});
exports.BiddingCategory.beforeBulkUpdate((query, instanse) => {
    if (query.attributes[exports.BiddingCategoryFields.name]) {
        query.fields.push(exports.BiddingCategoryFields.noSignName);
        query.attributes[exports.BiddingCategoryFields.noSignName] = TextHelper_1.TextHelper.noSignViStringAndToLowerCase(query.attributes[exports.BiddingCategoryFields.name]);
    }
});
// User.sync({force: true}).then(() => {
//     // Table created
//     return User.create({
//         firstName: 'John',
//         lastName: 'Hancock',
//         info: {email: '123', phone: 'asg52354235'}
//     });
// });
// User.create({
//     firstName: 'John',
//     lastName: 'Hancock',
//     info: {email: '123', phone: 'asg52354235'}
// });
// User.update(
//     {[UserFields.firstName]: 'nam', [UserFields.lastName]: 'trinh viet'},
//     {where: {id: 1}}
// ).then((err) => {
//     console.log(err)
// });
// User.findOne({attributes: [UserFields.id, UserFields.firstName]}).then(users => {
//     console.log(users.dataValues);
// })
//# sourceMappingURL=BiddingCategory.js.map