"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
exports.ConnectFields = {
    id: 'id',
    subjectId: 'subjectId',
    subjectType: 'subjectType',
    objectId: 'objectId',
    objectType: 'objectType',
    action: 'action',
    numberOfFollowing: 'numberOfFollowing',
    numberOfFollower: 'numberOfFollower',
    numberOfComments: 'numberOfComments',
    numberOfLikes: 'numberOfLikes',
};
exports.ConnectAssociations = {};
exports.Connect = _Base_1.sequelize.define('connect', {
    [exports.ConnectFields.id]: {
        type: sequelize_1.DataTypes.CHAR(50),
        primaryKey: true
    },
    [exports.ConnectFields.subjectId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.ConnectFields.objectId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.ConnectFields.action]: {
        type: sequelize_1.DataTypes.CHAR(20)
    },
    [exports.ConnectFields.subjectType]: {
        type: sequelize_1.DataTypes.CHAR(20)
    },
    [exports.ConnectFields.objectType]: {
        type: sequelize_1.DataTypes.CHAR(20)
    }
}, {
    indexes: [
        { unique: false, fields: [exports.ConnectFields.subjectId] },
        { unique: false, fields: [exports.ConnectFields.subjectType] },
        { unique: false, fields: [exports.ConnectFields.objectId] },
        { unique: false, fields: [exports.ConnectFields.objectType] },
        { unique: false, fields: [exports.ConnectFields.action] }
    ]
});
//# sourceMappingURL=Connect.js.map