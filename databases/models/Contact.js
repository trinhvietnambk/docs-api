"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
exports.ContactFields = {
    id: 'id',
    userId: 'userId',
    name: 'name',
    firstName: 'firstName',
    lastName: 'lastName',
    email: 'email',
    avatar: 'avatar',
    phone: 'phone',
    companyTaxCode: 'companyTaxCode',
    position: "position"
};
exports.ContactAssociations = {};
exports.Contact = _Base_1.sequelize.define('contact', {
    [exports.ContactFields.id]: {
        type: sequelize_1.DataTypes.CHAR(50),
        primaryKey: true
    },
    [exports.ContactFields.name]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.ContactFields.firstName]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.ContactFields.lastName]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.ContactFields.email]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.ContactFields.avatar]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.ContactFields.phone]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.ContactFields.position]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.ContactFields.companyTaxCode]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.ContactFields.userId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    }
}, {
    indexes: [
        { unique: false, fields: [exports.ContactFields.companyTaxCode] },
    ]
});
//# sourceMappingURL=Contact.js.map