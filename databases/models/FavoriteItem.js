"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
exports.FavoriteItemFields = {
    id: 'id',
    name: 'name',
    price: 'price',
    currency: 'currency',
    contractorId: 'contractorId',
};
exports.FavoriteItemAssociations = {};
exports.FavoriteItem = _Base_1.sequelize.define('activecode', {
    [exports.FavoriteItemFields.id]: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    [exports.FavoriteItemFields.name]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.FavoriteItemFields.price]: {
        type: sequelize_1.DataTypes.BIGINT
    },
    [exports.FavoriteItemFields.currency]: {
        type: sequelize_1.DataTypes.CHAR(50)
    }
}, {
    indexes: [
        {
            unique: false,
            fields: [exports.FavoriteItemFields.contractorId]
        }
    ]
});
//# sourceMappingURL=FavoriteItem.js.map