import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';
import {TextHelper} from "../../utities/TextHelper";
import {ActiveCodeFields} from "./ActiveCode";

export const BiddingCategoryFields = {
    id: 'id',
    name: 'name',
    typeOfBid: 'typeOfBid',
    bidOpenTime: 'bidOpenTime', // Thời gian mở thầu
    bidCloseTime: 'bidCloseTime', // Thời gian đóng thầu
    allowAlternativeBids: 'allowAlternativeBids',
    allowSealedBids: 'allowSealedBids',
    expectTypeOfContractor: 'expectTypeOfContractor',
    expectCountryOfContractor: 'expectCountryOfContractor',
    expectCityOfContractor: 'expectCityOfContractor',
    expectDistrictOfContractor: 'expectDistrictOfContractor',
    expectExperienceContractor: 'expectExperienceContractor',
    expectRatingOfContractor: 'expectRatingOfContractor',
    expectRevenueOfContractor: 'expectRevenueOfContractor',
    expectUnitRevenueOfContractor: 'expectUnitRevenueOfContractor',
    expectOtherRequirements: 'expectOtherRequirements',
    documents: 'documents',
    works: 'works',
    materials: 'materials',
    preferredCurrency: 'preferredCurrency',

    invitedEmails: 'invitedEmails',
    invitedFromFavorites: 'invitedFromFavorites',
    status: 'status',//Open or Bidded
    createdByUserId: 'createdByUserId',
    projectId: 'projectId',
    createdById: 'createdById',
    createdByType: 'createdByType',

    //counter
    numOfReceivedBids: 'numOfReceivedBids',
    numOfSentInvitations: 'numOfSentInvitations',
    numOfBiddingProcess: 'numOfBiddingProcess',


    //noSign prefix for extra field to support search
    noSignName: 'noSignName',

    //associations fields of BiddingCategory
    project: 'project'
};
export const BiddingStatus = {
    open: 'open',
    bidded: 'bidded',
};
export const BiddingCategoryAssociations = {
    [BiddingCategoryFields.project]: BiddingCategoryFields.project
};
export const BiddingCategory = sequelize.define('biddingCategory',
    {
        [BiddingCategoryFields.id]: {
            type: DataTypes.CHAR(50),
            primaryKey: true
        },
        [BiddingCategoryFields.name]: {
            type: DataTypes.TEXT
        },
        [BiddingCategoryFields.typeOfBid]: {
            type: DataTypes.TEXT
        },

        [BiddingCategoryFields.bidOpenTime]: {
            type: DataTypes.DATE
        },
        [BiddingCategoryFields.bidCloseTime]: {
            type: DataTypes.DATE
        },
        [BiddingCategoryFields.allowAlternativeBids]: {
            type: DataTypes.BOOLEAN
        },
        [BiddingCategoryFields.allowSealedBids]: {
            type: DataTypes.BOOLEAN
        },
        [BiddingCategoryFields.expectTypeOfContractor]: {
            type: DataTypes.TEXT
        },
        [BiddingCategoryFields.expectCountryOfContractor]: {
            type: DataTypes.TEXT
        },
        [BiddingCategoryFields.expectCityOfContractor]: {
            type: DataTypes.TEXT
        },
        [BiddingCategoryFields.expectDistrictOfContractor]: {
            type: DataTypes.TEXT
        },
        [BiddingCategoryFields.expectExperienceContractor]: {
            type: DataTypes.INTEGER
        },
        [BiddingCategoryFields.expectRatingOfContractor]: {
            type: DataTypes.INTEGER
        },
        [BiddingCategoryFields.expectRevenueOfContractor]: {
            type: DataTypes.REAL
        },
        [BiddingCategoryFields.expectUnitRevenueOfContractor]: {
            type: DataTypes.TEXT
        },
        [BiddingCategoryFields.expectOtherRequirements]: {
            type: DataTypes.TEXT
        },
        [BiddingCategoryFields.documents]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(BiddingCategoryFields.documents, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(BiddingCategoryFields.documents)) {
                    return JSON.parse(this.getDataValue(BiddingCategoryFields.documents));
                }
            }
        },
        [BiddingCategoryFields.works]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(BiddingCategoryFields.works, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(BiddingCategoryFields.works)) {
                    return JSON.parse(this.getDataValue(BiddingCategoryFields.works));
                }
            }
        },
        [BiddingCategoryFields.materials]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(BiddingCategoryFields.materials, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(BiddingCategoryFields.materials)) {
                    return JSON.parse(this.getDataValue(BiddingCategoryFields.materials));
                }
            }
        },
        [BiddingCategoryFields.invitedFromFavorites]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(BiddingCategoryFields.invitedFromFavorites, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(BiddingCategoryFields.invitedFromFavorites)) {
                    return JSON.parse(this.getDataValue(BiddingCategoryFields.invitedFromFavorites));
                }
            }
        },
        [BiddingCategoryFields.preferredCurrency]: {
            type: DataTypes.TEXT
        },
        [BiddingCategoryFields.numOfReceivedBids]: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        [BiddingCategoryFields.numOfSentInvitations]: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        [BiddingCategoryFields.numOfBiddingProcess]: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        [BiddingCategoryFields.invitedEmails]: {
            type: DataTypes.TEXT
        },
        [BiddingCategoryFields.status]: {
            type: DataTypes.TEXT
        },
        [BiddingCategoryFields.createdByUserId]: {
            type: DataTypes.CHAR(50)
        },
        [BiddingCategoryFields.projectId]: {
            type: DataTypes.CHAR(50)
        },
        [BiddingCategoryFields.createdById]: {
            type: DataTypes.CHAR(50)
        },
        [BiddingCategoryFields.createdByType]: {
            type: DataTypes.TEXT
        },
        [BiddingCategoryFields.noSignName]: {
            type: DataTypes.TEXT
        }
    },
    {
        indexes: [
            {unique: false, fields: [BiddingCategoryFields.projectId]},
            {
                method:'BTREE',
                fields: ['createdAt']
            },
            {
                method:'BTREE',
                fields: ['updatedAt']
            }
        ]
    }
);
BiddingCategory.beforeSave((BiddingCategory, options) => {
    if (BiddingCategory[BiddingCategoryFields.name]) {
        BiddingCategory.noSignName = TextHelper.noSignViStringAndToLowerCase(BiddingCategory[BiddingCategoryFields.name]);
    }
});
BiddingCategory.beforeBulkUpdate((query, instanse) => {
    if (query.attributes[BiddingCategoryFields.name]) {
        query.fields.push(BiddingCategoryFields.noSignName);
        query.attributes[BiddingCategoryFields.noSignName] = TextHelper.noSignViStringAndToLowerCase(query.attributes[BiddingCategoryFields.name]);
    }
});


// User.sync({force: true}).then(() => {
//     // Table created
//     return User.create({
//         firstName: 'John',
//         lastName: 'Hancock',
//         info: {email: '123', phone: 'asg52354235'}
//     });
// });
// User.create({
//     firstName: 'John',
//     lastName: 'Hancock',
//     info: {email: '123', phone: 'asg52354235'}
// });
// User.update(
//     {[UserFields.firstName]: 'nam', [UserFields.lastName]: 'trinh viet'},
//     {where: {id: 1}}
// ).then((err) => {
//     console.log(err)
// });

// User.findOne({attributes: [UserFields.id, UserFields.firstName]}).then(users => {
//     console.log(users.dataValues);
// })