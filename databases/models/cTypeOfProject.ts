import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';

export const TypeOfProjectFields = {
    id: 'id',
    value: 'value',
    contractorId: 'contractorId',
};
export const TypeOfProject = sequelize.define('cTypeOfProject',
    {

        [TypeOfProjectFields.id]: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        [TypeOfProjectFields.value]: {
            type: DataTypes.TEXT
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [TypeOfProjectFields.contractorId]
            }
        ]
    }
);
