"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
exports.SubBusinessFields = {
    id: 'id',
    value: 'value',
    contractorId: 'contractorId',
};
exports.SubBusiness = _Base_1.sequelize.define('cSubBusiness', {
    [exports.SubBusinessFields.id]: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    [exports.SubBusinessFields.value]: {
        type: sequelize_1.DataTypes.TEXT
    }
}, {
    indexes: [
        {
            unique: false,
            fields: [exports.SubBusinessFields.contractorId]
        }
    ]
});
//# sourceMappingURL=cSubBusiness.js.map