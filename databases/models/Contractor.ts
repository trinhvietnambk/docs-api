import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';
import {TextHelper} from "../../utities/TextHelper";
import {ProjectFields} from "./Project";

export const ContractorFields = {
    id: 'id',
    name: "name",
    noSignName: "noSignName",
    type: "type",//Loại nha thầu Công ty hoặc cá nhân

    wallImage: "wallImage",
    logo: "logo",
    address: "address",
    city: "city",
    country: "country",
    district: "district",
    phone: "phone",
    fax: "fax",
    website: "website",
    email: "email",
    taxCode: "taxCode",
    origination: "origination",
    foundedYear: "foundedYear",
    description: "description",
    isHouseLinkMember: "isHouseLinkMember",
    isVerifiedLicense: "isVerifiedLicense",
    totalRanking: "totalRanking",// A+ BBB Rating
    typeOfCompany: "typeOfCompany",
    qualityManagement: "qualityManagement", //Hệ thống quản trị
    language: "language",
    numberOfExpProject: "numberOfExpProject",
    numberOfEmployees: "numberOfEmployees",
    numberOfBiddedProject: "numberOfBiddedProject",
    numberOfRating: "numOfRating",
    revenue: "revenue",
    unitRevenue: "unitRevenue",
    prizes: "prizes",
    price: "price",
    unitPrice: "unitPrice",
    listVideos: "listVideos",
    documents: "documents",
    finances: "finances",
    imageFinances: "imageFinances",
    affiliations: "affiliations",
    createdByUserId: "createdByUserId",
    approved: "approved",
    verifyDocuments:"verifyDocuments",
    services:'services',

    typeOfContractor: "typeOfContractor",//Loại nha thầu như [Thiết kế, cơ điện, xây dựng....]
    typeOfConstruction: "typeOfConstruction",//['Sửa chữa, xây mới'];
    typeOfProject: "typeOfProject",//['Chung cư, nhà dân, khách sạn'];
    typeOfBusiness: "typeOfBusiness",
    subBusiness: "subBusiness", //Chính là các lĩnh vực kinh doanh trong trang quản trị
    mainMarkets: "mainMarkets", // Chính là typeOfContractor

    //counter
    numberOfFollower: 'numberOfLikes',
    numberOfLikes: 'numberOfLikes',


    houselinkRanking: "houselinkRanking",
    projectExperiences: "projectExperiences",
    products: "products",
    contacts: "contacts",
    ratingList: "ratingList",
    createdByUser: "createdByUser"
};
export const ContractorAssociations = {};
export const Contractor = sequelize.define('contractor',
    {

        [ContractorFields.id]: {
            type: DataTypes.CHAR(50),
            primaryKey: true
        },
        [ContractorFields.name]: {
            type: DataTypes.TEXT
        },
        [ContractorFields.noSignName]: {
            type: DataTypes.TEXT
        },
        [ContractorFields.type]: {
            type: DataTypes.CHAR(10)
        },

        [ContractorFields.wallImage]: {
            type: DataTypes.TEXT
        },
        [ContractorFields.logo]: {
            type: DataTypes.TEXT
        },
        [ContractorFields.address]: {
            type: DataTypes.TEXT
        },
        [ContractorFields.city]: {
            type: DataTypes.CHAR(50)
        },
        [ContractorFields.country]: {
            type: DataTypes.CHAR(50)
        },
        [ContractorFields.district]: {
            type: DataTypes.CHAR(50)
        },
        [ContractorFields.phone]: {
            type: DataTypes.CHAR(30)
        },
        [ContractorFields.fax]: {
            type: DataTypes.CHAR(30)
        },
        [ContractorFields.website]: {
            type: DataTypes.CHAR(50)
        },
        [ContractorFields.email]: {
            type: DataTypes.CHAR(100)
        },
        [ContractorFields.taxCode]: {
            type: DataTypes.CHAR(30)
        },
        [ContractorFields.origination]: {
            type: DataTypes.CHAR(30)
        },
        [ContractorFields.foundedYear]: {
            type: DataTypes.INTEGER
        },
        [ContractorFields.description]: {
            type: DataTypes.TEXT
        },

        [ContractorFields.isHouseLinkMember]: {
            type: DataTypes.BOOLEAN
        },
        [ContractorFields.isVerifiedLicense]: {
            type: DataTypes.BOOLEAN
        },
        [ContractorFields.totalRanking]: {
            type: DataTypes.INTEGER
        },
        [ContractorFields.houselinkRanking]: {
            type: DataTypes.TEXT
        },


        /*Start field map to table
        [ContractorFields.typeOfBusiness]: {
            type: DataTypes.TEXT
        },
        [ContractorFields.subBusiness]: {
            type: DataTypes.TEXT
        },
        [ContractorFields.typeOfContractor]: {
            type: DataTypes.TEXT
        },
        [ContractorFields.typeOfConstruction]: {
            type: DataTypes.TEXT
        },
        [ContractorFields.typeOfProject]: {
            type: DataTypes.TEXT
        },
        [ContractorFields.mainMarkets]: {
            type: DataTypes.TEXT
        },
        End field map to table*/

        [ContractorFields.typeOfCompany]: {
            type: DataTypes.CHAR(100)
        },
        [ContractorFields.qualityManagement]: {
            type: DataTypes.TEXT
        },
        [ContractorFields.language]: {
            type: DataTypes.TEXT
        },
        [ContractorFields.numberOfExpProject]: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        [ContractorFields.numberOfEmployees]: {
            type: DataTypes.CHAR(100)
        },
        [ContractorFields.numberOfBiddedProject]: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        [ContractorFields.numberOfRating]: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        [ContractorFields.revenue]: {
            type: DataTypes.REAL
        },
        [ContractorFields.unitRevenue]: {
            type: DataTypes.TEXT
        },
        [ContractorFields.price]: {
            type: DataTypes.REAL
        },
        [ContractorFields.unitPrice]: {
            type: DataTypes.TEXT
        },
        [ContractorFields.prizes]: {
            type: DataTypes.REAL
        },
        [ContractorFields.services]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(ContractorFields.services, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(ContractorFields.services)) {
                    return JSON.parse(this.getDataValue(ContractorFields.services));
                }
            }
        },
        [ContractorFields.houselinkRanking]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(ContractorFields.houselinkRanking, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(ContractorFields.houselinkRanking)) {
                    return JSON.parse(this.getDataValue(ContractorFields.houselinkRanking));
                }
            }
        },
        [ContractorFields.documents]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(ContractorFields.documents, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(ContractorFields.documents)) {
                    return JSON.parse(this.getDataValue(ContractorFields.documents));
                }
            }
        },
        [ContractorFields.finances]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(ContractorFields.finances, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(ContractorFields.finances)) {
                    return JSON.parse(this.getDataValue(ContractorFields.finances));
                }
            }
        },
        [ContractorFields.imageFinances]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(ContractorFields.imageFinances, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(ContractorFields.imageFinances)) {
                    return JSON.parse(this.getDataValue(ContractorFields.imageFinances));
                }
            }
        },
        [ContractorFields.affiliations]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(ContractorFields.affiliations, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(ContractorFields.affiliations)) {
                    return JSON.parse(this.getDataValue(ContractorFields.affiliations));
                }
            }
        },
        [ContractorFields.listVideos]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(ContractorFields.listVideos, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(ContractorFields.listVideos)) {
                    return JSON.parse(this.getDataValue(ContractorFields.listVideos));
                }
            }
        },
        [ContractorFields.verifyDocuments]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(ContractorFields.verifyDocuments, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(ContractorFields.verifyDocuments)) {
                    return JSON.parse(this.getDataValue(ContractorFields.verifyDocuments));
                }
            }
        },
        [ContractorFields.createdByUserId]: {
            type: DataTypes.CHAR(50)
        },
        [ContractorFields.approved]: {
            type: DataTypes.BOOLEAN
        },
        [ContractorFields.numberOfFollower]: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
        },
        [ContractorFields.numberOfLikes]: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
        }
    },
    {
        indexes: [
            // add a FULLTEXT index
            {type: 'FULLTEXT', fields: [ContractorFields.noSignName]},
            {
                method: 'BTREE',
                fields: [ContractorFields.revenue]
            },
            {
                method:'BTREE',
                fields: [
                    ContractorFields.price]
            },
            {
                method: 'BTREE',
                fields: [
                    ContractorFields.foundedYear
                ]
            },
            {
                method: 'BTREE',
                fields: [
                    ContractorFields.totalRanking]
            },
            {
                unique: false,
                fields: [ContractorFields.approved]
            },
            {
                unique: false,
                fields: [ContractorFields.createdByUserId]
            },
            {
                unique: false,
                fields: [
                    ContractorFields.typeOfCompany]
            },
            {
                unique: false,
                fields: [
                    ContractorFields.origination]
            },
            {
                unique: false,
                fields: [

                    ContractorFields.country]
            },
            {
                unique: false,
                fields: [
                    ContractorFields.city,
                    ContractorFields.district]
            },
            {
                unique: false,
                fields: [
                    ContractorFields.district]
            },
            {
                unique: false,
                fields: [ContractorFields.taxCode]
            },
            {
                method:'BTREE',
                fields: ['createdAt']
            },
            {
                method:'BTREE',
                fields: ['updatedAt']
            }

        ]
    }
);
Contractor.beforeSave((Contractor, options) => {
    if (Contractor[ContractorFields.name]) {
        Contractor.noSignName = TextHelper.noSignViStringAndToLowerCase(Contractor[ContractorFields.name]);
    }
});
Contractor.beforeBulkUpdate((query, instanse) => {
    if (query.attributes[ContractorFields.name]) {
        query.fields.push(ContractorFields.noSignName);
        query.attributes[ContractorFields.noSignName] = TextHelper.noSignViStringAndToLowerCase(query.attributes[ContractorFields.name]);
    }
});
