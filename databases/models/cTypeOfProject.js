"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
exports.TypeOfProjectFields = {
    id: 'id',
    value: 'value',
    contractorId: 'contractorId',
};
exports.TypeOfProject = _Base_1.sequelize.define('cTypeOfProject', {
    [exports.TypeOfProjectFields.id]: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    [exports.TypeOfProjectFields.value]: {
        type: sequelize_1.DataTypes.TEXT
    }
}, {
    indexes: [
        {
            unique: false,
            fields: [exports.TypeOfProjectFields.contractorId]
        }
    ]
});
//# sourceMappingURL=cTypeOfProject.js.map