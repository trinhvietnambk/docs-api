"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
const Quotation_1 = require("./Quotation");
exports.QuotationDraftFields = {
    id: 'id',
    createdByUserId: 'createdByUserId',
    fromId: 'fromId',
    fromType: 'fromType',
    toId: 'toId',
    toType: 'toType',
    contentLetter: 'contentLetter',
    timeSendBidding: 'timeSendBidding',
    note: 'note',
    isSentBidding: 'isSentBidding',
    biddingStatus: 'biddingStatus',
    feelingStatus: 'feelingStatus',
    totalMoney: 'totalMoney',
    currency: 'currency',
    totalTime: 'totalTime',
    unitTime: 'unitTime',
    version: 'version',
    projectId: 'projectId',
    receiverId: 'receiverId',
    documents: 'documents',
    works: 'works',
    materials: 'materials',
    alternativeBid: 'alternativeBid',
    totalMoneyWorks: 'totalMoneyWorks',
    totalTimeWorks: 'totalTimeWorks',
    totalMoneyMaterials: 'totalMoneyMaterials',
};
exports.QuotationDraftAssociations = {};
exports.QuotationDraft = _Base_1.sequelize.define('quotationDraft', {
    [exports.QuotationDraftFields.id]: {
        type: sequelize_1.DataTypes.CHAR(50),
        primaryKey: true
    },
    [exports.QuotationDraftFields.createdByUserId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.QuotationDraftFields.fromId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.QuotationDraftFields.toId]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.QuotationDraftFields.fromType]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.QuotationDraftFields.toType]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.QuotationDraftFields.contentLetter]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.QuotationDraftFields.timeSendBidding]: {
        type: sequelize_1.DataTypes.DATE
    },
    [exports.QuotationDraftFields.note]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.QuotationDraftFields.isSentBidding]: {
        type: sequelize_1.DataTypes.BOOLEAN
    },
    [exports.QuotationDraftFields.biddingStatus]: {
        type: sequelize_1.DataTypes.CHAR(20)
    },
    [exports.QuotationDraftFields.totalMoney]: {
        type: sequelize_1.DataTypes.BIGINT
    },
    [exports.QuotationDraftFields.currency]: {
        type: sequelize_1.DataTypes.CHAR(20)
    },
    [exports.QuotationDraftFields.totalTime]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [exports.QuotationDraftFields.unitTime]: {
        type: sequelize_1.DataTypes.CHAR(20)
    },
    [exports.QuotationDraftFields.version]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [exports.QuotationDraftFields.projectId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.QuotationDraftFields.receiverId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.QuotationDraftFields.documents]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.QuotationDraftFields.documents, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.QuotationDraftFields.documents)) {
                return JSON.parse(this.getDataValue(exports.QuotationDraftFields.documents));
            }
        }
    },
    [exports.QuotationDraftFields.works]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.QuotationDraftFields.works, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.QuotationDraftFields.works)) {
                return JSON.parse(this.getDataValue(exports.QuotationDraftFields.works));
            }
        }
    },
    [exports.QuotationDraftFields.materials]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.QuotationDraftFields.materials, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.QuotationDraftFields.materials)) {
                return JSON.parse(this.getDataValue(exports.QuotationDraftFields.materials));
            }
        }
    },
    [exports.QuotationDraftFields.alternativeBid]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.QuotationDraftFields.alternativeBid, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.QuotationDraftFields.alternativeBid)) {
                return JSON.parse(this.getDataValue(exports.QuotationDraftFields.alternativeBid));
            }
        }
    },
    [exports.QuotationDraftFields.totalMoneyWorks]: {
        type: sequelize_1.DataTypes.BIGINT
    },
    [exports.QuotationDraftFields.totalTimeWorks]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [exports.QuotationDraftFields.totalMoneyMaterials]: {
        type: sequelize_1.DataTypes.BIGINT
    }
}, {
    indexes: [
        {
            unique: false,
            fields: [
                Quotation_1.QuotationFields.fromId,
            ]
        },
        {
            unique: false,
            fields: [
                Quotation_1.QuotationFields.fromType,
            ]
        },
        {
            unique: false,
            fields: [
                Quotation_1.QuotationFields.toId,
            ]
        },
        {
            unique: false,
            fields: [
                Quotation_1.QuotationFields.toType,
            ]
        },
        {
            unique: false,
            fields: [
                Quotation_1.QuotationFields.receiverId,
            ]
        },
    ]
});
//# sourceMappingURL=QuotationDraft.js.map