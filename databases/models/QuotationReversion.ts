import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';
import {BIDDING_STATUS, RECEIVE_STATUS, SENDING_STATUS} from "../../constants/database/Biddings";
import {CURRENCY_UNIT_VND, TIME_UNIT} from "../../constants/database/Common";
import {MathHelper} from "../../utities/MathHelper";
import {ProjectFields} from "./Project";
import {QuotationFields} from "./Quotation";

export const QuotationReversionFields = {
    id: 'id',
    createdByUserId: 'createdByUserId',
    fromId: 'fromId',
    fromType: 'fromType',
    toId: 'toId', // Đấu thầu cho thực thể nào, toId ở đây chính là ID hạng mục đấu thầu
    toType: 'toType', //hiện tại là hạng mục đấu thầu
    contentLetter: 'contentLetter',
    timeSendBidding: 'timeSendBidding',
    note: 'note',
    isSentBidding: 'isSentBidding',
    biddingStatus: 'biddingStatus',
    totalMoney: 'totalMoney',
    currency: 'currency',
    totalTime: 'totalTime',
    unitTime: 'unitTime',
    version: 'version',
    projectId: 'projectId',
    receiverId: 'receiverId',
    documents: 'documents',
    works: 'works',
    materials: 'materials',
    alternativeBid: 'alternativeBid',
    totalMoneyWorks: 'totalMoneyWorks',
    totalTimeWorks: 'totalTimeWorks',
    totalMoneyMaterials: 'totalMoneyMaterials',
    originId: 'originId',
};
export const QuotationReversionAssociations = {};
export const QuotationReversion = sequelize.define('quotationReversion',
    {
        [QuotationReversionFields.id]: {
            type: DataTypes.CHAR(50),
            primaryKey: true
        },
        [QuotationReversionFields.createdByUserId]: {
            type: DataTypes.CHAR(50)
        },
        [QuotationReversionFields.fromId]: {
            type: DataTypes.CHAR(50)
        },
        [QuotationReversionFields.toId]: {
            type: DataTypes.CHAR(100)
        },
        [QuotationReversionFields.fromType]: {
            type: DataTypes.CHAR(100)
        },
        [QuotationReversionFields.toType]: {
            type: DataTypes.CHAR(100)
        },
        [QuotationReversionFields.contentLetter]: {
            type: DataTypes.TEXT
        },
        [QuotationReversionFields.timeSendBidding]: {
            type: DataTypes.DATE
        },
        [QuotationReversionFields.note]: {
            type: DataTypes.TEXT
        },
        [QuotationReversionFields.isSentBidding]: {
            type: DataTypes.BOOLEAN
        },
        [QuotationReversionFields.biddingStatus]: {
            type: DataTypes.CHAR(20)
        },
        [QuotationReversionFields.totalMoney]: {
            type: DataTypes.BIGINT
        },
        [QuotationReversionFields.currency]: {
            type: DataTypes.CHAR(20)
        },
        [QuotationReversionFields.totalTime]: {
            type: DataTypes.INTEGER
        },
        [QuotationReversionFields.unitTime]: {
            type: DataTypes.CHAR(20)
        },
        [QuotationReversionFields.version]: {
            type: DataTypes.INTEGER
        },
        [QuotationReversionFields.projectId]: {
            type: DataTypes.CHAR(50)
        },
        [QuotationReversionFields.receiverId]: {
            type: DataTypes.CHAR(50)
        },
        [QuotationReversionFields.documents]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(QuotationReversionFields.documents, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(QuotationReversionFields.documents)) {
                    return JSON.parse(this.getDataValue(QuotationReversionFields.documents));
                }
            }
        },
        [QuotationReversionFields.works]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(QuotationReversionFields.works, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(QuotationReversionFields.works)) {
                    return JSON.parse(this.getDataValue(QuotationReversionFields.works));
                }
            }
        },
        [QuotationReversionFields.materials]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(QuotationReversionFields.materials, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(QuotationReversionFields.materials)) {
                    return JSON.parse(this.getDataValue(QuotationReversionFields.materials));
                }
            }
        },
        [QuotationReversionFields.alternativeBid]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(QuotationReversionFields.alternativeBid, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(QuotationReversionFields.alternativeBid)) {
                    return JSON.parse(this.getDataValue(QuotationReversionFields.alternativeBid));
                }
            }
        },
        [QuotationReversionFields.totalMoneyWorks]: {
            type: DataTypes.BIGINT
        },
        [QuotationReversionFields.totalTimeWorks]: {
            type: DataTypes.INTEGER
        },
        [QuotationReversionFields.totalMoneyMaterials]: {
            type: DataTypes.BIGINT
        },
        [QuotationReversionFields.originId]: {
            type: DataTypes.CHAR(50)
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [
                    QuotationFields.fromId,

                ]
            },
            {
                unique: false,
                fields: [

                    QuotationFields.fromType,

                ]
            },
            {
                unique: false,
                fields: [

                    QuotationFields.toId,

                ]
            },
            {
                unique: false,
                fields: [

                    QuotationFields.toType,
                ]
            },
            {
                unique: false,
                fields: [

                    QuotationFields.receiverId,
                ]
            },
            {
                unique: false,
                fields: [

                    QuotationReversionFields.originId
                ]
            },
        ]
    }
);