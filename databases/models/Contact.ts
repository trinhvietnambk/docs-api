import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';
import {ConnectFields} from "./Connect";

export const ContactFields = {
    id: 'id',
    userId: 'userId',
    name: 'name',
    firstName: 'firstName',
    lastName: 'lastName',
    email: 'email',
    avatar: 'avatar',
    phone: 'phone',
    companyTaxCode: 'companyTaxCode',
    position: "position"
};
export const ContactAssociations = {};
export const Contact = sequelize.define('contact',
    {
        [ContactFields.id]: {
            type: DataTypes.CHAR(50),
            primaryKey: true
        },
        [ContactFields.name]: {
            type: DataTypes.CHAR(100)
        },
        [ContactFields.firstName]: {
            type: DataTypes.CHAR(100)
        },
        [ContactFields.lastName]: {
            type: DataTypes.CHAR(100)
        },
        [ContactFields.email]: {
            type: DataTypes.CHAR(100)
        },
        [ContactFields.avatar]: {
            type: DataTypes.TEXT
        },
        [ContactFields.phone]: {
            type: DataTypes.CHAR(100)
        },
        [ContactFields.position]: {
            type: DataTypes.CHAR(100)
        },
        [ContactFields.companyTaxCode]: {
            type: DataTypes.CHAR(100)
        },
        [ContactFields.userId]: {
            type: DataTypes.CHAR(50)
        }
    },
    {
        indexes: [
            {unique: false, fields: [ContactFields.companyTaxCode]},
        ]
    }
);
