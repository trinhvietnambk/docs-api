"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
exports.ParticipantFields = {
    id: 'id',
    participantId: 'participantId',
    projectId: 'projectId',
    role: 'role',
    participantType: 'participantType',
    name: 'name',
    contacts: 'contacts'
};
exports.ParticipantAssociations = {};
exports.Participant = _Base_1.sequelize.define('participant', {
    [exports.ParticipantFields.id]: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    [exports.ParticipantFields.participantId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.ParticipantFields.participantType]: {
        type: sequelize_1.DataTypes.CHAR(50),
    },
    [exports.ParticipantFields.name]: {
        type: sequelize_1.DataTypes.CHAR(100),
    },
    [exports.ParticipantFields.contacts]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.ParticipantFields.contacts, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.ParticipantFields.contacts)) {
                return JSON.parse(this.getDataValue(exports.ParticipantFields.contacts));
            }
        }
    },
    [exports.ParticipantFields.projectId]: {
        type: sequelize_1.DataTypes.CHAR(50),
    }
}, {
    indexes: [
        {
            unique: false,
            fields: [exports.ParticipantFields.projectId]
        }
    ]
});
//# sourceMappingURL=Participant.js.map