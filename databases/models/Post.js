"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
exports.PostFields = {
    id: 'id',
    ownerId: 'ownerId',
    ownerType: 'ownerType',
    content: 'content',
    images: 'images',
    videos: 'videos',
    audios: 'audios',
    files: 'files',
    createdByUserId: 'createdByUserId',
    numberOfLikes: 'numberOfLikes',
    numberOfComments: 'numberOfComments',
    visibility: 'visibility'
};
exports.PostAssociations = {};
exports.Post = _Base_1.sequelize.define('post', {
    [exports.PostFields.id]: {
        type: sequelize_1.DataTypes.CHAR(50),
        primaryKey: true
    },
    [exports.PostFields.ownerId]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.PostFields.ownerType]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.PostFields.content]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.PostFields.images]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.PostFields.images, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.PostFields.images)) {
                return JSON.parse(this.getDataValue(exports.PostFields.images));
            }
        }
    },
    [exports.PostFields.videos]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.PostFields.videos, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.PostFields.videos)) {
                return JSON.parse(this.getDataValue(exports.PostFields.videos));
            }
        }
    },
    [exports.PostFields.files]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.PostFields.files, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.PostFields.files)) {
                return JSON.parse(this.getDataValue(exports.PostFields.files));
            }
        }
    },
    [exports.PostFields.audios]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.PostFields.audios, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.PostFields.audios)) {
                return JSON.parse(this.getDataValue(exports.PostFields.audios));
            }
        }
    },
    [exports.PostFields.createdByUserId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.PostFields.numberOfLikes]: {
        type: sequelize_1.DataTypes.INTEGER,
        defaultValue: 0,
    },
    [exports.PostFields.numberOfComments]: {
        type: sequelize_1.DataTypes.INTEGER,
        defaultValue: 0,
    },
    [exports.PostFields.visibility]: {
        type: sequelize_1.DataTypes.INTEGER
    },
}, {
    indexes: [
        {
            unique: false,
            fields: [
                exports.PostFields.ownerId,
            ]
        },
        {
            unique: false,
            fields: [
                exports.PostFields.ownerType
            ]
        },
        {
            method: 'BTREE',
            fields: ['createdAt']
        },
        {
            method: 'BTREE',
            fields: ['updatedAt']
        },
        {
            method: 'BTREE',
            fields: [exports.PostFields.visibility]
        }
    ]
});
//# sourceMappingURL=Post.js.map