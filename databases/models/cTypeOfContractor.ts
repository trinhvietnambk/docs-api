import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';

export const TypeOfContractorFields = {
    id: 'id',
    value: 'value',
    contractorId: 'contractorId',
};
export const TypeOfContractor = sequelize.define('cTypeOfContractor',
    {

        [TypeOfContractorFields.id]: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        [TypeOfContractorFields.value]: {
            type: DataTypes.TEXT
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [TypeOfContractorFields.contractorId]
            }
        ]
    }
);
