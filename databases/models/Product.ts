import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';
import {TextHelper} from "../../utities/TextHelper";
import {ContractorFields} from "./Contractor";

export const ProductFields = {
    id: 'id',
    name: "name",
    noSignName: "noSignName",
    images: "images",
    description: "description",
    price: "price",
    comparePrice: "comparePrice",
    isChargeTaxes: 'isChargeTaxes',
    isRequireShip: "isRequireShip",
    minDepth: "minDepth",
    maxDepth: "maxDepth",
    minWeight: "minWeight",
    maxWeight: "maxWeight",
    minWidth: "minWidth",
    maxWidth: "maxWidth",
    unitWeight: "unitWeight",
    unitWidth: "unitWidth",
    unitDepth: "unitDepth",
    inventorySKU: "inventorySKU",
    inventoryBarcode: "inventoryBarcode",
    inventoryPolicy: "inventoryPolicy",
    productType: "productType",
    vendor: "vendor",
    createdByUserId: "createdByUserId",
    ownerId: 'ownerId',
    ownerType: 'ownerType',
    numberOfSell: 'numberOfSell',
    brand: "brand",
    sale: "sale",
    variants: "variants",
    sizes: "sizes",
    colors: "colors",
    materials: "materials",
    tags: "tags",
    collections: "collections",

};
export const ProductAssociations = {};
export const Product = sequelize.define('product',
    {

        [ProductFields.id]: {
            type: DataTypes.CHAR(50),
            primaryKey: true
        },
        [ProductFields.name]: {
            type: DataTypes.TEXT
        },
        [ProductFields.noSignName]: {
            type: DataTypes.TEXT
        },
        [ProductFields.images]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(ProductFields.images, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(ProductFields.images)) {
                    return JSON.parse(this.getDataValue(ProductFields.images));
                }
            }
        },
        [ProductFields.description]: {
            type: DataTypes.TEXT
        },

        [ProductFields.price]: {
            type: DataTypes.BIGINT
        },
        [ProductFields.comparePrice]: {
            type: DataTypes.BIGINT
        },
        [ProductFields.isChargeTaxes]: {
            type: DataTypes.BOOLEAN
        },
        [ProductFields.isRequireShip]: {
            type: DataTypes.BOOLEAN
        },
        [ProductFields.minDepth]: {
            type: DataTypes.INTEGER
        },
        [ProductFields.maxDepth]: {
            type: DataTypes.INTEGER
        },
        [ProductFields.minWeight]: {
            type: DataTypes.INTEGER
        },
        [ProductFields.maxWeight]: {
            type: DataTypes.INTEGER
        },
        [ProductFields.minWidth]: {
            type: DataTypes.INTEGER
        },
        [ProductFields.maxWidth]: {
            type: DataTypes.INTEGER
        },
        [ProductFields.unitWeight]: {
            type: DataTypes.CHAR(50)
        },
        [ProductFields.unitWidth]: {
            type: DataTypes.CHAR(30)
        },
        [ProductFields.unitDepth]: {
            type: DataTypes.CHAR(30)
        },
        [ProductFields.inventorySKU]: {
            type: DataTypes.CHAR(50)
        },
        [ProductFields.inventoryBarcode]: {
            type: DataTypes.CHAR(100)
        },
        [ProductFields.inventoryPolicy]: {
            type: DataTypes.CHAR(100)
        },
        [ProductFields.productType]: {
            type: DataTypes.CHAR(100)
        },
        [ProductFields.numberOfSell]: {
            type: DataTypes.INTEGER
        },
        [ProductFields.vendor]: {
            type: DataTypes.CHAR(100)
        },
        [ProductFields.createdByUserId]: {
            type: DataTypes.CHAR(50)
        },
        [ProductFields.ownerId]: {
            type: DataTypes.CHAR(50)
        },
        [ProductFields.ownerType]: {
            type: DataTypes.CHAR(50)
        },
        [ProductFields.brand]: {
            type: DataTypes.CHAR(100)
        },
        [ProductFields.sale]: {
            type: DataTypes.INTEGER
        },
        [ProductFields.variants]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(ProductFields.variants, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(ProductFields.variants)) {
                    return JSON.parse(this.getDataValue(ProductFields.variants));
                }
            }
        }
    },
    {
        indexes: [
            // add a FULLTEXT index
            {type: 'FULLTEXT', fields: [ProductFields.noSignName]},
            {
                method: 'BTREE',
                fields: [ProductFields.price]
            },
            {
                method: 'BTREE',
                fields: [
                    ProductFields.minDepth]
            },
            {
                method: 'BTREE',
                fields: [
                    ProductFields.maxDepth
                ]
            },
            {
                method: 'BTREE',
                fields: [
                    ProductFields.minWeight]
            },
            {
                method: 'BTREE',
                fields: [
                    ProductFields.maxWeight
                ]
            },
            {
                method: 'BTREE',
                fields: [
                    ProductFields.minWidth]
            },
            {
                method: 'BTREE',
                fields: [
                    ProductFields.maxWidth
                ]
            },
            {
                method: 'BTREE',
                fields: [
                    ProductFields.sale
                ]
            },
            {
                unique: false,
                fields: [ProductFields.productType]
            },
            {
                unique: false,
                fields: [ProductFields.numberOfSell]
            },
            {
                unique: false,
                fields: [ProductFields.vendor]
            },
            {
                unique: false,
                fields: [ProductFields.createdByUserId]
            },
            {
                unique: false,
                fields: [
                    ProductFields.ownerId]
            },
            {
                unique: false,
                fields: [

                    ProductFields.ownerType]
            },
            {
                unique: false,
                fields: [
                    ProductFields.brand]
            },
            {
                method: 'BTREE',
                fields: ['createdAt']
            },
            {
                method: 'BTREE',
                fields: ['updatedAt']
            }

        ]
    }
);
Product.beforeSave((Product, options) => {
    if (Product[ProductFields.name]) {
        Product.noSignName = TextHelper.noSignViStringAndToLowerCase(Product[ProductFields.name]);
    }
});
Product.beforeBulkUpdate((query, instanse) => {
    if (query.attributes[ProductFields.name]) {
        query.fields.push(ProductFields.noSignName);
        query.attributes[ProductFields.noSignName] = TextHelper.noSignViStringAndToLowerCase(query.attributes[ProductFields.name]);
    }
});
