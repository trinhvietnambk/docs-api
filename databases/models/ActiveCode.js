"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
exports.ActiveCodeFields = {
    id: 'id',
    code: 'code',
    type: 'type',
    email: 'email',
    extraInfo: 'extraInfo'
};
exports.ActiveCodeAssociations = {};
exports.ActiveCode = _Base_1.sequelize.define('activecode', {
    [exports.ActiveCodeFields.id]: {
        type: sequelize_1.DataTypes.CHAR(50),
        primaryKey: true
    },
    [exports.ActiveCodeFields.code]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.ActiveCodeFields.type]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.ActiveCodeFields.email]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.ActiveCodeFields.extraInfo]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.ActiveCodeFields.extraInfo, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.ActiveCodeFields.extraInfo)) {
                return JSON.parse(this.getDataValue(exports.ActiveCodeFields.extraInfo));
            }
        }
    }
}, {
    indexes: [
        { unique: true, fields: [exports.ActiveCodeFields.code] },
        { unique: false, fields: [exports.ActiveCodeFields.email] },
        { unique: false, fields: [exports.ActiveCodeFields.type] }
    ]
});
//# sourceMappingURL=ActiveCode.js.map