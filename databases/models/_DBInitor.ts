
// User.hasMany(Project, {
//     foreignKey: {
//         fieldName: ProjectFields.createdByUserId,
//         foreignKeyConstraint: true,
//         as: UserFields.createdProjects,
//     }
// });

// User.hasMany(Project, {
//     foreignKey: ProjectFields.createdByUserId,
//     as: UserAssociations.createdProjects
// });
//
// Project.belongsTo(User, {
//     foreignKey: ProjectFields.createdByUserId,
//     targetKey: UserFields.id,
//     as: ProjectAsociations.createdByUser,
// });

// Project.hasOne(User, {foreignKey: ProjectFields.createdByUserId});
// Project.belongsTo(User);

// force: true will drop the table if it already exists

import {User, UserFields} from "./User";
export var syncDB = async function (){
    await User.sync({force: false});
};


// Project.sync({force: false}).then(() => {
//     // Table created
//     // return Project.create({
//     //     name: 'Ten du an',
//     //     about: 'Mo ta du an',
//     //     info: {prizes: 532432, dealine: new Date()}
//     // });
// });

// User.findAll({}).then(users => {
//     for (var i = 0; i < users.length; i++) {
//         var obj = users[i];
//         console.log(obj.toJSON())
//     }
// });

// User.create({
//     [UserFields.name]:'Trịnh Viết Nam',
    // [UserFields.info]:{
    //     email:'vietnam.trinh@ved.com.vn',
    //     phone:'0165 650 3482'
    // },
// });

// User.create({
//     [UserFields.name]:'Ngô Đình Phúc',
//     [UserFields.info]:{
//         email:'dinhphuc.ngo@ved.com.vn',
//         phone:'0932 322 124'
//     },
// });
//
// Project.create({
//     [ProjectFields.name]: 'DDTank đại chiến thế giới',
//     [ProjectFields.about]: 'Sự kiện diễn ra vào ngày 11/9 và quy tụ rất nhiều những thành viên có level cao trong hệ thống',
//     [ProjectFields.info]: {
//         dealine: '10/3/2018',
//         amout: '200 triệu',
//         hope: 10000
//     },
//     [ProjectFields.createdByUserId]: 1,
// });

// Project.create({
//     [ProjectFields.name]: 'LOL ra mắt tướng mới',
//     [ProjectFields.about]: 'Sự kiệm quan trọng',
//     [ProjectFields.info]: {
//         dealine: '30/3/2019',
//         amout: '500 triệu',
//         hope: 40000
//     },
//     [ProjectFields.createdByUserId]: 11
// });


// User.findAll({
//     include: [{
//         as: UserAssociations.createdProjects,
//         model: Project,
//         attributes: [ProjectFields.id, ProjectFields.name, ProjectFields.about]
//     }]
// }).then(users => {
//     console.log(JSON.stringify(users));
// });

// Project.findAll({
//     attributes:[ProjectFields.id,ProjectFields.name,ProjectFields.about],
//     include: [{model: User, as: ProjectAsociations.createdByUser, attributes: [UserFields.id, UserFields.name]}]
// }).then(projects => {
//     console.log(JSON.stringify(projects));
// });

// User.findOne({
//     attributes:[UserFields.id,UserFields.name,UserFields.info],
//     where: {
//         [UserFields.id]: 1
//     }
// }).then(user => {
//     console.log(user.toJSON());
// });
//
// async function check() {
//     var users = await User.findAll({include: [{model: Project, as: UserAssociations.createdProjects}]});
//     console.log(JSON.stringify(users));
//
// }

// async function getUsers() {
//     return new Promise((resolve, reject) => {
//         return User.findAll({where: {[UserFields.id]: 32}}).then(resolve).catch(reject);
//     });
// }
//
// var request = require('request');
//
// async function get(url) {
//     return new Promise((resolve, reject) => {
//         request(url, (err, res, body) => {
//             if (err) return reject(err);
//             resolve(body);
//         });
//     })
// }
//
// async function check() {
//     var users = await getUsers();
//     console.log(users);
// }
//
// async function testGet() {
//     var t1 = new Date().getTime();
//     try {
//         var body = await get('httpsf://vnexpress.net');
//     } catch (e) {
//         console.log(e);
//     }
//
//     var t2 = new Date().getTime();
//     console.log(t2 - t1);
//     console.log(body);
// }
// var redis = require("redis"),
//     client = redis.createClient();
// const {promisify} = require('util');
// const redisGet = promisify(client.get).bind(client);
// const redisSet = promisify(client.set).bind(client);
// // testGet();
// async function testRedis(){
//     await redisSet('key',123432);
//     var t = await redisGet('key');
//     console.log(t);
// }
//
// // client.set("string key", "string val", redis.print);
// testRedis();