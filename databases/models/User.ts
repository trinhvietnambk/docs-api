import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';
import {TextHelper} from "../../utities/TextHelper";
import {FavoriteItemFields} from "./FavoriteItem";

export const UserFields = {
    id: 'id',
    facebookId: 'facebookId',
    googleId: 'googleId',
    name: 'name',
    firstName: 'firstName',
    lastName: 'lastName',
    avatar: 'avatar',
    about: 'about',
    cover: 'cover',
    address: 'address',
    district: 'district',
    city: 'city',
    country: 'country',
    email: 'email',
    phone: 'phone',
    isValidatedPhone: 'isValidatedPhone',
    isValidatedEmail: 'isValidatedEmail',
    language: 'language',
    accountStatus: 'accountStatus', //VIP or FREE
    password: 'password', //MD5 password text
    keyJWT: 'keyJWT',
    companyId: 'companyId',// contractor
    isSuperAdmin: 'isSuperAdmin',
    actived: 'actived',
    //JSON or Array need convert to string before save to DB
    type: 'type',// array of project_owner, contractor,supplier

    //counter fields
    numberOfFollowing: 'numberOfFollowing',

    //noSign prefix for extra field to support search
    noSignName: 'noSignName',

    //associations fields of user
    createdProjects: 'createdProjects',
    paymentMethods: 'paymentMethods',
    mapRoles: 'mapRoles',
    mapTypes: 'mapTypes'
};
export const UserAssociations = {
    [UserFields.createdProjects]: UserFields.createdProjects,
    [UserFields.paymentMethods]: UserFields.paymentMethods
};
export const User = sequelize.define('user',
    {
        [UserFields.id]: {
            type: DataTypes.CHAR(50),
            primaryKey: true
        },
        [UserFields.facebookId]: {
            type: DataTypes.TEXT
        },
        [UserFields.googleId]: {
            type: DataTypes.TEXT
        },
        [UserFields.name]: {
            type: DataTypes.TEXT
        },
        [UserFields.firstName]: {
            type: DataTypes.TEXT
        },
        [UserFields.lastName]: {
            type: DataTypes.TEXT
        },
        [UserFields.avatar]: {
            type: DataTypes.TEXT
        },
        [UserFields.about]: {
            type: DataTypes.TEXT
        },
        [UserFields.cover]: {
            type: DataTypes.TEXT
        },
        [UserFields.address]: {
            type: DataTypes.TEXT
        },
        [UserFields.district]: {
            type: DataTypes.TEXT
        },
        [UserFields.city]: {
            type: DataTypes.TEXT
        },
        [UserFields.country]: {
            type: DataTypes.TEXT
        },
        [UserFields.email]: {
            type: DataTypes.CHAR(50)
        },
        [UserFields.phone]: {
            type: DataTypes.CHAR(20)
        },
        [UserFields.isValidatedPhone]: {
            type: DataTypes.BOOLEAN
        },
        [UserFields.isValidatedEmail]: {
            type: DataTypes.BOOLEAN
        },
        [UserFields.language]: {
            type: DataTypes.CHAR(20)
        },
        [UserFields.accountStatus]: {
            type: DataTypes.ENUM,
            values: ['VIP', 'FREE'],
            defaultValue: 'FREE'
        },
        [UserFields.password]: {
            type: DataTypes.TEXT
        },
        [UserFields.keyJWT]: {
            type: DataTypes.TEXT
        },
        [UserFields.companyId]: {
            type: DataTypes.CHAR(50)
        },
        [UserFields.actived]: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        [UserFields.type]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(UserFields.type, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(UserFields.type)) {
                    return JSON.parse(this.getDataValue(UserFields.type));
                }
            }
        },
        [UserFields.noSignName]: {
            type: DataTypes.TEXT
        }
        , [UserFields.isSuperAdmin]: {
            type: DataTypes.BOOLEAN
        }
        , [UserFields.numberOfFollowing]: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [
                    UserFields.email
                ]
            }
        ]
    }
);
User.beforeSave((user, options) => {
    if (user[UserFields.name]) {
        user.noSignName = TextHelper.noSignViStringAndToLowerCase(user[UserFields.name]);
    }
});
User.beforeBulkUpdate((query, instanse) => {
    if (query.attributes[UserFields.name]) {
        query.fields.push(UserFields.noSignName);
        query.attributes[UserFields.noSignName] = TextHelper.noSignViStringAndToLowerCase(query.attributes[UserFields.name]);
    }
});


// User.sync({force: true}).then(() => {
//     // Table created
//     return User.create({
//         firstName: 'John',
//         lastName: 'Hancock',
//         info: {email: '123', phone: 'asg52354235'}
//     });
// });
// User.create({
//     firstName: 'John',
//     lastName: 'Hancock',
//     info: {email: '123', phone: 'asg52354235'}
// });
// User.update(
//     {[UserFields.firstName]: 'nam', [UserFields.lastName]: 'trinh viet'},
//     {where: {id: 1}}
// ).then((err) => {
//     console.log(err)
// });

// User.findOne({attributes: [UserFields.id, UserFields.firstName]}).then(users => {
//     console.log(users.dataValues);
// })