"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
const TextHelper_1 = require("../../utities/TextHelper");
exports.UserFields = {
    id: 'id',
    facebookId: 'facebookId',
    googleId: 'googleId',
    name: 'name',
    firstName: 'firstName',
    lastName: 'lastName',
    avatar: 'avatar',
    about: 'about',
    cover: 'cover',
    address: 'address',
    district: 'district',
    city: 'city',
    country: 'country',
    email: 'email',
    phone: 'phone',
    isValidatedPhone: 'isValidatedPhone',
    isValidatedEmail: 'isValidatedEmail',
    language: 'language',
    accountStatus: 'accountStatus',
    password: 'password',
    keyJWT: 'keyJWT',
    companyId: 'companyId',
    isSuperAdmin: 'isSuperAdmin',
    actived: 'actived',
    //JSON or Array need convert to string before save to DB
    type: 'type',
    //counter fields
    numberOfFollowing: 'numberOfFollowing',
    //noSign prefix for extra field to support search
    noSignName: 'noSignName',
    //associations fields of user
    createdProjects: 'createdProjects',
    paymentMethods: 'paymentMethods',
    mapRoles: 'mapRoles',
    mapTypes: 'mapTypes'
};
exports.UserAssociations = {
    [exports.UserFields.createdProjects]: exports.UserFields.createdProjects,
    [exports.UserFields.paymentMethods]: exports.UserFields.paymentMethods
};
exports.User = _Base_1.sequelize.define('user', {
    [exports.UserFields.id]: {
        type: sequelize_1.DataTypes.CHAR(50),
        primaryKey: true
    },
    [exports.UserFields.facebookId]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.UserFields.googleId]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.UserFields.name]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.UserFields.firstName]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.UserFields.lastName]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.UserFields.avatar]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.UserFields.about]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.UserFields.cover]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.UserFields.address]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.UserFields.district]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.UserFields.city]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.UserFields.country]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.UserFields.email]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.UserFields.phone]: {
        type: sequelize_1.DataTypes.CHAR(20)
    },
    [exports.UserFields.isValidatedPhone]: {
        type: sequelize_1.DataTypes.BOOLEAN
    },
    [exports.UserFields.isValidatedEmail]: {
        type: sequelize_1.DataTypes.BOOLEAN
    },
    [exports.UserFields.language]: {
        type: sequelize_1.DataTypes.CHAR(20)
    },
    [exports.UserFields.accountStatus]: {
        type: sequelize_1.DataTypes.ENUM,
        values: ['VIP', 'FREE'],
        defaultValue: 'FREE'
    },
    [exports.UserFields.password]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.UserFields.keyJWT]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.UserFields.companyId]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.UserFields.actived]: {
        type: sequelize_1.DataTypes.BOOLEAN,
        defaultValue: true
    },
    [exports.UserFields.type]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.UserFields.type, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.UserFields.type)) {
                return JSON.parse(this.getDataValue(exports.UserFields.type));
            }
        }
    },
    [exports.UserFields.noSignName]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.UserFields.isSuperAdmin]: {
        type: sequelize_1.DataTypes.BOOLEAN
    },
    [exports.UserFields.numberOfFollowing]: {
        type: sequelize_1.DataTypes.INTEGER,
        defaultValue: 0,
    }
}, {
    indexes: [
        {
            unique: false,
            fields: [
                exports.UserFields.email
            ]
        }
    ]
});
exports.User.beforeSave((user, options) => {
    if (user[exports.UserFields.name]) {
        user.noSignName = TextHelper_1.TextHelper.noSignViStringAndToLowerCase(user[exports.UserFields.name]);
    }
});
exports.User.beforeBulkUpdate((query, instanse) => {
    if (query.attributes[exports.UserFields.name]) {
        query.fields.push(exports.UserFields.noSignName);
        query.attributes[exports.UserFields.noSignName] = TextHelper_1.TextHelper.noSignViStringAndToLowerCase(query.attributes[exports.UserFields.name]);
    }
});
// User.sync({force: true}).then(() => {
//     // Table created
//     return User.create({
//         firstName: 'John',
//         lastName: 'Hancock',
//         info: {email: '123', phone: 'asg52354235'}
//     });
// });
// User.create({
//     firstName: 'John',
//     lastName: 'Hancock',
//     info: {email: '123', phone: 'asg52354235'}
// });
// User.update(
//     {[UserFields.firstName]: 'nam', [UserFields.lastName]: 'trinh viet'},
//     {where: {id: 1}}
// ).then((err) => {
//     console.log(err)
// });
// User.findOne({attributes: [UserFields.id, UserFields.firstName]}).then(users => {
//     console.log(users.dataValues);
// })
//# sourceMappingURL=User.js.map