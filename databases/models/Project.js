"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
const TextHelper_1 = require("../../utities/TextHelper");
exports.ProjectFields = {
    id: 'id',
    name: 'name',
    description: 'description',
    documents: 'documents',
    listImages: 'listImages',
    listVideos: 'listVideos',
    projectType: 'projectType',
    projectSubType: 'projectSubType',
    workType: 'workType',
    address: 'address',
    village: 'village',
    district: 'district',
    city: 'city',
    country: 'country',
    minBudget: 'minBudget',
    maxBudget: 'maxBudget',
    investmentType: 'investmentType',
    minConstructionArea: 'minConstructionArea',
    maxConstructionArea: 'maxConstructionArea',
    minLandArea: 'minLandArea',
    maxLandArea: 'maxLandArea',
    totalFloorArea: 'totalFloorArea',
    state: 'state',
    status: 'status',
    minStartTime: 'minStartTime',
    maxStartTime: 'maxStartTime',
    minFinishTime: 'minFinishTime',
    maxFinishTime: 'maxFinishTime',
    purpose: 'purpose',
    createdByUserId: 'createdByUserId',
    approved: 'approved',
    currencyUnit: 'currencyUnit',
    //noSign prefix for extra field to support search
    noSignName: 'noSignName',
    //counter
    numberOfFollower: 'numberOfLikes',
    numberOfLikes: 'numberOfLikes',
    //associations fields of Project
    createdByUser: 'createdByUser',
    participants: 'participants',
    biddingCategories: 'biddingCategories'
};
exports.ProjectAssociations = {
    [exports.ProjectFields.createdByUser]: exports.ProjectFields.createdByUser,
    [exports.ProjectFields.participants]: exports.ProjectFields.participants,
};
exports.Project = _Base_1.sequelize.define('project', {
    [exports.ProjectFields.id]: {
        type: sequelize_1.DataTypes.CHAR(50),
        primaryKey: true
    },
    [exports.ProjectFields.name]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.ProjectFields.description]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.ProjectFields.documents]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.ProjectFields.documents, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.ProjectFields.documents)) {
                return JSON.parse(this.getDataValue(exports.ProjectFields.documents));
            }
        }
    },
    [exports.ProjectFields.listImages]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.ProjectFields.listImages, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.ProjectFields.listImages)) {
                return JSON.parse(this.getDataValue(exports.ProjectFields.listImages));
            }
        }
    },
    [exports.ProjectFields.listVideos]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(exports.ProjectFields.listVideos, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(exports.ProjectFields.listVideos)) {
                return JSON.parse(this.getDataValue(exports.ProjectFields.listVideos));
            }
        }
    },
    [exports.ProjectFields.projectType]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.ProjectFields.projectSubType]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.ProjectFields.workType]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.ProjectFields.address]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.ProjectFields.village]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.ProjectFields.district]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.ProjectFields.city]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.ProjectFields.country]: {
        type: sequelize_1.DataTypes.CHAR(50)
    },
    [exports.ProjectFields.minBudget]: {
        type: sequelize_1.DataTypes.REAL
    },
    [exports.ProjectFields.maxBudget]: {
        type: sequelize_1.DataTypes.REAL
    },
    [exports.ProjectFields.investmentType]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.ProjectFields.minConstructionArea]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [exports.ProjectFields.maxConstructionArea]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [exports.ProjectFields.minLandArea]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [exports.ProjectFields.maxLandArea]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [exports.ProjectFields.totalFloorArea]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [exports.ProjectFields.minStartTime]: {
        type: sequelize_1.DataTypes.DATE
    },
    [exports.ProjectFields.maxStartTime]: {
        type: sequelize_1.DataTypes.DATE
    },
    [exports.ProjectFields.minFinishTime]: {
        type: sequelize_1.DataTypes.DATE
    },
    [exports.ProjectFields.maxFinishTime]: {
        type: sequelize_1.DataTypes.DATE
    },
    [exports.ProjectFields.purpose]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.ProjectFields.noSignName]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.ProjectFields.numberOfFollower]: {
        type: sequelize_1.DataTypes.INTEGER,
        defaultValue: 0,
    },
    [exports.ProjectFields.numberOfLikes]: {
        type: sequelize_1.DataTypes.INTEGER,
        defaultValue: 0,
    },
    [exports.ProjectFields.status]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.ProjectFields.state]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.ProjectFields.approved]: {
        type: sequelize_1.DataTypes.BOOLEAN
    }
}, {
    indexes: [
        // add a FULLTEXT index
        { type: 'FULLTEXT', fields: [exports.ProjectFields.noSignName] },
        {
            method: 'BTREE', fields: [
                exports.ProjectFields.maxFinishTime
            ]
        },
        {
            method: 'BTREE', fields: [
                exports.ProjectFields.minFinishTime,
            ]
        },
        {
            method: 'BTREE', fields: [
                exports.ProjectFields.maxStartTime,
            ]
        },
        {
            method: 'BTREE', fields: [
                exports.ProjectFields.minStartTime,
            ]
        },
        {
            method: 'BTREE', fields: [
                exports.ProjectFields.minLandArea,
            ]
        },
        {
            method: 'BTREE', fields: [
                exports.ProjectFields.maxLandArea
            ]
        },
        {
            method: 'BTREE', fields: [
                exports.ProjectFields.maxBudget,
            ]
        },
        {
            method: 'BTREE', fields: [
                exports.ProjectFields.minBudget,
            ]
        },
        {
            method: 'BTREE', fields: [
                exports.ProjectFields.minConstructionArea,
            ]
        },
        {
            method: 'BTREE', fields: [
                exports.ProjectFields.maxConstructionArea,
            ]
        },
        {
            unique: false, fields: [
                exports.ProjectFields.createdByUserId
            ]
        },
        {
            unique: false, fields: [
                exports.ProjectFields.totalFloorArea
            ]
        }, {
            unique: false, fields: [
                exports.ProjectFields.country,
            ]
        },
        {
            unique: false, fields: [
                exports.ProjectFields.district,
            ]
        }, {
            unique: false,
            fields: [
                exports.ProjectFields.approved,
            ]
        },
        {
            unique: false, fields: [
                exports.ProjectFields.city,
            ]
        },
        {
            unique: false, fields: [
                exports.ProjectFields.projectSubType,
            ]
        },
        {
            unique: false, fields: [
                exports.ProjectFields.projectType
            ]
        },
        {
            unique: false, fields: [
                exports.ProjectFields.workType,
            ]
        },
        {
            unique: false, fields: [
                exports.ProjectFields.status,
            ]
        },
        {
            unique: false, fields: [
                exports.ProjectFields.state,
            ]
        },
        {
            unique: false, fields: [
                exports.ProjectFields.investmentType
            ]
        },
        {
            method: 'BTREE',
            fields: ['createdAt']
        },
        {
            method: 'BTREE',
            fields: ['updatedAt']
        }
    ]
});
exports.Project.beforeSave((Project, options) => {
    if (Project[exports.ProjectFields.name]) {
        Project.noSignName = TextHelper_1.TextHelper.noSignViStringAndToLowerCase(Project[exports.ProjectFields.name]);
    }
});
exports.Project.beforeBulkUpdate((query, instanse) => {
    if (query.attributes[exports.ProjectFields.name]) {
        query.fields.push(exports.ProjectFields.noSignName);
        query.attributes[exports.ProjectFields.noSignName] = TextHelper_1.TextHelper.noSignViStringAndToLowerCase(query.attributes[exports.ProjectFields.name]);
    }
});
// User.sync({force: true}).then(() => {
//     // Table created
//     return User.create({
//         firstName: 'John',
//         lastName: 'Hancock',
//         info: {email: '123', phone: 'asg52354235'}
//     });
// });
// User.create({
//     firstName: 'John',
//     lastName: 'Hancock',
//     info: {email: '123', phone: 'asg52354235'}
// });
// User.update(
//     {[UserFields.firstName]: 'nam', [UserFields.lastName]: 'trinh viet'},
//     {where: {id: 1}}
// ).then((err) => {
//     console.log(err)
// });
// User.findOne({attributes: [UserFields.id, UserFields.firstName]}).then(users => {
//     console.log(users.dataValues);
// })
//# sourceMappingURL=Project.js.map