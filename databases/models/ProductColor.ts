import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';

export const ProductSizeFields = {
    id: 'id',
    value: 'value',
    productId: 'productId',
};
export const ProductColor = sequelize.define('productColor',
    {

        [ProductSizeFields.id]: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        [ProductSizeFields.value]: {
            type: DataTypes.CHAR(20)
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [ProductSizeFields.productId]
            }
        ]
    }
);
