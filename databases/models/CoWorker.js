"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
exports.CoWorkerFields = {
    id: 'id',
    companyId: 'companyId',
    memberId: 'memberId',
    role: 'role',
    status: 'status'
};
exports.CoWorkerAssociations = {};
exports.CoWorker = _Base_1.sequelize.define('coworker', {
    [exports.CoWorkerFields.id]: {
        type: sequelize_1.DataTypes.CHAR(50),
        primaryKey: true
    },
    [exports.CoWorkerFields.companyId]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.CoWorkerFields.memberId]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.CoWorkerFields.role]: {
        type: sequelize_1.DataTypes.CHAR(100)
    },
    [exports.CoWorkerFields.status]: {
        type: sequelize_1.DataTypes.CHAR(100)
    }
}, {
    indexes: [
        {
            unique: false,
            fields: [exports.CoWorkerFields.companyId]
        },
        {
            unique: false,
            fields: [exports.CoWorkerFields.status]
        },
        {
            unique: false,
            fields: [exports.CoWorkerFields.role]
        }
    ]
});
//# sourceMappingURL=CoWorker.js.map