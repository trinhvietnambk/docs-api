"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
exports.TypeOfConstructionFields = {
    id: 'id',
    value: 'value',
    contractorId: 'contractorId',
};
exports.TypeOfConstruction = _Base_1.sequelize.define('cTypeOfConstruction', {
    [exports.TypeOfConstructionFields.id]: {
        type: sequelize_1.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    [exports.TypeOfConstructionFields.value]: {
        type: sequelize_1.DataTypes.TEXT
    }
}, {
    indexes: [
        {
            unique: false,
            fields: [exports.TypeOfConstructionFields.contractorId]
        }
    ]
});
//# sourceMappingURL=cTypeOfConstruction.js.map