"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
exports.RoleFields = {
    id: 'id',
    subjectId: 'subjectId',
    subjectType: 'subjectType',
    objectId: 'objectId',
    objectType: 'objectType',
    role: 'role'
};
exports.RoleAssociations = {
    createdProjects: 'createdProjects'
};
exports.Role = _Base_1.sequelize.define('role', {
    [exports.RoleFields.subjectId]: {
        type: sequelize_1.DataTypes.CHAR,
        primaryKey: true
    },
    [exports.RoleFields.subjectType]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.RoleFields.objectId]: {
        type: sequelize_1.DataTypes.TEXT,
    },
    [exports.RoleFields.objectType]: {
        type: sequelize_1.DataTypes.TEXT,
    },
    [exports.RoleFields.role]: {
        type: sequelize_1.DataTypes.TEXT
    }
});
//# sourceMappingURL=_Role.js.map