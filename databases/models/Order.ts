import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';

export const OrderFields = {
    id: 'id',
    userId: 'userId',
    status: 'status',
    cart: 'cart',
    shipInfo: 'shipInfo'
};
export const OrderAssociations = {};
export const Order = sequelize.define('order',
    {

        [OrderFields.id]: {
            type: DataTypes.CHAR(50),
            primaryKey: true
        },
        [OrderFields.userId]: {
            type: DataTypes.CHAR(50)
        },
        [OrderFields.status]: {
            type: DataTypes.CHAR(150),
            defaultValue:'WAITNG'
        },
        [OrderFields.shipInfo]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(OrderFields.shipInfo, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(OrderFields.shipInfo)) {
                    return JSON.parse(this.getDataValue(OrderFields.shipInfo));
                }
            }
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [OrderFields.userId]
            },
            {
                unique: false,
                fields: [OrderFields.status]
            }
        ]
    }
);
