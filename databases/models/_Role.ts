import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize'
export const RoleFields = {
    id:'id',
    subjectId: 'subjectId',
    subjectType: 'subjectType',
    objectId: 'objectId',
    objectType: 'objectType',
    role: 'role'
};
export const RoleAssociations = {
    createdProjects: 'createdProjects'
};
export const Role = sequelize.define('role',
    {
        [RoleFields.subjectId]: {
            type: DataTypes.CHAR,
            primaryKey: true
        },
        [RoleFields.subjectType]: {
            type: DataTypes.TEXT
        },
        [RoleFields.objectId]: {
            type: DataTypes.TEXT,
        },
        [RoleFields.objectType]: {
            type: DataTypes.TEXT,
        },
        [RoleFields.role]: {
            type: DataTypes.TEXT
        }
    }
);
