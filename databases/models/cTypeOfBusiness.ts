import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';

export const TypeOfBusinessFields = {
    id: 'id',
    value: 'value',
    contractorId: 'contractorId',
};
export const TypeOfBusiness = sequelize.define('cTypeOfBusiness',
    {

        [TypeOfBusinessFields.id]: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        [TypeOfBusinessFields.value]: {
            type: DataTypes.TEXT
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [TypeOfBusinessFields.contractorId]
            }
        ]
    }
);
