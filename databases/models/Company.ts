import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';
import {WGET} from "../../lib/WGET";
import {TextHelper} from "../../utities/TextHelper";

export const CompanyFields = {
    url: 'url',
    maDoanhNghiep: 'maDoanhNghiep',
    tenChinhThuc: 'tenChinhThuc',
    noSignTenChinhThuc: 'noSignTenChinhThuc',
    loaiHinhCongTy: 'loaiHinhCongTy',
    tenGiaoDich: 'tenGiaoDich',
    ngayCap: 'ngayCap',
    coQuanThueQuanLy: 'coQuanThueQuanLy',
    ngayBatDauHoatDong: 'ngayBatDauHoatDong',
    trangThai: 'trangThai',
    diaChiTruSo: 'diaChiTruSo',
    quanHuyen: 'quanHuyen',
    tinhThanh: 'tinhThanh',
    dienThoai: 'dienThoai',
    fax: 'fax',
    email: 'email',
    website: 'website',
    nguoiDaiDien: 'nguoiDaiDien',
    dienThoaiNguoiDaiDien: 'dienThoaiNguoiDaiDien',
    diaChiNguoiDaiDien: 'diaChiNguoiDaiDien',
    giamDoc: 'giamDoc',
    dienThoaiGiamDoc: 'dienThoaiGiamDoc',
    diaChiGiamDoc: 'diaChiGiamDoc',
    keToan: 'keToan',
    dienThoaiKeToan: 'dienThoaiKeToan',
    diaChiKeToan: 'diaChiKeToan',
    nganhNgheChinh: 'nganhNgheChinh',
    linhVucKinhTe: 'linhVucKinhTe',
    loaiHinhKinhTe: 'loaiHinhKinhTe',
    loaiHinhToChuc: 'loaiHinhToChuc',
    capChuong: 'capChuong',
    loaiKhoan: 'loaiKhoan',
};
export const LocationAssociations = {};
export const Company = sequelize.define('company',
    {
        [CompanyFields.url]: {
            type: DataTypes.STRING(150),
            primaryKey: true
        },
        [CompanyFields.maDoanhNghiep]: {
            type: DataTypes.CHAR(50)
        },
        [CompanyFields.tenChinhThuc]: {
            type: DataTypes.CHAR(150)
        },
        [CompanyFields.noSignTenChinhThuc]: {
            type: DataTypes.CHAR(150)
        },
        [CompanyFields.loaiHinhCongTy]: {
            type: DataTypes.TEXT
        },
        [CompanyFields.tenGiaoDich]: {
            type: DataTypes.TEXT
        },
        [CompanyFields.ngayCap]: {
            type: DataTypes.TEXT
        }
        ,
        [CompanyFields.coQuanThueQuanLy]: {
            type: DataTypes.TEXT
        }
        ,
        [CompanyFields.ngayBatDauHoatDong]: {
            type: DataTypes.TEXT
        }
        ,
        [CompanyFields.trangThai]: {
            type: DataTypes.TEXT
        }
        ,
        [CompanyFields.diaChiTruSo]: {
            type: DataTypes.TEXT
        }
        ,
        [CompanyFields.quanHuyen]: {
            type: DataTypes.TEXT
        }
        ,
        [CompanyFields.tinhThanh]: {
            type: DataTypes.TEXT
        },
        [CompanyFields.dienThoai]: {
            type: DataTypes.TEXT
        },
        [CompanyFields.fax]: {
            type: DataTypes.TEXT
        },
        [CompanyFields.email]: {
            type: DataTypes.TEXT
        },
        [CompanyFields.website]: {
            type: DataTypes.TEXT
        },
        [CompanyFields.nguoiDaiDien]: {
            type: DataTypes.TEXT
        },
        [CompanyFields.dienThoaiNguoiDaiDien]: {
            type: DataTypes.TEXT
        },
        [CompanyFields.diaChiNguoiDaiDien]: {
            type: DataTypes.TEXT
        },
        [CompanyFields.giamDoc]: {
            type: DataTypes.TEXT
        },
        [CompanyFields.dienThoaiGiamDoc]: {
            type: DataTypes.TEXT
        },
        [CompanyFields.diaChiGiamDoc]: {
            type: DataTypes.TEXT
        }
        ,
        [CompanyFields.keToan]: {
            type: DataTypes.TEXT
        }
        ,
        [CompanyFields.dienThoaiKeToan]: {
            type: DataTypes.TEXT
        }
        ,
        [CompanyFields.diaChiKeToan]: {
            type: DataTypes.TEXT
        }
        ,
        [CompanyFields.nganhNgheChinh]: {
            type: DataTypes.TEXT
        },
        [CompanyFields.linhVucKinhTe]: {
            type: DataTypes.TEXT
        },
        [CompanyFields.loaiHinhKinhTe]: {
            type: DataTypes.TEXT
        },
        [CompanyFields.loaiHinhToChuc]: {
            type: DataTypes.TEXT
        },
        [CompanyFields.capChuong]: {
            type: DataTypes.TEXT
        },
        [CompanyFields.loaiKhoan]: {
            type: DataTypes.TEXT
        }
    },
    {
        indexes: [
            {method: 'BTREE', fields: [CompanyFields.maDoanhNghiep]},
            {unique: false, fields: [CompanyFields.tenChinhThuc]}
        ]
    }
);
export const HTMLPageXaPhuong = sequelize.define('htmlPageXaPhuong',
    {
        // id:{
        //     type:DataTypes.INTEGER,
        //     primaryKey:true,
        //     autoIncrement:true
        // },
        url: {
            type: DataTypes.STRING(150).BINARY,
            primaryKey: true
        },
        province: {
            type: DataTypes.TEXT
        },
        district: {
            type: DataTypes.TEXT
        },
        village: {
            type: DataTypes.TEXT
        },
        html: {
            type: DataTypes.TEXT
        }
    }
);
var cheerio = require('cheerio');
var fs = require('fs');

async function getListURLTinhThanh() {
    var html = await WGET.getHtml('https://vinabiz.org/categories/tinhthanh');
    var $ = cheerio.load(html);
    var tinhThanh = [];
    $('.btn.btn-labeled.btn-default.btn-block').each(function () {
        var province = $(this).find('b').text();
        var url = 'https://vinabiz.org' + $(this).attr('href');
        tinhThanh.push({
            province: province,
            url: url
        });
    });
    return tinhThanh;
}

async function getListUrlQuanHuyen(tinhThanh) {
    var quanHuyen = [];
    for (let item of tinhThanh) {
        var {province, url} = item;
        var $ = cheerio.load(await WGET.getHtml(url));
        $('.btn.btn-labeled.btn-default.btn-block').each(function () {
            var district = $(this).find('b').text();
            var url = 'https://vinabiz.org' + $(this).attr('href');
            quanHuyen.push({
                district: district,
                url: url,
                province: province
            });
        });
    }
    return quanHuyen;
}

async function getListUrlXaPhuong(quanHuyen) {
    var xaPhuong = [];
    for (let item of quanHuyen) {
        var {district, url, province} = item;
        var $ = cheerio.load(await WGET.getHtml(url));
        $('.btn.btn-labeled.btn-default.btn-block').each(function () {
            var village = $(this).find('b').text();
            var url = 'https://vinabiz.org' + $(this).attr('href');
            var numbser = $(this).find('.badge.pull-right').text();
            xaPhuong.push({
                village: village,
                url: url,
                province: province,
                district: district,
                numberCompany: numbser
            });
            console.log(village, numbser, url);
        });
    }
    return xaPhuong;
}

async function saveUrlPageOfXaPhuong(xaPhuong) {
    await HTMLPageXaPhuong.sync({force: false});
    for (let item of xaPhuong) {
        var {url, village, province, district, numberCompany} = item;
        var numberPage = Math.floor(numberCompany / 30);
        if ((numberCompany % 30) > 1) {
            numberPage = numberPage + 1;
        }
        for (var i = 1; i <= numberPage; i++) {
            var urlOfPage = url + '/' + i;
            console.log(urlOfPage);
            var html = null;
            await HTMLPageXaPhuong.create({
                village, province, district, url: urlOfPage, html
            });
        }
    }

}
var listItems = null;
async function crawlHTMLOfPageXaPhuong() {
    await Company.sync({force: false});
    while (true) {
        console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ');
        if(listItems == null){
            var listItems = await HTMLPageXaPhuong.findAll({
                where: {
                    html: null
                }
            });
        }
        var item = listItems.shift();

        // var item = await HTMLPageXaPhuong.findOne({
        //     where: {
        //         html: null
        //     },
        //     offset: Math.floor(Math.random() * 10)
        //     // ,
        //     // order: [
        //     //     Sequelize.fn('RAND'),
        //     // ]
        // });
        console.log('yyyyyyyyyyyyyyyyyyyyyyy ');
        if (!item) {
            console.log('het phan tu null');
            break;
        }

        try {
            var html = await WGET.getHtml(item.url);
        } catch (e) {
            continue;
        }

        item.html = html;
        var $ = cheerio.load(item.html);
        var urlCompanies = [];
        $('.col-md-12.padding-left-0').each(function () {
            var url = 'https://vinabiz.org/' + $(this).find('a').attr('href');
            urlCompanies.push(url);
        });
        for (let url of urlCompanies) {
            try {
                await Company.create({
                    url: url
                });
            } catch (e) {
                console.log('Bi break 1');
                break;
            }
        }
        try {
            await item.save();
        } catch (e) {
            console.log('Bi break 2');
        }
        ;

    }
}

function decodeEmail(href) {
    if (!href) return null;
    var o = "/cdn-cgi/l/email-protection#";
    var a = href.indexOf(o);

    function r(e, t) {
        var r = e.substr(t, 2);
        return parseInt(r, 16);
    }

    function n(n, o) {
        for (var c = "", a = r(n, o), i = o + 2; i < n.length; i += 2) {
            var f = r(n, i) ^ a;
            c += String.fromCharCode(f);
        }
        try {
            c = decodeURIComponent(escape(c));
        } catch (l) {

        }
        return c;
    }

    var email = n(href, a + o.length);
    return email;
}

async function crawlerCompanyInfo() {
    while (true) {
        var item = await Company.findOne({
            attributes: ['url'],
            where: {
                // [CompanyFields.tenChinhThuc]: null
                [CompanyFields.maDoanhNghiep]: null
            },
            offset: Math.floor(Math.random() * 30)
            // ,
            // order: [
            //     Sequelize.fn('RAND'),
            // ]
        });
        if (!item) break;
        // console.log(html);
        try {
            var html = await WGET.getHtml(item.url);
            var $ = cheerio.load(html);
        } catch (e) {
            continue;
        }

        var $trs = $('table tr');
        var tenChinhThuc = $($trs[1]).find('td:nth-child(2)').text().trim();
        var loaiHinhCongTy = getTypeOfCompany(tenChinhThuc);
        var tenGiaoDich = $($trs[1]).find('td:nth-child(4)').text().trim();
        var maDoanhNghiep = $($trs[2]).find('td:nth-child(2)').text().trim();
        var ngayCap = $($trs[2]).find('td:nth-child(4)').text().trim();
        var coQuanThueQuanLy = $($trs[3]).find('td:nth-child(2)').text().trim();
        var ngayBatDauHoatDong = $($trs[3]).find('td:nth-child(4)').text().trim();
        var trangThai = $($trs[4]).find('td:nth-child(2)').text().trim();
        if ($($trs[4]).find('td:nth-child(2) .alert.alert-success.fade.in').length > 0) {
            var diaChiTruSo = $($trs[7]).find('td:nth-child(2)').text().trim();
            var quanHuyen = getDistrict(diaChiTruSo);
            var tinhThanh = getProvince(diaChiTruSo);
            var dienThoai = $($trs[8]).find('td:nth-child(2)').text().trim();
            var fax = $($trs[8]).find('td:nth-child(4)').text().trim();
            var email = decodeEmail($($trs[9]).find('td:nth-child(2) a').attr('href'));
            var website = $($trs[9]).find('td:nth-child(4)').text().trim();
            var nguoiDaiDien = $($trs[10]).find('td:nth-child(2)').text().trim();
            var dienThoaiNguoiDaiDien = $($trs[10]).find('td:nth-child(4)').text().trim();
            var diaChiNguoiDaiDien = $($trs[11]).find('td:nth-child(2)').text().trim();
            var giamDoc = $($trs[12]).find('td:nth-child(2)').text().trim();
            var dienThoaiGiamDoc = $($trs[12]).find('td:nth-child(4)').text().trim();
            var diaChiGiamDoc = $($trs[13]).find('td:nth-child(2)').text().trim();

            var ketToan = $($trs[14]).find('td:nth-child(2)').text().trim();
            var dienThoaiKeToan = $($trs[14]).find('td:nth-child(4)').text().trim();
            var diaChiKeToan = $($trs[15]).find('td:nth-child(2)').text().trim();

            var nghanhNgheChinh = $($trs[18]).find('td:nth-child(2)').text().trim();
            var linkVucKinhTe = $($trs[18]).find('td:nth-child(4)').text().trim();
            var loaiHinhKinhTe = $($trs[19]).find('td:nth-child(2)').text().trim();
            var loaiHinhToChuc = $($trs[19]).find('td:nth-child(4)').text().trim();
            var capChuong = $($trs[20]).find('td:nth-child(2)').text().trim();
            var loaiKhoan = $($trs[20]).find('td:nth-child(4)').text().trim();

            item[CompanyFields.tenChinhThuc] = tenChinhThuc,
                item[CompanyFields.loaiHinhCongTy] = loaiHinhCongTy,
                item[CompanyFields.tenGiaoDich] = tenGiaoDich,
                item[CompanyFields.maDoanhNghiep] = maDoanhNghiep,
                item[CompanyFields.ngayCap] = ngayCap,
                item[CompanyFields.coQuanThueQuanLy] = coQuanThueQuanLy,
                item[CompanyFields.ngayBatDauHoatDong] = ngayBatDauHoatDong,
                item[CompanyFields.trangThai] = trangThai,
                item[CompanyFields.diaChiTruSo] = diaChiTruSo,
                item[CompanyFields.quanHuyen] = quanHuyen,
                item[CompanyFields.tinhThanh] = tinhThanh,
                item[CompanyFields.dienThoai] = dienThoai,
                item[CompanyFields.fax] = fax,
                item[CompanyFields.email] = email,
                item[CompanyFields.website] = website,
                item[CompanyFields.nguoiDaiDien] = nguoiDaiDien,
                item[CompanyFields.dienThoaiNguoiDaiDien] = dienThoaiNguoiDaiDien,
                item[CompanyFields.diaChiNguoiDaiDien] = diaChiNguoiDaiDien,
                item[CompanyFields.giamDoc] = giamDoc,
                item[CompanyFields.dienThoaiGiamDoc] = dienThoaiGiamDoc,
                item[CompanyFields.diaChiGiamDoc] = diaChiGiamDoc,
                item[CompanyFields.keToan] = ketToan,
                item[CompanyFields.dienThoaiKeToan] = dienThoaiKeToan,
                item[CompanyFields.diaChiKeToan] = diaChiKeToan,
                item[CompanyFields.nganhNgheChinh] = nghanhNgheChinh,
                item[CompanyFields.linhVucKinhTe] = linkVucKinhTe,
                item[CompanyFields.loaiHinhKinhTe] = loaiHinhKinhTe,
                item[CompanyFields.loaiHinhToChuc] = loaiHinhToChuc,
                item[CompanyFields.capChuong] = capChuong,
                item[CompanyFields.loaiKhoan] = loaiKhoan;
            try {
                await item.save();
            } catch (e) {
                await item.destroy();
            }

            // console.log(item.toJSON());
        } else {
            await item.destroy();
        }
    }
    // item.html = html;
    // await item.save();

}

function getProvince(address) {
    var split = address.split(',');
    var t = split.pop();
    // t = TextHelper.noSignViString(t);
    t = TextHelper.titleCase(t);
    t = t.replace('Tp ', 'Thành Phố ');
    t = t.replace('Tx ', 'Thị Xã ');
    t = t.replace('Thi Xa ', 'Thị Xã ');
    return t;
}

function getDistrict(address) {
    var split = address.split(',');
    split.pop();
    var t = split.pop();
    if (!t) return '';
    t = t.trim();
    split = t.split(' ');
    if (split.length > 2 && split[0] == 'Huyện') {
        // split.shift();
    } else if (split.length > 2 && split[0] == 'huyện') {
        // split.shift();
        split[0] = 'Huyện';
    } else if (split.length > 2 && split[0] == 'Quận') {
        split.shift();
    } else if (split.length > 2 && split[0] == 'quận') {
        // split.shift();
        split[0] = 'Quận';
    }
    // t = TextHelper.noSignViString(split.join(' '));
    t = TextHelper.titleCase(t);
    t = t.replace('Tp ', 'Thành Phố ');
    t = t.replace('Tx ', 'Thị Xã ');
    t = t.replace('Thi Xa ', 'Thị Xã ');
    return t;
}

const COMPANY_TYPE = {
    'JSC': 'Join Stock Company',
    'CL': 'Company Limited',
    'OMC': 'One Member Co.,Ltd',
    'RO': 'Representative Office'
};

function getTypeOfCompany(name) {
    name = name.toLowerCase();
    console.log(name);
    if (name.indexOf('trách nhiệm hữu hạn MTV') >= 0) return COMPANY_TYPE.OMC;
    if (name.indexOf('tnhh một thành viên') >= 0) return COMPANY_TYPE.OMC;
    if (name.indexOf('trách nhiệm hữu hạn 1 thành viên') >= 0) return COMPANY_TYPE.OMC;
    if (name.indexOf('văn phòng đại diện') >= 0) return COMPANY_TYPE.RO;
    if (name.indexOf('trách nhiệm hữu hạn một thành viên') >= 0) return COMPANY_TYPE.OMC;
    if (name.indexOf('trách nhiệm hữu hạn') >= 0) return COMPANY_TYPE.CL;
    if (name.indexOf('tnhh mtv') >= 0) return COMPANY_TYPE.OMC;
    if (name.indexOf('vpđd') >= 0) return COMPANY_TYPE.RO;
    if (name.indexOf('cổ phần') >= 0) return COMPANY_TYPE.JSC;
    if (name.indexOf('ctcp') >= 0) return COMPANY_TYPE.JSC;
    if (name.indexOf('tnhh') >= 0) return COMPANY_TYPE.CL;
}

async function initDataCompanies() {
    // var tinhThanh = await getListURLTinhThanh();
    // var quanHuyen = await getListUrlQuanHuyen(tinhThanh);
    // var xaphuong = await getListUrlXaPhuong(quanHuyen);
    // fs.writeFileSync('xa-phuong.txt', JSON.stringify(xaphuong), function (error) {
    //     console.log(error);
    // });
    // var xaPhuong = JSON.parse(fs.readFileSync('xa-phuong.txt'));
    // await saveUrlPageOfXaPhuong(xaPhuong);
    try {
        for (var i = 0; i < 1; i++) {
            crawlHTMLOfPageXaPhuong();
        }
    } catch (e) {

    }
    try {
        for (var i = 0; i < 20; i++) {
            crawlerCompanyInfo();
        }

    } catch (e) {

    }

    // crawlerCompanyInfo();
}

async function createNoSignField(){
    var offset = 243200;
    while(true){
        var companies = await Company.findAll({
            offset:offset,
            limit: 100
        });
        for(var c of companies){
            if(c[CompanyFields.noSignTenChinhThuc]){
                continue;
            }
            c[CompanyFields.noSignTenChinhThuc] = TextHelper.noSignViStringAndToLowerCase(c[CompanyFields.tenChinhThuc]);
            await c.save();
        }
        console.log(companies.length);
        offset += 100;
    }
}

// (async function () {
//     await HTMLPageXaPhuong.sync({force: false});
//     await Company.sync({force: false});
// })();
// initDataCompanies();

// createNoSignField();