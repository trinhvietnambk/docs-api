import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';

export const ParticipantFields = {
    id: 'id',
    participantId: 'participantId',
    projectId: 'projectId',
    role: 'role',
    participantType: 'participantType',
    name: 'name',
    contacts: 'contacts'
};
export const ParticipantAssociations = {};
export const Participant = sequelize.define('participant',
    {
        [ParticipantFields.id]: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        [ParticipantFields.participantId]: {
            type: DataTypes.CHAR(50)
        },
        [ParticipantFields.participantType]: {
            type: DataTypes.CHAR(50),
        },
        [ParticipantFields.name]: {
            type: DataTypes.CHAR(100),
        },
        [ParticipantFields.contacts]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(ParticipantFields.contacts, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(ParticipantFields.contacts)) {
                    return JSON.parse(this.getDataValue(ParticipantFields.contacts));
                }
            }
        },
        [ParticipantFields.projectId]: {
            type: DataTypes.CHAR(50),
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [ParticipantFields.projectId]
            }
        ]
    }
);
