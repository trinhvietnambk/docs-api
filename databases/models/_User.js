"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
const TextHelper_1 = require("../../utities/TextHelper");
exports.UserFields = {
    id: 'id',
    name: 'name',
    info: 'info',
    noSignName: 'noSignName',
    createdProjects: 'createdProjects'
};
exports.UserAssociations = {
    createdProjects: 'createdProjects'
};
exports.User = _Base_1.sequelize.define('user', {
    [exports.UserFields.id]: {
        type: sequelize_1.DataTypes.CHAR,
        primaryKey: true
    },
    [exports.UserFields.name]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [exports.UserFields.info]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            console.log('aaaaaaaaaaa ', val);
            this.setDataValue(exports.UserFields.info, JSON.stringify(val));
        },
        get() {
            console.log('bbbbbbbbbbbbbbbbbbb ', this.getDataValue(exports.UserFields.noSignName));
            if (this.getDataValue(exports.UserFields.info)) {
                return JSON.parse(this.getDataValue(exports.UserFields.info));
            }
        }
    },
    [exports.UserFields.noSignName]: {
        type: sequelize_1.DataTypes.TEXT
    }
});
exports.User.beforeSave((user, options) => {
    if (user[exports.UserFields.name]) {
        user.noSignName = TextHelper_1.TextHelper.changeAlias(user[exports.UserFields.name]);
    }
});
exports.User.beforeBulkUpdate((query, instanse) => {
    if (query.attributes[exports.UserFields.name]) {
        query.fields.push(exports.UserFields.noSignName);
        query.attributes[exports.UserFields.noSignName] = TextUtil.changeAlias(query.attributes[exports.UserFields.name]);
    }
});
// User.sync({force: true}).then(() => {
//     // Table created
//     return User.create({
//         firstName: 'John',
//         lastName: 'Hancock',
//         info: {email: '123', phone: 'asg52354235'}
//     });
// });
// User.create({
//     firstName: 'John',
//     lastName: 'Hancock',
//     info: {email: '123', phone: 'asg52354235'}
// });
// User.update(
//     {[UserFields.firstName]: 'nam', [UserFields.lastName]: 'trinh viet'},
//     {where: {id: 1}}
// ).then((err) => {
//     console.log(err)
// });
// User.findOne({attributes: [UserFields.id, UserFields.firstName]}).then(users => {
//     console.log(users.dataValues);
// })
//# sourceMappingURL=_User.js.map