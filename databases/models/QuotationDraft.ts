import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';
import {BIDDING_STATUS, RECEIVE_STATUS, SENDING_STATUS} from "../../constants/database/Biddings";
import {CURRENCY_UNIT_VND, TIME_UNIT} from "../../constants/database/Common";
import {MathHelper} from "../../utities/MathHelper";
import {ProjectFields} from "./Project";
import {QuotationFields} from "./Quotation";

export const QuotationDraftFields = {
    id: 'id',
    createdByUserId: 'createdByUserId',
    fromId: 'fromId',
    fromType: 'fromType',
    toId: 'toId', // Đấu thầu cho thực thể nào, toId ở đây chính là ID hạng mục đấu thầu
    toType: 'toType', //hiện tại là hạng mục đấu thầu
    contentLetter: 'contentLetter',
    timeSendBidding: 'timeSendBidding',
    note: 'note',
    isSentBidding: 'isSentBidding',
    biddingStatus: 'biddingStatus',
    feelingStatus: 'feelingStatus',
    totalMoney: 'totalMoney',
    currency: 'currency',
    totalTime: 'totalTime',
    unitTime: 'unitTime',
    version: 'version',
    projectId: 'projectId',
    receiverId: 'receiverId',
    documents: 'documents',
    works: 'works',
    materials: 'materials',
    alternativeBid: 'alternativeBid',
    totalMoneyWorks: 'totalMoneyWorks',
    totalTimeWorks: 'totalTimeWorks',
    totalMoneyMaterials: 'totalMoneyMaterials',
};
export const QuotationDraftAssociations = {};
export const QuotationDraft = sequelize.define('quotationDraft',
    {
        [QuotationDraftFields.id]: {
            type: DataTypes.CHAR(50),
            primaryKey: true
        },
        [QuotationDraftFields.createdByUserId]: {
            type: DataTypes.CHAR(50)
        },
        [QuotationDraftFields.fromId]: {
            type: DataTypes.CHAR(50)
        },
        [QuotationDraftFields.toId]: {
            type: DataTypes.CHAR(100)
        },
        [QuotationDraftFields.fromType]: {
            type: DataTypes.CHAR(100)
        },
        [QuotationDraftFields.toType]: {
            type: DataTypes.CHAR(100)
        },
        [QuotationDraftFields.contentLetter]: {
            type: DataTypes.TEXT
        },
        [QuotationDraftFields.timeSendBidding]: {
            type: DataTypes.DATE
        },
        [QuotationDraftFields.note]: {
            type: DataTypes.TEXT
        },
        [QuotationDraftFields.isSentBidding]: {
            type: DataTypes.BOOLEAN
        },
        [QuotationDraftFields.biddingStatus]: {
            type: DataTypes.CHAR(20)
        },
        [QuotationDraftFields.totalMoney]: {
            type: DataTypes.BIGINT
        },
        [QuotationDraftFields.currency]: {
            type: DataTypes.CHAR(20)
        },
        [QuotationDraftFields.totalTime]: {
            type: DataTypes.INTEGER
        },
        [QuotationDraftFields.unitTime]: {
            type: DataTypes.CHAR(20)
        },
        [QuotationDraftFields.version]: {
            type: DataTypes.INTEGER
        },
        [QuotationDraftFields.projectId]: {
            type: DataTypes.CHAR(50)
        },
        [QuotationDraftFields.receiverId]: {
            type: DataTypes.CHAR(50)
        },
        [QuotationDraftFields.documents]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(QuotationDraftFields.documents, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(QuotationDraftFields.documents)) {
                    return JSON.parse(this.getDataValue(QuotationDraftFields.documents));
                }
            }
        },
        [QuotationDraftFields.works]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(QuotationDraftFields.works, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(QuotationDraftFields.works)) {
                    return JSON.parse(this.getDataValue(QuotationDraftFields.works));
                }
            }
        },
        [QuotationDraftFields.materials]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(QuotationDraftFields.materials, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(QuotationDraftFields.materials)) {
                    return JSON.parse(this.getDataValue(QuotationDraftFields.materials));
                }
            }
        },
        [QuotationDraftFields.alternativeBid]: {
            type: DataTypes.TEXT,
            set(val) {
                this.setDataValue(QuotationDraftFields.alternativeBid, JSON.stringify(val));
            },
            get() {
                if (this.getDataValue(QuotationDraftFields.alternativeBid)) {
                    return JSON.parse(this.getDataValue(QuotationDraftFields.alternativeBid));
                }
            }
        },
        [QuotationDraftFields.totalMoneyWorks]: {
            type: DataTypes.BIGINT
        },
        [QuotationDraftFields.totalTimeWorks]: {
            type: DataTypes.INTEGER
        },
        [QuotationDraftFields.totalMoneyMaterials]: {
            type: DataTypes.BIGINT
        }
    },
    {
        indexes: [
            {
                unique: false,
                fields: [
                    QuotationFields.fromId,

                ]
            },
            {
                unique: false,
                fields: [

                    QuotationFields.fromType,

                ]
            },
            {
                unique: false,
                fields: [

                    QuotationFields.toId,

                ]
            },
            {
                unique: false,
                fields: [

                    QuotationFields.toType,
                ]
            },
            {
                unique: false,
                fields: [

                    QuotationFields.receiverId,
                ]
            },
        ]
    }
);