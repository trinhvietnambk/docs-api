"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _Base_1 = require("./_Base");
const sequelize_1 = require("sequelize");
const TextHelper_1 = require("../../utities/TextHelper");
const Project_1 = require("./Project");
exports.ProjectExperience = _Base_1.sequelize.define('projectExperience', {
    [Project_1.ProjectFields.id]: {
        type: sequelize_1.DataTypes.CHAR(50),
        primaryKey: true
    },
    [Project_1.ProjectFields.name]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [Project_1.ProjectFields.description]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [Project_1.ProjectFields.documents]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(Project_1.ProjectFields.documents, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(Project_1.ProjectFields.documents)) {
                return JSON.parse(this.getDataValue(Project_1.ProjectFields.documents));
            }
        }
    },
    [Project_1.ProjectFields.listImages]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(Project_1.ProjectFields.listImages, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(Project_1.ProjectFields.listImages)) {
                return JSON.parse(this.getDataValue(Project_1.ProjectFields.listImages));
            }
        }
    },
    [Project_1.ProjectFields.listVideos]: {
        type: sequelize_1.DataTypes.TEXT,
        set(val) {
            this.setDataValue(Project_1.ProjectFields.listVideos, JSON.stringify(val));
        },
        get() {
            if (this.getDataValue(Project_1.ProjectFields.listVideos)) {
                return JSON.parse(this.getDataValue(Project_1.ProjectFields.listVideos));
            }
        }
    },
    [Project_1.ProjectFields.projectType]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [Project_1.ProjectFields.projectSubType]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [Project_1.ProjectFields.workType]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [Project_1.ProjectFields.address]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [Project_1.ProjectFields.village]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [Project_1.ProjectFields.district]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [Project_1.ProjectFields.city]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [Project_1.ProjectFields.country]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [Project_1.ProjectFields.minBudget]: {
        type: sequelize_1.DataTypes.REAL
    },
    [Project_1.ProjectFields.maxBudget]: {
        type: sequelize_1.DataTypes.REAL
    },
    [Project_1.ProjectFields.investmentType]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [Project_1.ProjectFields.minConstructionArea]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [Project_1.ProjectFields.maxConstructionArea]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [Project_1.ProjectFields.minLandArea]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [Project_1.ProjectFields.maxLandArea]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [Project_1.ProjectFields.totalFloorArea]: {
        type: sequelize_1.DataTypes.INTEGER
    },
    [Project_1.ProjectFields.minStartTime]: {
        type: sequelize_1.DataTypes.DATE
    },
    [Project_1.ProjectFields.maxStartTime]: {
        type: sequelize_1.DataTypes.DATE
    },
    [Project_1.ProjectFields.minFinishTime]: {
        type: sequelize_1.DataTypes.DATE
    },
    [Project_1.ProjectFields.maxFinishTime]: {
        type: sequelize_1.DataTypes.DATE
    },
    [Project_1.ProjectFields.purpose]: {
        type: sequelize_1.DataTypes.TEXT
    },
    [Project_1.ProjectFields.noSignName]: {
        type: sequelize_1.DataTypes.TEXT
    }
}, {
    indexes: [
        // add a FULLTEXT index
        { type: 'FULLTEXT', fields: [Project_1.ProjectFields.noSignName] },
    ]
});
exports.ProjectExperience.beforeSave((Project, options) => {
    if (Project[Project_1.ProjectFields.name]) {
        Project.noSignName = TextHelper_1.TextHelper.noSignViStringAndToLowerCase(Project[Project_1.ProjectFields.name]);
    }
});
exports.ProjectExperience.beforeBulkUpdate((query, instanse) => {
    if (query.attributes[Project_1.ProjectFields.name]) {
        query.fields.push(Project_1.ProjectFields.noSignName);
        query.attributes[Project_1.ProjectFields.noSignName] = TextHelper_1.TextHelper.noSignViStringAndToLowerCase(query.attributes[Project_1.ProjectFields.name]);
    }
});
//# sourceMappingURL=ProjectExperience.js.map