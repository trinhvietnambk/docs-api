import {sequelize} from "./_Base";
import {DataTypes} from 'sequelize';

export const MainMaketFields = {
    id: 'id',
    value: 'value',
    contractorId: 'contractorId',
};
export const MainMaket = sequelize.define('cMainMaket',
    {

        [MainMaketFields.id]: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        [MainMaketFields.value]: {
            type: DataTypes.TEXT
        }
    }
);
