"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by NamTV on 3/23/2017.
 */
exports.DOCUMENT = 'document';
exports.PROJECT = 'project';
exports.CATEGORY_PROJECT = 'categoryProject';
exports.MATERIAL_PROJECT = 'materialProject';
exports.BIDDING = 'bidding';
exports.MARK_BIDDING = 'markBidding';
exports.CONTRACTOR = 'contractor';
exports.ACCOUNT = 'account';
exports.NOTIFICATION = 'notification';
exports.RATING = 'rating';
exports.CONTACT = 'contact';
exports.REPORT = 'report';
exports.THREAD_MESSAGE = 'threadMessage';
exports.MESSAGE = 'message';
exports.FINANCE = 'finance';
exports.COUNTER = 'counter';
exports.LINK_ENTITY = 'linkEntity';
exports.COUNTRY_CITY_DISTRICT = 'countryCityDistrict';
exports.BIDDING_CATEGORY = 'biddingCategory'; //Hạng mục đấu thầu
exports.ROLE = 'role';
exports.ROLE_BY_ENTITY = 'roleByEntity';
exports.MATERIAL_VIEW_WHO_CAN = 'whocan';
exports.INDEX_COMMENT_ID = 'INDEX_COMMENT_ID';
exports.INDEX_USER_CAN = 'INDEX_USER_CAN';
exports.INDEX_LINK_TO_ID = 'INDEX_LINK_TO_ID';
exports.INDEX_USER_RATING = 'INDEX_USER_RATING';
exports.INDEX_PARTNER_THREAD_MESSAGE = 'INDEX_PARTNER_THREAD_MESSAGE';
exports.INDEX_IS_READ_MESSAGE = 'INDEX_IS_READ_MESSAGE';
exports.INDEX_IS_READ_NOTIFY = 'INDEX_IS_READ_NOTIFY';
exports.UDT_CONTACT = 'UDT_CONTACT';
exports.UDT_PROJECT_CATEGORY = 'UDT_PROJECT_CATEGORY';
exports.UDT_MATERIAL = 'UDT_MATERIAL';
exports.UDT_HOUSELINK_RANKING = 'UDT_HOUSELINK_RANKING';
exports.UDT_DOCUMENT = 'UDT_DOCUMENT';
exports.UDT_FINANCE = 'UDT_FINANCE';
exports.UDT_PROJECT_EXPERIENCE = 'UDT_PROJECT_EXPERIENCE';
exports.WORK_OF_BIDDING = 'workOfBidding';
exports.SEARCH_FAVORITE = 'searchFavorite';
exports.COMPANY_INFO = 'companyInfo';
exports.INDEX_COMPANY_INFO_NAME = 'INDEX_COMPANY_INFO_NAME';
exports.GUEST = 'GUEST';
exports.POST = 'post';
exports.COMMENT = 'comment';
exports.PRODUCT = 'product';
//# sourceMappingURL=EntityName.js.map