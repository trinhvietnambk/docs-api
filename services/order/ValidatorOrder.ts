import {OrderFields} from "../../databases/models/Order";

var Joi = require('joi');
export const VALIDATION_ORDER_CREATE = {
    options: {allowUnknownBody: false},
    body: {
        [OrderFields.shipInfo]: Joi.object().required()
    }
};
export const VALIDATION_ORDER_ADMIN_LIST = {
    options: {allowUnknownBody: false},
    body: {
        [OrderFields.userId]: Joi.string(),
        [OrderFields.status]: Joi.string(),
        orderBy: Joi.string().valid('createdAt').required(),
        orderType: Joi.string().valid('ASC', 'DESC').required(),
        page: Joi.number().min(1)
    }
};
export const VALIDATION_MY_ORDER = {
    body: {
        [OrderFields.status]: Joi.string(),
        orderBy: Joi.string().valid('createdAt').required(),
        orderType: Joi.string().valid('ASC', 'DESC').required(),
        page: Joi.number().min(1)
    }
};
export const VALIDATION_CHANGE_ORDER_STATUS = {
    options: {allowUnknownBody: false},
    body: {
        [OrderFields.id]: Joi.string().required(),
        [OrderFields.status]: Joi.string().required(),
    }
};
export const VALIDATION_DELETE_ORDER = {
    options: {allowUnknownBody: false},
    body: {
        [OrderFields.id]: Joi.string().required()
    }
};