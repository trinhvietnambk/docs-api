"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by nam on 3/26/2017.
 */
/*
 * Validation for rounter projects
 * */
var Joi = require('joi');
exports.VALIDATION_COMPANY_INFO_LIST = {
    options: { allowUnknownBody: false },
    body: {
        name: Joi.string().min(1).max(20),
        taxId: Joi.string().min(1).max(20),
    }
};
//# sourceMappingURL=ValidatorCompanyInfo.js.map