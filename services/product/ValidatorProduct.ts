import {ProductFields} from "../../databases/models/Product";
import {CONTRACTOR} from "../../constants/database/EntityName";
import {ContractorFields} from "../../databases/models/Contractor";
import {USER_ACCOUNT} from "../../constants/database/Account";

var Joi = require('joi');
export const VALIDATION_PRODUCT_CREATE = {
    options: {allowUnknownBody: false},
    body: {
        [ProductFields.name]: Joi.string().required(),
        [ProductFields.images]: Joi.array().items(Joi.string()),
        [ProductFields.description]:Joi.string(),
        [ProductFields.price]: Joi.number().required(),
        [ProductFields.comparePrice]: Joi.number(),
        [ProductFields.isChargeTaxes]:Joi.boolean(),
        [ProductFields.isRequireShip]: Joi.boolean(),
        [ProductFields.minDepth]: Joi.number(),
        [ProductFields.maxDepth]: Joi.number(),
        [ProductFields.minWeight]: Joi.number(),
        [ProductFields.maxWeight]: Joi.number(),
        [ProductFields.minWidth]: Joi.number(),
        [ProductFields.maxWidth]: Joi.number(),
        [ProductFields.unitWeight]: Joi.number(),
        [ProductFields.unitWidth]: Joi.number(),
        [ProductFields.unitDepth]: Joi.number(),
        [ProductFields.inventorySKU]: Joi.string(),
        [ProductFields.inventoryBarcode]: Joi.string(),
        [ProductFields.inventoryPolicy]: Joi.string(),
        [ProductFields.productType]:Joi.string(),
        [ProductFields.vendor]:Joi.string(),
        [ProductFields.ownerId]: Joi.string().required(),
        [ProductFields.ownerType]: Joi.string(),
        [ProductFields.brand]:Joi.string(),
        [ProductFields.sale]:Joi.number(),
        [ProductFields.variants]:Joi.array().items(Joi.object()),
        [ProductFields.sizes]:Joi.array().items(Joi.number()),
        [ProductFields.colors]:Joi.array().items(Joi.string()),
        [ProductFields.materials]:Joi.array().items(Joi.string()),
        [ProductFields.ownerId]: Joi.string().required(),
        [ProductFields.ownerType]: Joi.string().valid(CONTRACTOR,USER_ACCOUNT).required(),
        [ProductFields.tags]: Joi.array().items(Joi.string()),
        [ProductFields.collections]: Joi.array().items(Joi.string()),
    }
};
export const VALIDATION_PRODUCT_UPDATE = {
    options: {allowUnknownBody: false},
    body: {
        [ProductFields.id]: Joi.string().required(),
        [ProductFields.name]: Joi.string(),
        [ProductFields.images]: Joi.array().items(Joi.string()),
        [ProductFields.description]:Joi.string(),
        [ProductFields.price]: Joi.number(),
        [ProductFields.comparePrice]: Joi.number(),
        [ProductFields.isChargeTaxes]:Joi.boolean(),
        [ProductFields.isRequireShip]: Joi.boolean(),
        [ProductFields.minDepth]: Joi.number(),
        [ProductFields.maxDepth]: Joi.number(),
        [ProductFields.minWeight]: Joi.number(),
        [ProductFields.maxWeight]: Joi.number(),
        [ProductFields.minWidth]: Joi.number(),
        [ProductFields.maxWidth]: Joi.number(),
        [ProductFields.unitWeight]: Joi.number(),
        [ProductFields.unitWidth]: Joi.number(),
        [ProductFields.unitDepth]: Joi.number(),
        [ProductFields.inventorySKU]: Joi.string(),
        [ProductFields.inventoryBarcode]: Joi.string(),
        [ProductFields.inventoryPolicy]: Joi.string(),
        [ProductFields.productType]:Joi.string(),
        [ProductFields.vendor]:Joi.string(),
        [ProductFields.brand]:Joi.string(),
        [ProductFields.sale]:Joi.number(),
        [ProductFields.variants]:Joi.array().items(Joi.object()),
        [ProductFields.sizes]:Joi.array().items(Joi.number()),
        [ProductFields.colors]:Joi.array().items(Joi.string()),
        [ProductFields.materials]:Joi.array().items(Joi.string()),
        [ProductFields.tags]: Joi.array().items(Joi.string()),
        [ProductFields.collections]: Joi.array().items(Joi.string())
    }
};
export const VALIDATION_PRODUCT_DELETE = {
    options: {allowUnknownBody: false},
    body: {
        [ProductFields.id]: Joi.string().required(),
        [ProductFields.ownerId]: Joi.string().required(),
    }
};
export const VALIDATION_PRODUCT_SEARCH = {
    options: {allowUnknownBody: false},
    body: {
        [ProductFields.name]: Joi.string(),
        [ProductFields.price]: Joi.number(),
        [ProductFields.comparePrice]: Joi.number(),
        [ProductFields.isChargeTaxes]:Joi.boolean(),
        [ProductFields.isRequireShip]: Joi.boolean(),
        [ProductFields.minDepth]: Joi.number(),
        [ProductFields.maxDepth]: Joi.number(),
        [ProductFields.minWeight]: Joi.number(),
        [ProductFields.maxWeight]: Joi.number(),
        [ProductFields.minWidth]: Joi.number(),
        [ProductFields.maxWidth]: Joi.number(),
        // [ProductFields.inventorySKU]: Joi.string(),
        // [ProductFields.inventoryBarcode]: Joi.string(),
        // [ProductFields.inventoryPolicy]: Joi.string(),
        [ProductFields.productType]:Joi.array().items(Joi.string()),
        [ProductFields.vendor]:Joi.array().items(Joi.string()),
        [ProductFields.brand]:Joi.array().items(Joi.string()),
        [ProductFields.sale]:Joi.number(),
        [ProductFields.sizes]:Joi.array().items(Joi.number()),
        [ProductFields.colors]:Joi.array().items(Joi.string()),
        [ProductFields.materials]:Joi.array().items(Joi.string()),
        [ProductFields.tags]: Joi.array().items(Joi.string()),
        [ProductFields.collections]: Joi.array().items(Joi.string()),
        orderBy: Joi.string().valid('updatedAt','createdAt').required(),
        orderType: Joi.string().valid('ASC','DESC').required(),
        filterFollow:Joi.boolean(),
        isPopular:Joi.boolean(),
        page:Joi.number().min(1),
    }
};
export const VALIDATION_PRODUCT_LIST = {
    options: {allowUnknownBody: false},
    body: {
        [ProductFields.ownerId]: Joi.string().required(),
        [ProductFields.name]: Joi.string(),
        [ProductFields.price]: Joi.number(),
        [ProductFields.comparePrice]: Joi.number(),
        [ProductFields.isChargeTaxes]:Joi.boolean(),
        [ProductFields.isRequireShip]: Joi.boolean(),
        [ProductFields.minDepth]: Joi.number(),
        [ProductFields.maxDepth]: Joi.number(),
        [ProductFields.minWeight]: Joi.number(),
        [ProductFields.maxWeight]: Joi.number(),
        [ProductFields.minWidth]: Joi.number(),
        [ProductFields.maxWidth]: Joi.number(),
        // [ProductFields.inventorySKU]: Joi.string(),
        // [ProductFields.inventoryBarcode]: Joi.string(),
        // [ProductFields.inventoryPolicy]: Joi.string(),
        [ProductFields.productType]:Joi.array().items(Joi.string()),
        [ProductFields.vendor]:Joi.array().items(Joi.string()),
        [ProductFields.brand]:Joi.array().items(Joi.string()),
        [ProductFields.sale]:Joi.number(),
        [ProductFields.sizes]:Joi.array().items(Joi.number()),
        [ProductFields.colors]:Joi.array().items(Joi.string()),
        [ProductFields.materials]:Joi.array().items(Joi.string()),
        [ProductFields.tags]: Joi.array().items(Joi.string()),
        [ProductFields.collections]: Joi.array().items(Joi.string()),
        [ProductFields.ownerId]: Joi.string().required(),
        [ProductFields.ownerType]: Joi.string().valid(CONTRACTOR,USER_ACCOUNT).required(),
        orderBy: Joi.string().valid('updatedAt','createdAt').required(),
        orderType: Joi.string().valid('ASC','DESC').required(),
        filterFollow:Joi.boolean(),
        isPopular:Joi.boolean(),
        page:Joi.number().min(1),
    }
};
export const VALIDATION_PRODUCT_DETAIL_ONE = {
    options: {allowUnknownBody: false},
    params: {
        id: Joi.number().required()
    }
};
export const VALIDATION_PRODUCT_RELATE = {
    options: {allowUnknownBody: false},
    body: {
        id: Joi.number().required()
    }
};