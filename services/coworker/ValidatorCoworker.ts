const Joi = require('joi');
export const VALIDATION_INVITE_COWORKERS = {
    options: {allowUnknownBody: false},
    body: {
        contractorId: Joi.string().required(),
        coworkes: Joi.array().items(Joi.object().keys({
            email: Joi.string().required(),
            fullName: Joi.string()
        })).min(1)
    }
};
export const VALIDATION_JOIN_BY_CODE = {
    options: {allowUnknownBody: false},
    body: {
        code: Joi.string().required()
    }
};
export const VALIDATION_LEAVE = {
    options: {allowUnknownBody: false},
    body: {
        contractorId: Joi.string().required()
    }
};
export const VALIDATION_LIST = {
    options: {allowUnknownBody: false},
    body: {
        contractorId: Joi.string().required()
    }
};
export const VALIDATION_CHANGE_MEMBER_STATUS = {
    options: {allowUnknownBody: false},
    body: {
        contractorId: Joi.string().required(),
        memberId: Joi.string().required(),
        status: Joi.string().required().valid('active', 'deactive')
    }
};