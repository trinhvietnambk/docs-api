/**
 * Created by nam on 3/26/2017.
 */
/*
 * Validation for rounter extras
 * */
import {PROJECT, CONTRACTOR, BIDDING_CATEGORY, ACCOUNT, BIDDING} from "../../constants/database/EntityName";
import {Validation} from "../../constants/schema/Validation";
import {BiddingCategoryFields} from "../../databases/models/BiddingCategory";
import {ProjectFields} from "../../databases/models/Project";
import {USER_ACCOUNT} from "../../constants/database/Account";

var Joi = require('joi');
export const VALIDATION_LIST_BIDDING_CATEGORY = {
    options: {allowUnknownBody: false},
    body: {
        [BiddingCategoryFields.projectId]: Joi.string().required(),
    }
};
export const VALIDATION_DETAIL_BIDDING_CATEGORY = {
    options: {allowUnknownBody: false},
    params: {
        id: Joi.string().required()
    }
};
export const VALIDATION_DELETE_BIDDING_CATEGORY = {
    options: {allowUnknownBody: false},
    body: {
        projectId: Validation.ID.required(),
        id: Validation.ID.required(),
        deleteById: Validation.ID.required(),
        deleteByType: Joi.string().valid(PROJECT, CONTRACTOR).required(),
    }
};

export const VALIDATION_CREATE_BIDDING_ITEM = {
    options: {allowUnknownBody: false},
    body: {

        [BiddingCategoryFields.name]: Joi.string().required(),
        [BiddingCategoryFields.typeOfBid]: Joi.string().valid('public', 'private').required(),
        [BiddingCategoryFields.bidOpenTime]: Joi.date().required(),
        [BiddingCategoryFields.bidCloseTime]: Joi.date().required(),
        [BiddingCategoryFields.allowAlternativeBids]: Joi.boolean().required(),
        [BiddingCategoryFields.allowSealedBids]: Joi.boolean().required(),
        [BiddingCategoryFields.expectTypeOfContractor]: Joi.string(),
        [BiddingCategoryFields.expectCountryOfContractor]: Joi.string(),
        [BiddingCategoryFields.expectCityOfContractor]: Joi.string(),
        [BiddingCategoryFields.expectDistrictOfContractor]: Joi.string(),
        [BiddingCategoryFields.expectExperienceContractor]: Joi.number(),
        [BiddingCategoryFields.expectRatingOfContractor]: Joi.number(),
        [BiddingCategoryFields.expectRevenueOfContractor]: Joi.number(),
        [BiddingCategoryFields.expectUnitRevenueOfContractor]: Joi.string(),
        [BiddingCategoryFields.expectOtherRequirements]: Joi.string(),
        [BiddingCategoryFields.documents]: Joi.array().items(Joi.object()),
        [BiddingCategoryFields.works]: Joi.array().items(Joi.object()),
        [BiddingCategoryFields.materials]: Joi.array().items(Joi.object()),
        [BiddingCategoryFields.preferredCurrency]: Joi.string().required(),
        [BiddingCategoryFields.invitedEmails]: Joi.array().items(Joi.string()),
        [BiddingCategoryFields.invitedFromFavorites]: Joi.array().items(Joi.object().keys({
            id: Joi.string(),
            type: Joi.string().valid(CONTRACTOR, USER_ACCOUNT),
        })),
        [BiddingCategoryFields.projectId]: Joi.string().required(),
    }
};

export const VALIDATION_UPDATE_BIDDING_ITEM = {
    options: {allowUnknownBody: false},
    body: {
        id: Joi.string().required(),
        updateById: Joi.string().required(),
        updateByType: Joi.string().valid(PROJECT, CONTRACTOR).required(),
        [BiddingCategoryFields.name]: Joi.string(),
        [BiddingCategoryFields.typeOfBid]: Joi.string(),
        [BiddingCategoryFields.bidOpenTime]: Joi.date(),
        [BiddingCategoryFields.bidCloseTime]: Joi.date(),
        [BiddingCategoryFields.allowAlternativeBids]: Joi.boolean(),
        [BiddingCategoryFields.allowSealedBids]: Joi.boolean(),
        [BiddingCategoryFields.expectTypeOfContractor]: Joi.string(),
        [BiddingCategoryFields.expectCountryOfContractor]: Joi.string(),
        [BiddingCategoryFields.expectCityOfContractor]: Joi.string(),
        [BiddingCategoryFields.expectDistrictOfContractor]: Joi.string(),
        [BiddingCategoryFields.expectExperienceContractor]: Joi.number(),
        [BiddingCategoryFields.expectRatingOfContractor]: Joi.number(),
        [BiddingCategoryFields.expectRevenueOfContractor]: Joi.number(),
        [BiddingCategoryFields.expectUnitRevenueOfContractor]: Joi.string(),
        [BiddingCategoryFields.expectOtherRequirements]: Joi.string(),
        [BiddingCategoryFields.documents]: Joi.array().items(Joi.object()),
        [BiddingCategoryFields.works]: Joi.array().items(Joi.object()),
        [BiddingCategoryFields.materials]: Joi.array().items(Joi.object()),
        [BiddingCategoryFields.preferredCurrency]: Joi.string(),
        [BiddingCategoryFields.invitedEmails]: Joi.array().items(Joi.string()),
        [BiddingCategoryFields.projectId]: Joi.string().required(),
    }
};

export const VALIDATION_EXTRAS_CREATE_RATING = {
    options: {allowUnknownBody: false},
    body: {
        star: Validation.RANKING.required(),
        content: Validation.DESCRIPTION.required(),
        ownerId: Joi.number().required(),
        ownerType: Joi.string().valid(CONTRACTOR, ACCOUNT, PROJECT).required(),
        senderId: Joi.number().required(),
        senderType: Joi.string().valid(CONTRACTOR, ACCOUNT).required()
    }
};
export const VALIDATION_EXTRAS_EXIST_RATING = {
    options: {allowUnknownBody: false},
    body: {
        ownerId: Joi.number().required(),
        senderId: Joi.number().required()
    }
};
export const VALIDATION_EXTRAS_CREATE_DOCUMENT = {
    options: {allowUnknownBody: false},
    body: {
        name: Validation.NAME.required(),
        url: Joi.string().required(),
        type: Joi.string().required(),
        idFile: Joi.string().required(),
        ownerId: Joi.number().required(),
        ownerType: Joi.string().valid(CONTRACTOR, PROJECT, BIDDING_CATEGORY, BIDDING).required()
    }
};

export const VALIDATION_EXTRAS_GET_CANDIDATE_RATING = {
    options: {allowUnknownBody: false},
    body: {}
};