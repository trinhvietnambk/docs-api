"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by nam on 3/26/2017.
 */
/*
 * Validation for rounter extras
 * */
const EntityName_1 = require("../../constants/database/EntityName");
const Validation_1 = require("../../constants/schema/Validation");
const BiddingCategory_1 = require("../../databases/models/BiddingCategory");
const Account_1 = require("../../constants/database/Account");
var Joi = require('joi');
exports.VALIDATION_LIST_BIDDING_CATEGORY = {
    options: { allowUnknownBody: false },
    body: {
        [BiddingCategory_1.BiddingCategoryFields.projectId]: Joi.string().required(),
    }
};
exports.VALIDATION_DETAIL_BIDDING_CATEGORY = {
    options: { allowUnknownBody: false },
    params: {
        id: Joi.string().required()
    }
};
exports.VALIDATION_DELETE_BIDDING_CATEGORY = {
    options: { allowUnknownBody: false },
    body: {
        projectId: Validation_1.Validation.ID.required(),
        id: Validation_1.Validation.ID.required(),
        deleteById: Validation_1.Validation.ID.required(),
        deleteByType: Joi.string().valid(EntityName_1.PROJECT, EntityName_1.CONTRACTOR).required(),
    }
};
exports.VALIDATION_CREATE_BIDDING_ITEM = {
    options: { allowUnknownBody: false },
    body: {
        [BiddingCategory_1.BiddingCategoryFields.name]: Joi.string().required(),
        [BiddingCategory_1.BiddingCategoryFields.typeOfBid]: Joi.string().valid('public', 'private').required(),
        [BiddingCategory_1.BiddingCategoryFields.bidOpenTime]: Joi.date().required(),
        [BiddingCategory_1.BiddingCategoryFields.bidCloseTime]: Joi.date().required(),
        [BiddingCategory_1.BiddingCategoryFields.allowAlternativeBids]: Joi.boolean().required(),
        [BiddingCategory_1.BiddingCategoryFields.allowSealedBids]: Joi.boolean().required(),
        [BiddingCategory_1.BiddingCategoryFields.expectTypeOfContractor]: Joi.string(),
        [BiddingCategory_1.BiddingCategoryFields.expectCountryOfContractor]: Joi.string(),
        [BiddingCategory_1.BiddingCategoryFields.expectCityOfContractor]: Joi.string(),
        [BiddingCategory_1.BiddingCategoryFields.expectDistrictOfContractor]: Joi.string(),
        [BiddingCategory_1.BiddingCategoryFields.expectExperienceContractor]: Joi.number(),
        [BiddingCategory_1.BiddingCategoryFields.expectRatingOfContractor]: Joi.number(),
        [BiddingCategory_1.BiddingCategoryFields.expectRevenueOfContractor]: Joi.number(),
        [BiddingCategory_1.BiddingCategoryFields.expectUnitRevenueOfContractor]: Joi.string(),
        [BiddingCategory_1.BiddingCategoryFields.expectOtherRequirements]: Joi.string(),
        [BiddingCategory_1.BiddingCategoryFields.documents]: Joi.array().items(Joi.object()),
        [BiddingCategory_1.BiddingCategoryFields.works]: Joi.array().items(Joi.object()),
        [BiddingCategory_1.BiddingCategoryFields.materials]: Joi.array().items(Joi.object()),
        [BiddingCategory_1.BiddingCategoryFields.preferredCurrency]: Joi.string().required(),
        [BiddingCategory_1.BiddingCategoryFields.invitedEmails]: Joi.array().items(Joi.string()),
        [BiddingCategory_1.BiddingCategoryFields.invitedFromFavorites]: Joi.array().items(Joi.object().keys({
            id: Joi.string(),
            type: Joi.string().valid(EntityName_1.CONTRACTOR, Account_1.USER_ACCOUNT),
        })),
        [BiddingCategory_1.BiddingCategoryFields.projectId]: Joi.string().required(),
    }
};
exports.VALIDATION_UPDATE_BIDDING_ITEM = {
    options: { allowUnknownBody: false },
    body: {
        id: Joi.string().required(),
        updateById: Joi.string().required(),
        updateByType: Joi.string().valid(EntityName_1.PROJECT, EntityName_1.CONTRACTOR).required(),
        [BiddingCategory_1.BiddingCategoryFields.name]: Joi.string(),
        [BiddingCategory_1.BiddingCategoryFields.typeOfBid]: Joi.string(),
        [BiddingCategory_1.BiddingCategoryFields.bidOpenTime]: Joi.date(),
        [BiddingCategory_1.BiddingCategoryFields.bidCloseTime]: Joi.date(),
        [BiddingCategory_1.BiddingCategoryFields.allowAlternativeBids]: Joi.boolean(),
        [BiddingCategory_1.BiddingCategoryFields.allowSealedBids]: Joi.boolean(),
        [BiddingCategory_1.BiddingCategoryFields.expectTypeOfContractor]: Joi.string(),
        [BiddingCategory_1.BiddingCategoryFields.expectCountryOfContractor]: Joi.string(),
        [BiddingCategory_1.BiddingCategoryFields.expectCityOfContractor]: Joi.string(),
        [BiddingCategory_1.BiddingCategoryFields.expectDistrictOfContractor]: Joi.string(),
        [BiddingCategory_1.BiddingCategoryFields.expectExperienceContractor]: Joi.number(),
        [BiddingCategory_1.BiddingCategoryFields.expectRatingOfContractor]: Joi.number(),
        [BiddingCategory_1.BiddingCategoryFields.expectRevenueOfContractor]: Joi.number(),
        [BiddingCategory_1.BiddingCategoryFields.expectUnitRevenueOfContractor]: Joi.string(),
        [BiddingCategory_1.BiddingCategoryFields.expectOtherRequirements]: Joi.string(),
        [BiddingCategory_1.BiddingCategoryFields.documents]: Joi.array().items(Joi.object()),
        [BiddingCategory_1.BiddingCategoryFields.works]: Joi.array().items(Joi.object()),
        [BiddingCategory_1.BiddingCategoryFields.materials]: Joi.array().items(Joi.object()),
        [BiddingCategory_1.BiddingCategoryFields.preferredCurrency]: Joi.string(),
        [BiddingCategory_1.BiddingCategoryFields.invitedEmails]: Joi.array().items(Joi.string()),
        [BiddingCategory_1.BiddingCategoryFields.projectId]: Joi.string().required(),
    }
};
exports.VALIDATION_EXTRAS_CREATE_RATING = {
    options: { allowUnknownBody: false },
    body: {
        star: Validation_1.Validation.RANKING.required(),
        content: Validation_1.Validation.DESCRIPTION.required(),
        ownerId: Joi.number().required(),
        ownerType: Joi.string().valid(EntityName_1.CONTRACTOR, EntityName_1.ACCOUNT, EntityName_1.PROJECT).required(),
        senderId: Joi.number().required(),
        senderType: Joi.string().valid(EntityName_1.CONTRACTOR, EntityName_1.ACCOUNT).required()
    }
};
exports.VALIDATION_EXTRAS_EXIST_RATING = {
    options: { allowUnknownBody: false },
    body: {
        ownerId: Joi.number().required(),
        senderId: Joi.number().required()
    }
};
exports.VALIDATION_EXTRAS_CREATE_DOCUMENT = {
    options: { allowUnknownBody: false },
    body: {
        name: Validation_1.Validation.NAME.required(),
        url: Joi.string().required(),
        type: Joi.string().required(),
        idFile: Joi.string().required(),
        ownerId: Joi.number().required(),
        ownerType: Joi.string().valid(EntityName_1.CONTRACTOR, EntityName_1.PROJECT, EntityName_1.BIDDING_CATEGORY, EntityName_1.BIDDING).required()
    }
};
exports.VALIDATION_EXTRAS_GET_CANDIDATE_RATING = {
    options: { allowUnknownBody: false },
    body: {}
};
//# sourceMappingURL=ValidatorBiddingCategory.js.map