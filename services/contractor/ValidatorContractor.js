"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Contractor_1 = require("../../databases/models/Contractor");
const Project_1 = require("../../databases/models/Project");
/**
 * Created by nam on 3/26/2017.
 */
/*
 * Validation for rounter contractors
 * */
var Joi = require('joi');
exports.VALIDATION_CONTRACTORS_LIST = {
    options: { allowUnknownBody: false },
    body: {
        [Contractor_1.ContractorFields.name]: Joi.string(),
        [Contractor_1.ContractorFields.type]: Joi.string(),
        [Contractor_1.ContractorFields.city]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.country]: Joi.string(),
        [Contractor_1.ContractorFields.district]: Joi.string(),
        [Contractor_1.ContractorFields.origination]: Joi.string(),
        [Contractor_1.ContractorFields.foundedYear]: Joi.number(),
        [Contractor_1.ContractorFields.typeOfCompany]: Joi.string(),
        [Contractor_1.ContractorFields.revenue]: Joi.number(),
        [Contractor_1.ContractorFields.typeOfContractor]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.typeOfConstruction]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.typeOfProject]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.typeOfBusiness]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.subBusiness]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.mainMarkets]: Joi.array().items(Joi.string()),
        orderBy: Joi.string().valid('updatedAt', 'createdAt', Contractor_1.ContractorFields.revenue).required(),
        orderType: Joi.string().valid('ASC', 'DESC').required(),
        filterFollow: Joi.boolean(),
        page: Joi.number().min(1),
    }
};
exports.VALIDATION_CONTRACTORS_DETAIL_ONE = {
    options: { allowUnknownBody: false },
    params: {
        id: Joi.string().required()
    }
};
exports.VALIDATION_CONTRACTORS_CREATE_PROJECT_EXPERIENCE = {
    options: { allowUnknownBody: false },
    body: {
        [Project_1.ProjectFields.name]: Joi.string().min(5).required(),
        [Project_1.ProjectFields.description]: Joi.string().min(5),
        [Project_1.ProjectFields.documents]: Joi.array().items(Joi.object()),
        [Project_1.ProjectFields.listVideos]: Joi.array().items(Joi.string()),
        [Project_1.ProjectFields.listImages]: Joi.array().items(Joi.string()),
        [Project_1.ProjectFields.projectType]: Joi.string(),
        [Project_1.ProjectFields.projectSubType]: Joi.string(),
        [Project_1.ProjectFields.workType]: Joi.string(),
        [Project_1.ProjectFields.address]: Joi.string().required(),
        [Project_1.ProjectFields.village]: Joi.string(),
        [Project_1.ProjectFields.district]: Joi.string().required(),
        [Project_1.ProjectFields.city]: Joi.string().required(),
        [Project_1.ProjectFields.country]: Joi.string().required(),
        [Project_1.ProjectFields.minBudget]: Joi.number(),
        [Project_1.ProjectFields.maxBudget]: Joi.number(),
        [Project_1.ProjectFields.investmentType]: Joi.string(),
        [Project_1.ProjectFields.minConstructionArea]: Joi.number(),
        [Project_1.ProjectFields.maxConstructionArea]: Joi.number(),
        [Project_1.ProjectFields.minLandArea]: Joi.number(),
        [Project_1.ProjectFields.maxLandArea]: Joi.number(),
        [Project_1.ProjectFields.totalFloorArea]: Joi.number(),
        [Project_1.ProjectFields.state]: Joi.string(),
        [Project_1.ProjectFields.status]: Joi.string(),
        [Project_1.ProjectFields.minStartTime]: Joi.date(),
        [Project_1.ProjectFields.maxStartTime]: Joi.date(),
        [Project_1.ProjectFields.minFinishTime]: Joi.date(),
        [Project_1.ProjectFields.maxFinishTime]: Joi.date(),
        [Project_1.ProjectFields.purpose]: Joi.string(),
    }
};
exports.VALIDATION_CONTRACTORS_UPDATE_PROJECT_EXPERIENCE = {
    options: { allowUnknownBody: false },
    body: {
        [Project_1.ProjectFields.id]: Joi.string().min(5).required(),
        [Project_1.ProjectFields.name]: Joi.string().min(5),
        [Project_1.ProjectFields.description]: Joi.string(),
        [Project_1.ProjectFields.documents]: Joi.array().items(Joi.object()),
        [Project_1.ProjectFields.listVideos]: Joi.array().items(Joi.string()),
        [Project_1.ProjectFields.listImages]: Joi.array().items(Joi.string()),
        [Project_1.ProjectFields.projectType]: Joi.string(),
        [Project_1.ProjectFields.projectSubType]: Joi.string(),
        [Project_1.ProjectFields.workType]: Joi.string(),
        [Project_1.ProjectFields.address]: Joi.string(),
        [Project_1.ProjectFields.village]: Joi.string(),
        [Project_1.ProjectFields.district]: Joi.string(),
        [Project_1.ProjectFields.city]: Joi.string(),
        [Project_1.ProjectFields.country]: Joi.string(),
        [Project_1.ProjectFields.minBudget]: Joi.number(),
        [Project_1.ProjectFields.maxBudget]: Joi.number(),
        [Project_1.ProjectFields.investmentType]: Joi.string(),
        [Project_1.ProjectFields.minConstructionArea]: Joi.number(),
        [Project_1.ProjectFields.maxConstructionArea]: Joi.number(),
        [Project_1.ProjectFields.minLandArea]: Joi.number(),
        [Project_1.ProjectFields.maxLandArea]: Joi.number(),
        [Project_1.ProjectFields.totalFloorArea]: Joi.number(),
        [Project_1.ProjectFields.state]: Joi.string(),
        [Project_1.ProjectFields.status]: Joi.string(),
        [Project_1.ProjectFields.minStartTime]: Joi.date(),
        [Project_1.ProjectFields.maxStartTime]: Joi.date(),
        [Project_1.ProjectFields.minFinishTime]: Joi.date(),
        [Project_1.ProjectFields.maxFinishTime]: Joi.date(),
        [Project_1.ProjectFields.purpose]: Joi.string(),
    }
};
exports.VALIDATION_CONTRACTORS_CREATE = {
    options: { allowUnknownBody: false },
    body: {
        [Contractor_1.ContractorFields.name]: Joi.string().required(),
        [Contractor_1.ContractorFields.type]: Joi.string(),
        [Contractor_1.ContractorFields.wallImage]: Joi.string(),
        [Contractor_1.ContractorFields.logo]: Joi.string(),
        [Contractor_1.ContractorFields.address]: Joi.string().required(),
        [Contractor_1.ContractorFields.city]: Joi.string().required(),
        [Contractor_1.ContractorFields.country]: Joi.string().required(),
        [Contractor_1.ContractorFields.district]: Joi.string().required(),
        [Contractor_1.ContractorFields.phone]: Joi.string().required(),
        [Contractor_1.ContractorFields.fax]: Joi.string(),
        [Contractor_1.ContractorFields.website]: Joi.string(),
        [Contractor_1.ContractorFields.email]: Joi.string().required(),
        [Contractor_1.ContractorFields.taxCode]: Joi.string().required(),
        [Contractor_1.ContractorFields.origination]: Joi.string().required(),
        [Contractor_1.ContractorFields.foundedYear]: Joi.number().required(),
        [Contractor_1.ContractorFields.description]: Joi.string(),
        [Contractor_1.ContractorFields.typeOfCompany]: Joi.string(),
        [Contractor_1.ContractorFields.qualityManagement]: Joi.string(),
        [Contractor_1.ContractorFields.language]: Joi.string(),
        [Contractor_1.ContractorFields.numberOfEmployees]: Joi.string().max(100),
        [Contractor_1.ContractorFields.revenue]: Joi.number(),
        [Contractor_1.ContractorFields.unitRevenue]: Joi.string(),
        [Contractor_1.ContractorFields.price]: Joi.number(),
        [Contractor_1.ContractorFields.unitPrice]: Joi.string(),
        [Contractor_1.ContractorFields.prizes]: Joi.number(),
        [Contractor_1.ContractorFields.verifyDocuments]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.listVideos]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.documents]: Joi.array().items(Joi.object()),
        [Contractor_1.ContractorFields.finances]: Joi.array().items(Joi.object()),
        [Contractor_1.ContractorFields.imageFinances]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.affiliations]: Joi.array().items(Joi.object()),
        [Contractor_1.ContractorFields.services]: Joi.array().items(Joi.object()),
        [Contractor_1.ContractorFields.typeOfContractor]: Joi.array().items(Joi.string()).required(),
        [Contractor_1.ContractorFields.typeOfConstruction]: Joi.array().items(Joi.string()).required(),
        [Contractor_1.ContractorFields.typeOfProject]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.typeOfBusiness]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.subBusiness]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.mainMarkets]: Joi.array().items(Joi.string()),
    }
};
exports.VALIDATION_CONTRACTORS_UPDATE = {
    options: { allowUnknownBody: false },
    params: {
        'contractorId': Joi.string().required()
    },
    body: {
        [Contractor_1.ContractorFields.name]: Joi.string(),
        [Contractor_1.ContractorFields.type]: Joi.string(),
        [Contractor_1.ContractorFields.wallImage]: Joi.string(),
        [Contractor_1.ContractorFields.logo]: Joi.string(),
        [Contractor_1.ContractorFields.address]: Joi.string(),
        [Contractor_1.ContractorFields.city]: Joi.string(),
        [Contractor_1.ContractorFields.country]: Joi.string(),
        [Contractor_1.ContractorFields.district]: Joi.string(),
        [Contractor_1.ContractorFields.phone]: Joi.string(),
        [Contractor_1.ContractorFields.fax]: Joi.string(),
        [Contractor_1.ContractorFields.website]: Joi.string(),
        [Contractor_1.ContractorFields.email]: Joi.string(),
        [Contractor_1.ContractorFields.taxCode]: Joi.string(),
        [Contractor_1.ContractorFields.origination]: Joi.string(),
        [Contractor_1.ContractorFields.foundedYear]: Joi.number(),
        [Contractor_1.ContractorFields.description]: Joi.string(),
        [Contractor_1.ContractorFields.typeOfCompany]: Joi.string(),
        [Contractor_1.ContractorFields.qualityManagement]: Joi.string(),
        [Contractor_1.ContractorFields.language]: Joi.string(),
        [Contractor_1.ContractorFields.numberOfEmployees]: Joi.string().max(100),
        [Contractor_1.ContractorFields.revenue]: Joi.number(),
        [Contractor_1.ContractorFields.unitRevenue]: Joi.string(),
        [Contractor_1.ContractorFields.prizes]: Joi.string(),
        [Contractor_1.ContractorFields.verifyDocuments]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.listVideos]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.documents]: Joi.array().items(Joi.object()),
        [Contractor_1.ContractorFields.finances]: Joi.array().items(Joi.object()),
        [Contractor_1.ContractorFields.imageFinances]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.affiliations]: Joi.array().items(Joi.object()),
        [Contractor_1.ContractorFields.services]: Joi.array().items(Joi.object()),
        [Contractor_1.ContractorFields.typeOfContractor]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.typeOfConstruction]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.typeOfProject]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.typeOfBusiness]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.subBusiness]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.mainMarkets]: Joi.array().items(Joi.string()),
    }
};
exports.VALIDATION_UPDATE_HOUSELINK_EVALUATION = {
    options: { allowUnknownBody: false },
    body: {
        [Contractor_1.ContractorFields.id]: Joi.string().required(),
        [Contractor_1.ContractorFields.houselinkRanking]: Joi.array().items(Joi.object().keys({
            category: Joi.string().required(),
            ranking: Joi.string().required(),
            score: Joi.number().max(5).min(1).required(),
        })),
        [Contractor_1.ContractorFields.totalRanking]: Joi.number(),
        [Contractor_1.ContractorFields.isHouseLinkMember]: Joi.boolean()
    }
};
exports.VALIDATION_APPROVED_CONTRACTOR = {
    options: { allowUnknownBody: false },
    body: {
        [Contractor_1.ContractorFields.id]: Joi.string().required(),
        [Contractor_1.ContractorFields.approved]: Joi.boolean().required()
    }
};
exports.VALIDATION_ADMIN_LIST_CONTRACTORS = {
    options: { allowUnknownBody: false },
    body: {
        [Contractor_1.ContractorFields.name]: Joi.string(),
        [Contractor_1.ContractorFields.type]: Joi.string(),
        [Contractor_1.ContractorFields.city]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.country]: Joi.string(),
        [Contractor_1.ContractorFields.district]: Joi.string(),
        [Contractor_1.ContractorFields.origination]: Joi.string(),
        [Contractor_1.ContractorFields.foundedYear]: Joi.number(),
        [Contractor_1.ContractorFields.typeOfCompany]: Joi.string(),
        [Contractor_1.ContractorFields.revenue]: Joi.number(),
        [Contractor_1.ContractorFields.approved]: Joi.boolean(),
        [Contractor_1.ContractorFields.typeOfContractor]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.typeOfConstruction]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.typeOfProject]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.typeOfBusiness]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.subBusiness]: Joi.array().items(Joi.string()),
        [Contractor_1.ContractorFields.mainMarkets]: Joi.array().items(Joi.string()),
        orderBy: Joi.string().valid('updatedAt', 'createdAt', Contractor_1.ContractorFields.revenue).required(),
        orderType: Joi.string().valid('ASC', 'DESC').required(),
        page: Joi.number().min(1),
    }
};
exports.VALIDATION_INVITE_COWORKERS = {
    options: { allowUnknownBody: false },
    body: {
        contractorId: Joi.string().required(),
        coworkes: Joi.array().items(Joi.object().keys({
            email: Joi.string().required(),
            fullName: Joi.string()
        })).min(1)
    }
};
//# sourceMappingURL=ValidatorContractor.js.map