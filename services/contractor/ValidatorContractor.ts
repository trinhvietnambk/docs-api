/**
 * Created by nam on 3/26/2017.
 */
/*
 * Validation for rounter projects
 * */
import {Validation} from "../../constants/schema/Validation";
import {ContractorFields} from "../../databases/models/Contractor";
import {ProjectFields} from "../../databases/models/Project";
/**
 * Created by nam on 3/26/2017.
 */
/*
 * Validation for rounter contractors
 * */
var Joi = require('joi');
export const VALIDATION_CONTRACTORS_LIST = {
    options: {allowUnknownBody: false},
    body: {
        [ContractorFields.name]: Joi.string(),
        [ContractorFields.type]: Joi.string(),
        [ContractorFields.city]: Joi.array().items(Joi.string()),
        [ContractorFields.country]: Joi.string(),
        [ContractorFields.district]: Joi.string(),
        [ContractorFields.origination]: Joi.string(),
        [ContractorFields.foundedYear]: Joi.number(),
        [ContractorFields.typeOfCompany]: Joi.string(),
        [ContractorFields.revenue]: Joi.number(),

        [ContractorFields.typeOfContractor]: Joi.array().items(Joi.string()),
        [ContractorFields.typeOfConstruction]: Joi.array().items(Joi.string()),
        [ContractorFields.typeOfProject]: Joi.array().items(Joi.string()),
        [ContractorFields.typeOfBusiness]: Joi.array().items(Joi.string()),
        [ContractorFields.subBusiness]: Joi.array().items(Joi.string()),
        [ContractorFields.mainMarkets]: Joi.array().items(Joi.string()),
        orderBy: Joi.string().valid('updatedAt', 'createdAt', ContractorFields.revenue).required(),
        orderType: Joi.string().valid('ASC', 'DESC').required(),
        filterFollow: Joi.boolean(),
        page: Joi.number().min(1),
    }
};
export const VALIDATION_CONTRACTORS_DETAIL_ONE = {
    options: {allowUnknownBody: false},
    params: {
        id: Joi.string().required()
    }
};
export const VALIDATION_CONTRACTORS_CREATE_PROJECT_EXPERIENCE = {
    options: {allowUnknownBody: false},
    body: {
        [ProjectFields.name]: Joi.string().min(5).required(),
        [ProjectFields.description]: Joi.string().min(5),
        [ProjectFields.documents]: Joi.array().items(Joi.object()),
        [ProjectFields.listVideos]: Joi.array().items(Joi.string()),
        [ProjectFields.listImages]: Joi.array().items(Joi.string()),
        [ProjectFields.projectType]: Joi.string(),
        [ProjectFields.projectSubType]: Joi.string(),
        [ProjectFields.workType]: Joi.string(),
        [ProjectFields.address]: Joi.string().required(),
        [ProjectFields.village]: Joi.string(),
        [ProjectFields.district]: Joi.string().required(),
        [ProjectFields.city]: Joi.string().required(),
        [ProjectFields.country]: Joi.string().required(),
        [ProjectFields.minBudget]: Joi.number(),
        [ProjectFields.maxBudget]: Joi.number(),
        [ProjectFields.investmentType]: Joi.string(),
        [ProjectFields.minConstructionArea]: Joi.number(),
        [ProjectFields.maxConstructionArea]: Joi.number(),
        [ProjectFields.minLandArea]: Joi.number(),
        [ProjectFields.maxLandArea]: Joi.number(),
        [ProjectFields.totalFloorArea]: Joi.number(),
        [ProjectFields.state]: Joi.string(),
        [ProjectFields.status]: Joi.string(),
        [ProjectFields.minStartTime]: Joi.date(),
        [ProjectFields.maxStartTime]: Joi.date(),
        [ProjectFields.minFinishTime]: Joi.date(),
        [ProjectFields.maxFinishTime]: Joi.date(),
        [ProjectFields.purpose]: Joi.string(),
    }
};
export const VALIDATION_CONTRACTORS_UPDATE_PROJECT_EXPERIENCE = {
    options: {allowUnknownBody: false},
    body: {
        [ProjectFields.id]: Joi.string().min(5).required(),
        [ProjectFields.name]: Joi.string().min(5),
        [ProjectFields.description]: Joi.string(),
        [ProjectFields.documents]: Joi.array().items(Joi.object()),
        [ProjectFields.listVideos]: Joi.array().items(Joi.string()),
        [ProjectFields.listImages]: Joi.array().items(Joi.string()),
        [ProjectFields.projectType]: Joi.string(),
        [ProjectFields.projectSubType]: Joi.string(),
        [ProjectFields.workType]: Joi.string(),
        [ProjectFields.address]: Joi.string(),
        [ProjectFields.village]: Joi.string(),
        [ProjectFields.district]: Joi.string(),
        [ProjectFields.city]: Joi.string(),
        [ProjectFields.country]: Joi.string(),
        [ProjectFields.minBudget]: Joi.number(),
        [ProjectFields.maxBudget]: Joi.number(),
        [ProjectFields.investmentType]: Joi.string(),
        [ProjectFields.minConstructionArea]: Joi.number(),
        [ProjectFields.maxConstructionArea]: Joi.number(),
        [ProjectFields.minLandArea]: Joi.number(),
        [ProjectFields.maxLandArea]: Joi.number(),
        [ProjectFields.totalFloorArea]: Joi.number(),
        [ProjectFields.state]: Joi.string(),
        [ProjectFields.status]: Joi.string(),
        [ProjectFields.minStartTime]: Joi.date(),
        [ProjectFields.maxStartTime]: Joi.date(),
        [ProjectFields.minFinishTime]: Joi.date(),
        [ProjectFields.maxFinishTime]: Joi.date(),
        [ProjectFields.purpose]: Joi.string(),


    }
};
export const VALIDATION_CONTRACTORS_CREATE = {
    options: {allowUnknownBody: false},
    body: {

        [ContractorFields.name]: Joi.string().required(),
        [ContractorFields.type]: Joi.string(),
        [ContractorFields.wallImage]: Joi.string(),
        [ContractorFields.logo]: Joi.string(),
        [ContractorFields.address]: Joi.string().required(),
        [ContractorFields.city]: Joi.string().required(),
        [ContractorFields.country]: Joi.string().required(),
        [ContractorFields.district]: Joi.string().required(),
        [ContractorFields.phone]: Joi.string().required(),
        [ContractorFields.fax]: Joi.string(),
        [ContractorFields.website]: Joi.string(),
        [ContractorFields.email]: Joi.string().required(),
        [ContractorFields.taxCode]: Joi.string().required(),
        [ContractorFields.origination]: Joi.string().required(),
        [ContractorFields.foundedYear]: Joi.number().required(),
        [ContractorFields.description]: Joi.string(),
        [ContractorFields.typeOfCompany]: Joi.string(),
        [ContractorFields.qualityManagement]: Joi.string(),
        [ContractorFields.language]: Joi.string(),
        [ContractorFields.numberOfEmployees]: Joi.string().max(100),
        [ContractorFields.revenue]: Joi.number(),
        [ContractorFields.unitRevenue]: Joi.string(),
        [ContractorFields.price]: Joi.number(),
        [ContractorFields.unitPrice]: Joi.string(),
        [ContractorFields.prizes]: Joi.number(),
        [ContractorFields.verifyDocuments]: Joi.array().items(Joi.string()),

        [ContractorFields.listVideos]: Joi.array().items(Joi.string()),
        [ContractorFields.documents]: Joi.array().items(Joi.object()),
        [ContractorFields.finances]: Joi.array().items(Joi.object()),
        [ContractorFields.imageFinances]: Joi.array().items(Joi.string()),
        [ContractorFields.affiliations]: Joi.array().items(Joi.object()),
        [ContractorFields.services]: Joi.array().items(Joi.object()),

        [ContractorFields.typeOfContractor]: Joi.array().items(Joi.string()).required(),
        [ContractorFields.typeOfConstruction]: Joi.array().items(Joi.string()).required(),
        [ContractorFields.typeOfProject]: Joi.array().items(Joi.string()),
        [ContractorFields.typeOfBusiness]: Joi.array().items(Joi.string()),
        [ContractorFields.subBusiness]: Joi.array().items(Joi.string()),
        [ContractorFields.mainMarkets]: Joi.array().items(Joi.string()),
    }
};

export const VALIDATION_CONTRACTORS_UPDATE = {
    options: {allowUnknownBody: false},
    params:{
        'contractorId':Joi.string().required()
    },
    body: {
        [ContractorFields.name]: Joi.string(),
        [ContractorFields.type]: Joi.string(),
        [ContractorFields.wallImage]: Joi.string(),
        [ContractorFields.logo]: Joi.string(),
        [ContractorFields.address]: Joi.string(),
        [ContractorFields.city]: Joi.string(),
        [ContractorFields.country]: Joi.string(),
        [ContractorFields.district]: Joi.string(),
        [ContractorFields.phone]: Joi.string(),
        [ContractorFields.fax]: Joi.string(),
        [ContractorFields.website]: Joi.string(),
        [ContractorFields.email]: Joi.string(),
        [ContractorFields.taxCode]: Joi.string(),
        [ContractorFields.origination]: Joi.string(),
        [ContractorFields.foundedYear]: Joi.number(),
        [ContractorFields.description]: Joi.string(),
        [ContractorFields.typeOfCompany]: Joi.string(),
        [ContractorFields.qualityManagement]: Joi.string(),
        [ContractorFields.language]: Joi.string(),
        [ContractorFields.numberOfEmployees]:  Joi.string().max(100),
        [ContractorFields.revenue]: Joi.number(),
        [ContractorFields.unitRevenue]: Joi.string(),
        [ContractorFields.prizes]: Joi.string(),
        [ContractorFields.verifyDocuments]: Joi.array().items(Joi.string()),

        [ContractorFields.listVideos]: Joi.array().items(Joi.string()),
        [ContractorFields.documents]: Joi.array().items(Joi.object()),
        [ContractorFields.finances]: Joi.array().items(Joi.object()),
        [ContractorFields.imageFinances]: Joi.array().items(Joi.string()),
        [ContractorFields.affiliations]: Joi.array().items(Joi.object()),
        [ContractorFields.services]: Joi.array().items(Joi.object()),

        [ContractorFields.typeOfContractor]: Joi.array().items(Joi.string()),
        [ContractorFields.typeOfConstruction]: Joi.array().items(Joi.string()),
        [ContractorFields.typeOfProject]: Joi.array().items(Joi.string()),
        [ContractorFields.typeOfBusiness]: Joi.array().items(Joi.string()),
        [ContractorFields.subBusiness]: Joi.array().items(Joi.string()),
        [ContractorFields.mainMarkets]: Joi.array().items(Joi.string()),

    }
};
export const VALIDATION_UPDATE_HOUSELINK_EVALUATION = {
    options: {allowUnknownBody: false},
    body: {
        [ContractorFields.id]: Joi.string().required(),
        [ContractorFields.houselinkRanking]: Joi.array().items(Joi.object().keys({
            category: Joi.string().required(),
            ranking: Joi.string().required(),
            score: Joi.number().max(5).min(1).required(),
        })),
        [ContractorFields.totalRanking]: Joi.number(),
        [ContractorFields.isHouseLinkMember]: Joi.boolean()
    }
};
export const VALIDATION_APPROVED_CONTRACTOR = {
    options: {allowUnknownBody: false},
    body: {
        [ContractorFields.id]: Joi.string().required(),
        [ContractorFields.approved]: Joi.boolean().required()
    }
};
export const VALIDATION_ADMIN_LIST_CONTRACTORS = {
    options: {allowUnknownBody: false},
    body: {
        [ContractorFields.name]: Joi.string(),
        [ContractorFields.type]: Joi.string(),
        [ContractorFields.city]: Joi.array().items(Joi.string()),
        [ContractorFields.country]: Joi.string(),
        [ContractorFields.district]: Joi.string(),
        [ContractorFields.origination]: Joi.string(),
        [ContractorFields.foundedYear]: Joi.number(),
        [ContractorFields.typeOfCompany]: Joi.string(),
        [ContractorFields.revenue]: Joi.number(),
        [ContractorFields.approved]: Joi.boolean(),

        [ContractorFields.typeOfContractor]: Joi.array().items(Joi.string()),
        [ContractorFields.typeOfConstruction]: Joi.array().items(Joi.string()),
        [ContractorFields.typeOfProject]: Joi.array().items(Joi.string()),
        [ContractorFields.typeOfBusiness]: Joi.array().items(Joi.string()),
        [ContractorFields.subBusiness]: Joi.array().items(Joi.string()),
        [ContractorFields.mainMarkets]: Joi.array().items(Joi.string()),
        orderBy: Joi.string().valid('updatedAt', 'createdAt', ContractorFields.revenue).required(),
        orderType: Joi.string().valid('ASC', 'DESC').required(),
        page: Joi.number().min(1),
    }
};
