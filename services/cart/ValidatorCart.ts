import {CartFields} from "../../databases/models/Cart";

var Joi = require('joi');
export const VALIDATION_ADD_PRODUCT_TO_CART = {
    options: {allowUnknownBody: false},
    body: {
        [CartFields.productId]: Joi.string().required(),
        [CartFields.amount]: Joi.number().min(1).required(),
    }
};
export const VALIDATION_REMOVE_PRODUCT = {
    options: {allowUnknownBody: false},
    body: {
        [CartFields.productId]: Joi.string().required()
    }
};
export const VALIDATION_DETAIL_CART = {
    options: {allowUnknownBody: false}
};
export const VALIDATION_CHANGE_AMOUNT_PRODUCT = {
    options: {allowUnknownBody: false},
    body: {
        [CartFields.productId]: Joi.string().required(),
        [CartFields.amount]: Joi.number().min(1).required(),
    }
};