"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Project_1 = require("../../databases/models/Project");
var Joi = require('joi');
exports.VALIDATION_PROJECTS_SEARCH = {
    options: { allowUnknownBody: false },
    body: {
        [Project_1.ProjectFields.name]: Joi.string(),
        [Project_1.ProjectFields.projectType]: Joi.string(),
        [Project_1.ProjectFields.projectSubType]: Joi.string(),
        [Project_1.ProjectFields.workType]: Joi.string(),
        [Project_1.ProjectFields.district]: Joi.string(),
        [Project_1.ProjectFields.city]: Joi.array().items(Joi.string()),
        [Project_1.ProjectFields.country]: Joi.string(),
        [Project_1.ProjectFields.minBudget]: Joi.number(),
        [Project_1.ProjectFields.maxBudget]: Joi.number(),
        [Project_1.ProjectFields.investmentType]: Joi.string(),
        [Project_1.ProjectFields.minConstructionArea]: Joi.number(),
        [Project_1.ProjectFields.maxConstructionArea]: Joi.number(),
        [Project_1.ProjectFields.minLandArea]: Joi.number(),
        [Project_1.ProjectFields.maxLandArea]: Joi.number(),
        [Project_1.ProjectFields.totalFloorArea]: Joi.number(),
        [Project_1.ProjectFields.state]: Joi.string(),
        [Project_1.ProjectFields.status]: Joi.string(),
        [Project_1.ProjectFields.minStartTime]: Joi.date(),
        [Project_1.ProjectFields.maxStartTime]: Joi.date(),
        [Project_1.ProjectFields.minFinishTime]: Joi.date(),
        [Project_1.ProjectFields.maxFinishTime]: Joi.date(),
        page: Joi.number().min(1),
        orderBy: Joi.string().valid('updatedAt', 'createdAt', Project_1.ProjectFields.maxBudget).required(),
        orderType: Joi.string().valid('ASC', 'DESC').required(),
        filterFollow: Joi.boolean(),
    }
};
exports.VALIDATION_PROJECTS_DETAIL_ONE = {
    options: { allowUnknownBody: false },
    params: {
        id: Joi.number().required()
    }
};
exports.VALIDATION_PROJECTS_CREATE = {
    options: { allowUnknownBody: false },
    body: {
        [Project_1.ProjectFields.name]: Joi.string().min(5).required(),
        [Project_1.ProjectFields.description]: Joi.string().min(5),
        [Project_1.ProjectFields.documents]: Joi.array().items(Joi.object()),
        [Project_1.ProjectFields.listVideos]: Joi.array().items(Joi.string()),
        [Project_1.ProjectFields.listImages]: Joi.array().items(Joi.string()),
        [Project_1.ProjectFields.projectType]: Joi.string(),
        [Project_1.ProjectFields.projectSubType]: Joi.string(),
        [Project_1.ProjectFields.workType]: Joi.string(),
        [Project_1.ProjectFields.address]: Joi.string().required(),
        [Project_1.ProjectFields.village]: Joi.string(),
        [Project_1.ProjectFields.district]: Joi.string().required(),
        [Project_1.ProjectFields.city]: Joi.string().required(),
        [Project_1.ProjectFields.country]: Joi.string().required(),
        [Project_1.ProjectFields.minBudget]: Joi.number(),
        [Project_1.ProjectFields.maxBudget]: Joi.number(),
        [Project_1.ProjectFields.investmentType]: Joi.string(),
        [Project_1.ProjectFields.minConstructionArea]: Joi.number(),
        [Project_1.ProjectFields.maxConstructionArea]: Joi.number(),
        [Project_1.ProjectFields.minLandArea]: Joi.number(),
        [Project_1.ProjectFields.maxLandArea]: Joi.number(),
        [Project_1.ProjectFields.totalFloorArea]: Joi.number(),
        [Project_1.ProjectFields.state]: Joi.string(),
        [Project_1.ProjectFields.status]: Joi.string(),
        [Project_1.ProjectFields.minStartTime]: Joi.date(),
        [Project_1.ProjectFields.maxStartTime]: Joi.date(),
        [Project_1.ProjectFields.minFinishTime]: Joi.date(),
        [Project_1.ProjectFields.maxFinishTime]: Joi.date(),
        [Project_1.ProjectFields.purpose]: Joi.string(),
    }
};
exports.VALIDATION_PROJECTS_UPDATE = {
    options: { allowUnknownBody: false },
    body: {
        [Project_1.ProjectFields.name]: Joi.string().min(5),
        [Project_1.ProjectFields.description]: Joi.string(),
        [Project_1.ProjectFields.documents]: Joi.array().items(Joi.object()),
        [Project_1.ProjectFields.listVideos]: Joi.array().items(Joi.string()),
        [Project_1.ProjectFields.listImages]: Joi.array().items(Joi.string()),
        [Project_1.ProjectFields.projectType]: Joi.string(),
        [Project_1.ProjectFields.projectSubType]: Joi.string(),
        [Project_1.ProjectFields.workType]: Joi.string(),
        [Project_1.ProjectFields.address]: Joi.string(),
        [Project_1.ProjectFields.village]: Joi.string(),
        [Project_1.ProjectFields.district]: Joi.string(),
        [Project_1.ProjectFields.city]: Joi.string(),
        [Project_1.ProjectFields.country]: Joi.string(),
        [Project_1.ProjectFields.minBudget]: Joi.number(),
        [Project_1.ProjectFields.maxBudget]: Joi.number(),
        [Project_1.ProjectFields.investmentType]: Joi.string(),
        [Project_1.ProjectFields.minConstructionArea]: Joi.number(),
        [Project_1.ProjectFields.maxConstructionArea]: Joi.number(),
        [Project_1.ProjectFields.minLandArea]: Joi.number(),
        [Project_1.ProjectFields.maxLandArea]: Joi.number(),
        [Project_1.ProjectFields.totalFloorArea]: Joi.number(),
        [Project_1.ProjectFields.state]: Joi.string(),
        [Project_1.ProjectFields.status]: Joi.string(),
        [Project_1.ProjectFields.minStartTime]: Joi.date(),
        [Project_1.ProjectFields.maxStartTime]: Joi.date(),
        [Project_1.ProjectFields.minFinishTime]: Joi.date(),
        [Project_1.ProjectFields.maxFinishTime]: Joi.date(),
        [Project_1.ProjectFields.purpose]: Joi.string(),
    }
};
exports.VALIDATION_APPROVED_PROJECT = {
    options: { allowUnknownBody: false },
    body: {
        [Project_1.ProjectFields.id]: Joi.string().required(),
        [Project_1.ProjectFields.approved]: Joi.boolean().required()
    }
};
exports.VALIDATION_ADMIN_LIST_PROJECTS = {
    options: { allowUnknownBody: false },
    body: {
        [Project_1.ProjectFields.approved]: Joi.boolean().required(),
        [Project_1.ProjectFields.name]: Joi.string(),
        [Project_1.ProjectFields.projectType]: Joi.string(),
        [Project_1.ProjectFields.projectSubType]: Joi.string(),
        [Project_1.ProjectFields.workType]: Joi.string(),
        [Project_1.ProjectFields.district]: Joi.string(),
        [Project_1.ProjectFields.city]: Joi.array().items(Joi.string()),
        [Project_1.ProjectFields.country]: Joi.string(),
        [Project_1.ProjectFields.minBudget]: Joi.number(),
        [Project_1.ProjectFields.maxBudget]: Joi.number(),
        [Project_1.ProjectFields.investmentType]: Joi.string(),
        [Project_1.ProjectFields.minConstructionArea]: Joi.number(),
        [Project_1.ProjectFields.maxConstructionArea]: Joi.number(),
        [Project_1.ProjectFields.minLandArea]: Joi.number(),
        [Project_1.ProjectFields.maxLandArea]: Joi.number(),
        [Project_1.ProjectFields.totalFloorArea]: Joi.number(),
        [Project_1.ProjectFields.state]: Joi.string(),
        [Project_1.ProjectFields.status]: Joi.string(),
        [Project_1.ProjectFields.minStartTime]: Joi.date(),
        [Project_1.ProjectFields.maxStartTime]: Joi.date(),
        [Project_1.ProjectFields.minFinishTime]: Joi.date(),
        [Project_1.ProjectFields.maxFinishTime]: Joi.date(),
        page: Joi.number().min(1),
        orderBy: Joi.string().valid('updatedAt', 'createdAt', Project_1.ProjectFields.maxBudget).required(),
        orderType: Joi.string().valid('ASC', 'DESC').required(),
    }
};
//# sourceMappingURL=ValidatorProject.js.map