/**
 * Created by nam on 3/26/2017.
 */
/*
 * Validation for rounter projects
 * */
import {Validation} from "../../constants/schema/Validation";
import {ProjectFields} from "../../databases/models/Project";
import {ContractorFields} from "../../databases/models/Contractor";

var Joi = require('joi');
export const VALIDATION_PROJECTS_SEARCH = {
    options: {allowUnknownBody: false},
    body: {
        [ProjectFields.name]: Joi.string(),
        [ProjectFields.projectType]: Joi.string(),
        [ProjectFields.projectSubType]: Joi.string(),
        [ProjectFields.workType]: Joi.string(),
        [ProjectFields.district]: Joi.string(),
        [ProjectFields.city]: Joi.array().items(Joi.string()),
        [ProjectFields.country]: Joi.string(),
        [ProjectFields.minBudget]: Joi.number(),
        [ProjectFields.maxBudget]: Joi.number(),
        [ProjectFields.investmentType]: Joi.string(),
        [ProjectFields.minConstructionArea]: Joi.number(),
        [ProjectFields.maxConstructionArea]: Joi.number(),
        [ProjectFields.minLandArea]: Joi.number(),
        [ProjectFields.maxLandArea]: Joi.number(),
        [ProjectFields.totalFloorArea]: Joi.number(),
        [ProjectFields.state]: Joi.string(),
        [ProjectFields.status]: Joi.string(),
        [ProjectFields.minStartTime]: Joi.date(),
        [ProjectFields.maxStartTime]: Joi.date(),
        [ProjectFields.minFinishTime]: Joi.date(),
        [ProjectFields.maxFinishTime]: Joi.date(),
        [ProjectFields.isHot]: Joi.boolean(),
        page: Joi.number().min(1),
        orderBy: Joi.string().valid('updatedAt', 'createdAt', ProjectFields.maxBudget).required(),
        orderType: Joi.string().valid('ASC', 'DESC').required(),
        filterFollow: Joi.boolean(),
    }
}
export const VALIDATION_PROJECTS_DETAIL_ONE = {
    options: {allowUnknownBody: false},
    params: {
        id: Joi.number().required()
    }
}
export const VALIDATION_PROJECTS_CREATE = {
    options: {allowUnknownBody: false},
    body: {
        [ProjectFields.name]: Joi.string().min(5).required(),
        [ProjectFields.description]: Joi.string().min(5),
        [ProjectFields.documents]: Joi.array().items(Joi.object()),
        [ProjectFields.listVideos]: Joi.array().items(Joi.string()),
        [ProjectFields.listImages]: Joi.array().items(Joi.string()),
        [ProjectFields.projectType]: Joi.string(),
        [ProjectFields.projectSubType]: Joi.string(),
        [ProjectFields.workType]: Joi.string(),
        [ProjectFields.address]: Joi.string().required(),
        [ProjectFields.village]: Joi.string(),
        [ProjectFields.district]: Joi.string().required(),
        [ProjectFields.city]: Joi.string().required(),
        [ProjectFields.country]: Joi.string().required(),
        [ProjectFields.minBudget]: Joi.number(),
        [ProjectFields.maxBudget]: Joi.number(),
        [ProjectFields.investmentType]: Joi.string(),
        [ProjectFields.minConstructionArea]: Joi.number(),
        [ProjectFields.maxConstructionArea]: Joi.number(),
        [ProjectFields.minLandArea]: Joi.number(),
        [ProjectFields.maxLandArea]: Joi.number(),
        [ProjectFields.totalFloorArea]: Joi.number(),
        [ProjectFields.state]: Joi.string(),
        [ProjectFields.status]: Joi.string(),
        [ProjectFields.minStartTime]: Joi.date(),
        [ProjectFields.maxStartTime]: Joi.date(),
        [ProjectFields.minFinishTime]: Joi.date(),
        [ProjectFields.maxFinishTime]: Joi.date(),
        [ProjectFields.purpose]: Joi.string(),

        // typeOfContractor: Validation.ARRAY_TYPE_OF_CONTRACTOR,
        // typeOfConstruction: Validation.TYPE_OF_CONTRUCTION,
        // typeOfProject: Validation.TYPE_OF_PROJECT,
        // name: Validation.NAME.required(),
        // address: Validation.ADDRESS.required(),
        // location: Validation.LOCATION,
        // city: Validation.CITY.required(),
        // country: Validation.COUNTRY.required(),
        // district: Validation.DISTRICT.required(),
        // phase: Validation.PHASE,// Giai đoạn của dự án bao gồm [chuẩn bị, đấu thầu, thi công, hoàn thành]
        // startBuildTime: Validation.DATE,
        // endBuildTime: Validation.DATE,
        // startBuildTimeMin: Validation.DATE,
        // startBuildTimeMax: Validation.DATE,
        // endBuildTimeMin: Validation.DATE,
        // endBuildTimeMax: Validation.DATE,
        //
        // fromBudget: Validation.BUGET,
        // endBudget: Validation.BUGET,
        // unitBugget: Validation.CURRENCY_UNIT,
        //
        // typeOfInvestment: Validation.TYPE_OF_INVESTMENT,
        //
        // startScheduled: Validation.DATE,
        // endScheduled: Validation.DATE,
        //
        // bidSolicitor: Validation.BIDDING_SOLICITOR, //ID Các bên mên mời thầu
        //
        // biddingForm: Joi.number(), //Hình thức đấu thầu
        //
        // countryOfContractor: Validation.COUNTRY,
        // districtOfContractor: Validation.COUNTRY,
        //
        // bidOpenTime: Validation.DATE, // Thời gian mở thầu
        // bidCloseTime: Validation.DATE, // Thời gian đóng thầu
        //
        // minimumExperienceOfContractor: Joi.number(),
        // creditRatingOfContractor: Validation.RANKING,// Mức tín nhiệm
        // minimumRevenueOfContractor: Validation.REVENUE,//
        // unitRevenueOfContractor: Validation.CURRENCY_UNIT,
        // otherRequirements: Joi.string(),
        // listImage: Joi.array().items(Joi.string()).min(0).required(),
        // listVideo: Joi.array().items(Joi.string()).min(0).required(),
        // investment: Validation.INVESTMENT,
        // unitInvestment: Validation.CURRENCY_UNIT,
        //
        //
        // areaOfLandMin: Validation.AREA, // Dien tich dat
        // areaOfLandMax: Validation.AREA, // Dien tich dat
        // unitAreaOfLand: Validation.AREA_UNIT,
        //
        // contructionAreaMin: Validation.AREA,//Dien tich xay dung
        // contructionAreaMax: Validation.AREA,//Dien tich xay dung
        // unitContructionArea: Validation.AREA_UNIT,
        // projectMaterials:Joi.array().items(Joi.object()).min(0).required(),
        // projectDocuments:Joi.array().items(Joi.object()).min(0).required(),
        //
        // stage: Joi.string(),
        // status: Joi.string(),
        // propertyType: Joi.string(),
        // subPropertyType: Joi.string(),
        // purpose: Joi.string(),
        // ownerRoll: Joi.string(),
        // description: Joi.string(),
    }
}

export const VALIDATION_PROJECTS_UPDATE = {
    options: {allowUnknownBody: false},
    body: {
        [ProjectFields.name]: Joi.string().min(5),
        [ProjectFields.description]: Joi.string(),
        [ProjectFields.documents]: Joi.array().items(Joi.object()),
        [ProjectFields.listVideos]: Joi.array().items(Joi.string()),
        [ProjectFields.listImages]: Joi.array().items(Joi.string()),
        [ProjectFields.projectType]: Joi.string(),
        [ProjectFields.projectSubType]: Joi.string(),
        [ProjectFields.workType]: Joi.string(),
        [ProjectFields.address]: Joi.string(),
        [ProjectFields.village]: Joi.string(),
        [ProjectFields.district]: Joi.string(),
        [ProjectFields.city]: Joi.string(),
        [ProjectFields.country]: Joi.string(),
        [ProjectFields.minBudget]: Joi.number(),
        [ProjectFields.maxBudget]: Joi.number(),
        [ProjectFields.investmentType]: Joi.string(),
        [ProjectFields.minConstructionArea]: Joi.number(),
        [ProjectFields.maxConstructionArea]: Joi.number(),
        [ProjectFields.minLandArea]: Joi.number(),
        [ProjectFields.maxLandArea]: Joi.number(),
        [ProjectFields.totalFloorArea]: Joi.number(),
        [ProjectFields.state]: Joi.string(),
        [ProjectFields.status]: Joi.string(),
        [ProjectFields.minStartTime]: Joi.date(),
        [ProjectFields.maxStartTime]: Joi.date(),
        [ProjectFields.minFinishTime]: Joi.date(),
        [ProjectFields.maxFinishTime]: Joi.date(),
        [ProjectFields.purpose]: Joi.string()
    }
}
export const VALIDATION_APPROVED_PROJECT = {
    options: {allowUnknownBody: false},
    body: {
        [ProjectFields.id]: Joi.string().required(),
        [ProjectFields.approved]: Joi.boolean().required()
    }
};
export const VALIDATION_ADMIN_LIST_PROJECTS = {
    options: {allowUnknownBody: false},
    body: {
        [ProjectFields.approved]: Joi.boolean().required(),
        [ProjectFields.name]: Joi.string(),
        [ProjectFields.projectType]: Joi.string(),
        [ProjectFields.projectSubType]: Joi.string(),
        [ProjectFields.workType]: Joi.string(),
        [ProjectFields.district]: Joi.string(),
        [ProjectFields.city]: Joi.array().items(Joi.string()),
        [ProjectFields.country]: Joi.string(),
        [ProjectFields.minBudget]: Joi.number(),
        [ProjectFields.maxBudget]: Joi.number(),
        [ProjectFields.investmentType]: Joi.string(),
        [ProjectFields.minConstructionArea]: Joi.number(),
        [ProjectFields.maxConstructionArea]: Joi.number(),
        [ProjectFields.minLandArea]: Joi.number(),
        [ProjectFields.maxLandArea]: Joi.number(),
        [ProjectFields.totalFloorArea]: Joi.number(),
        [ProjectFields.state]: Joi.string(),
        [ProjectFields.status]: Joi.string(),
        [ProjectFields.minStartTime]: Joi.date(),
        [ProjectFields.maxStartTime]: Joi.date(),
        [ProjectFields.minFinishTime]: Joi.date(),
        [ProjectFields.maxFinishTime]: Joi.date(),
        [ProjectFields.isHot]: Joi.boolean(),
        page: Joi.number().min(1),
        orderBy: Joi.string().valid('updatedAt', 'createdAt', ProjectFields.maxBudget).required(),
        orderType: Joi.string().valid('ASC', 'DESC').required(),
    }
};
