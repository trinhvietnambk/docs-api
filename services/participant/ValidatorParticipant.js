"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Participant_1 = require("../../databases/models/Participant");
const EntityName_1 = require("../../constants/database/EntityName");
const Account_1 = require("../../constants/database/Account");
var Joi = require('joi');
exports.VALIDATION_PARTICIPANTS_CREATE = {
    options: { allowUnknownBody: false },
    body: {
        [Participant_1.ParticipantFields.name]: Joi.string(),
        [Participant_1.ParticipantFields.participantType]: Joi.string().valid(EntityName_1.CONTRACTOR, Account_1.USER_ACCOUNT).required(),
        [Participant_1.ParticipantFields.participantId]: Joi.string().required(),
        [Participant_1.ParticipantFields.projectId]: Joi.string().required(),
        [Participant_1.ParticipantFields.role]: Joi.string().required(),
        [Participant_1.ParticipantFields.contacts]: Joi.array().items(Joi.object()),
    }
};
exports.VALIDATION_PARTICIPANTS_DELETE = {
    options: { allowUnknownBody: false },
    body: {
        [Participant_1.ParticipantFields.participantType]: Joi.string().valid(EntityName_1.CONTRACTOR, Account_1.USER_ACCOUNT).required(),
        [Participant_1.ParticipantFields.participantId]: Joi.string().required(),
        [Participant_1.ParticipantFields.projectId]: Joi.string().required(),
    }
};
exports.VALIDATION_PARTICIPANTS_LIST = {
    options: { allowUnknownBody: false },
    body: {
        [Participant_1.ParticipantFields.projectId]: Joi.string().required()
    }
};
//# sourceMappingURL=ValidatorParticipant.js.map