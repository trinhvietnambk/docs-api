import {ParticipantFields} from "../../databases/models/Participant";
import {CONTRACTOR} from "../../constants/database/EntityName";
import {USER_ACCOUNT} from "../../constants/database/Account";
var Joi = require('joi');
export const VALIDATION_PARTICIPANTS_CREATE = {
    options: {allowUnknownBody: false},
    body: {
        [ParticipantFields.name]: Joi.string(),
        [ParticipantFields.participantType]: Joi.string().valid(CONTRACTOR,USER_ACCOUNT).required(),
        [ParticipantFields.participantId]: Joi.string().required(),
        [ParticipantFields.projectId]: Joi.string().required(),
        [ParticipantFields.role]: Joi.string().required(),
        [ParticipantFields.contacts]: Joi.array().items(Joi.object()),
    }
};
export const VALIDATION_PARTICIPANTS_DELETE = {
    options: {allowUnknownBody: false},
    body: {
        [ParticipantFields.participantType]: Joi.string().valid(CONTRACTOR,USER_ACCOUNT).required(),
        [ParticipantFields.participantId]: Joi.string().required(),
        [ParticipantFields.projectId]: Joi.string().required(),
    }
};
export const VALIDATION_PARTICIPANTS_LIST = {
    options: {allowUnknownBody: false},
    body: {
        [ParticipantFields.projectId]: Joi.string().required()
    }
};
