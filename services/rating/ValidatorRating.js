"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Rating_1 = require("../../databases/models/Rating");
var Joi = require('joi');
exports.VALIDATION_RATING_CREATE = {
    options: { allowUnknownBody: false },
    body: {
        [Rating_1.RatingFields.subjectId]: Joi.string().required(),
        [Rating_1.RatingFields.subjectType]: Joi.string().required(),
        [Rating_1.RatingFields.objectId]: Joi.string().required(),
        [Rating_1.RatingFields.objectType]: Joi.string().required(),
        [Rating_1.RatingFields.content]: Joi.string(),
        [Rating_1.RatingFields.star]: Joi.number().required(),
        [Rating_1.RatingFields.role]: Joi.string().required(),
    }
};
exports.VALIDATION_RATING_UPDATE = {
    options: { allowUnknownBody: false },
    body: {
        [Rating_1.RatingFields.id]: Joi.string().required(),
        [Rating_1.RatingFields.content]: Joi.string(),
        [Rating_1.RatingFields.star]: Joi.number(),
        [Rating_1.RatingFields.role]: Joi.string(),
    }
};
exports.VALIDATION_RATING_DELETE = {
    options: { allowUnknownBody: false },
    body: {
        [Rating_1.RatingFields.id]: Joi.string().required(),
    }
};
exports.VALIDATION_RATING_LIST = {
    options: { allowUnknownBody: false },
    body: {
        [Rating_1.RatingFields.objectId]: Joi.string().required(),
        page: Joi.number()
    }
};
//# sourceMappingURL=ValidatorRating.js.map