import {RatingFields} from "../../databases/models/Rating";

var Joi = require('joi');
export const VALIDATION_RATING_CREATE = {
    options: {allowUnknownBody: false},
    body: {
        [RatingFields.subjectId]: Joi.string().required(),
        [RatingFields.subjectType]: Joi.string().required(),
        [RatingFields.objectId]: Joi.string().required(),
        [RatingFields.objectType]: Joi.string().required(),
        [RatingFields.content]: Joi.string(),
        [RatingFields.star]: Joi.number().required(),
        [RatingFields.role]: Joi.string().required(),
    }
};
export const VALIDATION_RATING_UPDATE = {
    options: {allowUnknownBody: false},
    body: {
        [RatingFields.id]: Joi.string().required(),
        [RatingFields.content]: Joi.string(),
        [RatingFields.star]: Joi.number(),
        [RatingFields.role]: Joi.string(),
    }
};
export const VALIDATION_RATING_DELETE = {
    options: {allowUnknownBody: false},
    body: {
        [RatingFields.id]: Joi.string().required(),
    }
};
export const VALIDATION_RATING_LIST = {
    options: {allowUnknownBody: false},
    body: {
        [RatingFields.objectId]: Joi.string().required(),
        page: Joi.number()
    }
};
