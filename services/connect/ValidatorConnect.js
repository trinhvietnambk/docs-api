"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Connect_1 = require("../../databases/models/Connect");
const Account_1 = require("../../constants/database/Account");
const EntityName_1 = require("../../constants/database/EntityName");
var Joi = require('joi');
exports.VALIDATION_CONNECTS_CREATE = {
    options: { allowUnknownBody: false },
    body: {
        [Connect_1.ConnectFields.subjectId]: Joi.string().required(),
        [Connect_1.ConnectFields.subjectType]: Joi.string().valid(Account_1.USER_ACCOUNT).required(),
        [Connect_1.ConnectFields.objectId]: Joi.string().required(),
        [Connect_1.ConnectFields.objectType]: Joi.string()
            .when(Connect_1.ConnectFields.action, { is: 'FOLLOWING', then: Joi.valid(EntityName_1.CONTRACTOR, EntityName_1.PROJECT, Account_1.USER_ACCOUNT, EntityName_1.PRODUCT) })
            .when(Connect_1.ConnectFields.action, { is: 'LIKE', then: Joi.valid(EntityName_1.CONTRACTOR, EntityName_1.PROJECT, EntityName_1.POST, EntityName_1.COMMENT) }),
        [Connect_1.ConnectFields.action]: Joi.string().valid('FOLLOWING', 'LIKE').required(),
    }
};
exports.VALIDATION_CONNECTS_DELETE = {
    options: { allowUnknownBody: false },
    body: {
        [Connect_1.ConnectFields.subjectId]: Joi.string().required(),
        [Connect_1.ConnectFields.subjectType]: Joi.string().required(),
        [Connect_1.ConnectFields.objectId]: Joi.string().required(),
        [Connect_1.ConnectFields.objectType]: Joi.string().required(),
        [Connect_1.ConnectFields.action]: Joi.string().required(),
    }
};
exports.VALIDATION_CONNECTS_LIST_SUBJECTS = {
    options: { allowUnknownBody: false },
    body: {
        [Connect_1.ConnectFields.subjectType]: Joi.string().required(),
        [Connect_1.ConnectFields.objectId]: Joi.string().required(),
        [Connect_1.ConnectFields.action]: Joi.string().required(),
        page: Joi.number()
    }
};
exports.VALIDATION_CONNECTS_LIST_OBJECTS = {
    options: { allowUnknownBody: false },
    body: {
        [Connect_1.ConnectFields.subjectId]: Joi.string().required(),
        [Connect_1.ConnectFields.objectType]: Joi.string().required(),
        [Connect_1.ConnectFields.action]: Joi.string().required(),
        page: Joi.number()
    }
};
exports.VALIDATION_CONNECTS_COUNT_SUBJECTS = {
    options: { allowUnknownBody: false },
    body: {
        [Connect_1.ConnectFields.subjectType]: Joi.string().required(),
        [Connect_1.ConnectFields.objectId]: Joi.string().required(),
        [Connect_1.ConnectFields.action]: Joi.string().required()
    }
};
exports.VALIDATION_CONNECTS_COUNT_OBJECTS = {
    options: { allowUnknownBody: false },
    body: {
        [Connect_1.ConnectFields.subjectId]: Joi.string().required(),
        [Connect_1.ConnectFields.objectType]: Joi.string().required(),
        [Connect_1.ConnectFields.action]: Joi.string().required()
    }
};
//# sourceMappingURL=ValidatorConnect.js.map