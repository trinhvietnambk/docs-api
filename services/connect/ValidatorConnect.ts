import {ConnectFields} from "../../databases/models/Connect";
import {USER_ACCOUNT} from "../../constants/database/Account";
import {COMMENT, CONTRACTOR, POST, PRODUCT, PROJECT} from "../../constants/database/EntityName";

var Joi = require('joi');
export const VALIDATION_CONNECTS_CREATE = {
    options: {allowUnknownBody: false},
    body: {
        [ConnectFields.subjectId]: Joi.string().required(),
        [ConnectFields.subjectType]: Joi.string().valid(USER_ACCOUNT).required(),
        [ConnectFields.objectId]: Joi.string().required(),
        [ConnectFields.objectType]: Joi.string()
            .when(ConnectFields.action, {is: 'FOLLOWING', then: Joi.valid(CONTRACTOR, PROJECT, USER_ACCOUNT,PRODUCT)})
            .when(ConnectFields.action, {is: 'LIKE', then: Joi.valid(CONTRACTOR, PROJECT, POST, COMMENT)}),
        [ConnectFields.action]: Joi.string().valid('FOLLOWING', 'LIKE').required(),
    }
};
export const VALIDATION_CONNECTS_DELETE = {
    options: {allowUnknownBody: false},
    body: {
        [ConnectFields.subjectId]: Joi.string().required(),
        [ConnectFields.subjectType]: Joi.string().required(),
        [ConnectFields.objectId]: Joi.string().required(),
        [ConnectFields.objectType]: Joi.string().required(),
        [ConnectFields.action]: Joi.string().required(),
    }
};
export const VALIDATION_CONNECTS_LIST_SUBJECTS = {
    options: {allowUnknownBody: false},
    body: {
        [ConnectFields.subjectType]: Joi.string().required(),
        [ConnectFields.objectId]: Joi.string().required(),
        [ConnectFields.action]: Joi.string().required(),
        page: Joi.number()
    }
};
export const VALIDATION_CONNECTS_LIST_OBJECTS = {
    options: {allowUnknownBody: false},
    body: {
        [ConnectFields.subjectId]: Joi.string().required(),
        [ConnectFields.objectType]: Joi.string().required(),
        [ConnectFields.action]: Joi.string().required(),
        page: Joi.number()
    }
};
export const VALIDATION_CONNECTS_COUNT_SUBJECTS = {
    options: {allowUnknownBody: false},
    body: {
        [ConnectFields.subjectType]: Joi.string().required(),
        [ConnectFields.objectId]: Joi.string().required(),
        [ConnectFields.action]: Joi.string().required()
    }
};
export const VALIDATION_CONNECTS_COUNT_OBJECTS = {
    options: {allowUnknownBody: false},
    body: {
        [ConnectFields.subjectId]: Joi.string().required(),
        [ConnectFields.objectType]: Joi.string().required(),
        [ConnectFields.action]: Joi.string().required()
    }
};
