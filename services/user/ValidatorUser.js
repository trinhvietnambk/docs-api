"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Validation_1 = require("../../constants/schema/Validation");
const EntityName_1 = require("../../constants/database/EntityName");
const User_1 = require("../../databases/models/User");
/**
 * Created by NamTV on 3/28/2017.
 */
var Joi = require('joi');
exports.VALIDATION_ACCOUNTS_UPDATE = {
    options: { allowUnknownBody: false },
    body: {
        [User_1.UserFields.id]: Joi.string(),
        [User_1.UserFields.type]: Joi.array().items(Joi.string().valid('ProjectOwner', 'Contractor', 'Supplier')),
        [User_1.UserFields.name]: Joi.string().max(200),
        [User_1.UserFields.lastName]: Joi.string().max(200),
        [User_1.UserFields.firstName]: Joi.string().max(200),
        [User_1.UserFields.address]: Joi.string().max(200),
        [User_1.UserFields.city]: Joi.string().max(50),
        [User_1.UserFields.country]: Joi.string().max(50),
        [User_1.UserFields.district]: Joi.string().max(50),
        [User_1.UserFields.phone]: Joi.string().max(15),
        [User_1.UserFields.avatar]: Joi.string(),
        [User_1.UserFields.cover]: Joi.string(),
        [User_1.UserFields.language]: Joi.string(),
        [User_1.UserFields.about]: Joi.string()
    }
};
exports.VALIDATION_ACCOUNTS_UPDATE_EMAIL_NOTIFY = {
    options: { allowUnknownBody: false },
    body: {
        emailSettingNotifications: Joi.object().keys({
            '1': Joi.boolean().required(),
            '2': Joi.boolean().required(),
            '3': Joi.boolean().required(),
            '4': Joi.boolean().required(),
            '5': Joi.boolean().required(),
            '6': Joi.boolean().required(),
            '7': Joi.boolean().required(),
            '8': Joi.boolean().required(),
            '9': Joi.boolean().required(),
            '10': Joi.boolean().required(),
            '11': Joi.boolean().required(),
        }).required(),
        emailNofity: Joi.string().required(),
        password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
        formatEmail: Joi.number()
    }
};
exports.VALIDATION_ACCOUNTS_CHANGE_PASSWORD = {
    options: { allowUnknownBody: false },
    body: {
        password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
        newPassword: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
    }
};
exports.VALIDATION_ACCOUNTS_RESET_PASSWORD = {
    options: { allowUnknownBody: false },
    body: {
        temPassword: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
        newPassword: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
    }
};
exports.VALIDATION_ACCOUNTS_FORGET_PASSWORD = {
    options: { allowUnknownBody: false },
    body: {
        email: Validation_1.Validation.EMAIL.required()
    }
};
exports.VALIDATION_EXTRAS_ACCOUNT = {
    options: { allowUnknownBody: false },
    params: {
        type: Joi.string().valid(EntityName_1.PROJECT, EntityName_1.CONTRACTOR).required()
    },
    body: {
        phase: Joi.number()
    }
};
exports.VALIDATION_PROJECT_OF_ACCOUNT = {
    options: { allowUnknownBody: false },
    body: {
        phase: Joi.number(),
        name: Joi.string(),
        page: Joi.number()
    }
};
exports.VALIDATION_CONTRACTOR_OF_ACCOUNT = {
    options: { allowUnknownBody: false },
    body: {
        name: Joi.string()
    }
};
exports.VALIDATION_LOGIN = {
    options: { allowUnknownBody: false },
    body: {
        email: Joi.string().email().when('type', { is: 'normal', then: Joi.required() }),
        password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).when('type', { is: 'normal', then: Joi.required() }),
        type: Joi.string().valid('normal', 'facebook', 'google').required(),
        accessToken: Joi.string()
            .when('type', { is: 'facebook', then: Joi.required() })
            .when('type', { is: 'google', then: Joi.required() }),
        idToken: Joi.string()
            .when('type', { is: 'google', then: Joi.required() }),
    }
};
exports.VALIDATION_LOGOUT = {
    options: { allowUnknownBody: false },
    body: {}
};
exports.VALIDATION_BLOCK_USER = {
    options: { allowUnknownBody: false },
    body: {
        [User_1.UserFields.id]: Joi.string().required()
    }
};
exports.VALIDATION_UN_BLOCK_USER = {
    options: { allowUnknownBody: false },
    body: {
        [User_1.UserFields.id]: Joi.string().required()
    }
};
exports.VALIDATION_REGISTERS_ACCOUNT = {
    options: { allowUnknownBody: false },
    body: {
        // type: Joi.string().valid(USER_ACCOUNT,COMPANY_ACCOUNT),// User hoặc công ty
        // name: Joi.string().max(200).required(),
        [User_1.UserFields.type]: Joi.array().items(Joi.string().valid('ProjectOwner', 'Contractor', 'Supplier')).min(1).required(),
        lastName: Joi.string().max(200).required(),
        firstName: Joi.string().max(200).required(),
        email: Joi.string().email().required(),
        password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
    }
};
//# sourceMappingURL=ValidatorUser.js.map