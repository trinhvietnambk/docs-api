import {USER_ACCOUNT, COMPANY_ACCOUNT} from "../../constants/database/Account";
import {Validation} from "../../constants/schema/Validation";
import {PROJECT, CONTRACTOR} from "../../constants/database/EntityName";
import {UserFields} from "../../databases/models/User";

/**
 * Created by NamTV on 3/28/2017.
 */
var Joi = require('joi');
export const VALIDATION_ACCOUNTS_UPDATE = {
    options: {allowUnknownBody: false},
    body: {
        [UserFields.id]: Joi.string(),
        [UserFields.type]: Joi.array().items(Joi.string().valid('ProjectOwner', 'Contractor', 'Supplier')),
        [UserFields.name]: Joi.string().max(200),
        [UserFields.lastName]: Joi.string().max(200),
        [UserFields.firstName]: Joi.string().max(200),
        [UserFields.address]: Joi.string().max(200),
        [UserFields.city]: Joi.string().max(50),
        [UserFields.country]: Joi.string().max(50),
        [UserFields.district]: Joi.string().max(50),
        [UserFields.phone]: Joi.string().max(15),
        [UserFields.avatar]: Joi.string(),
        [UserFields.cover]: Joi.string(),
        [UserFields.language]: Joi.string(),
        [UserFields.about]: Joi.string()
    }
};

export const VALIDATION_ACCOUNTS_UPDATE_EMAIL_NOTIFY = {
    options: {allowUnknownBody: false},
    body: {
        emailSettingNotifications: Joi.object().keys({
            '1': Joi.boolean().required(),
            '2': Joi.boolean().required(),
            '3': Joi.boolean().required(),
            '4': Joi.boolean().required(),
            '5': Joi.boolean().required(),
            '6': Joi.boolean().required(),
            '7': Joi.boolean().required(),
            '8': Joi.boolean().required(),
            '9': Joi.boolean().required(),
            '10': Joi.boolean().required(),
            '11': Joi.boolean().required(),
        }).required(),
        emailNofity: Joi.string().required(),
        password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
        formatEmail: Joi.number()
    }
};

export const VALIDATION_ACCOUNTS_CHANGE_PASSWORD = {
    options: {allowUnknownBody: false},
    body: {
        password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
        newPassword: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
    }
};
export const VALIDATION_ACCOUNTS_RESET_PASSWORD = {
    options: {allowUnknownBody: false},
    body: {
        temPassword: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
        newPassword: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
    }
};

export const VALIDATION_ACCOUNTS_FORGET_PASSWORD = {
    options: {allowUnknownBody: false},
    body: {
        email: Validation.EMAIL.required()
    }
};

export const VALIDATION_EXTRAS_ACCOUNT = {
    options: {allowUnknownBody: false},
    params: {
        type: Joi.string().valid(PROJECT, CONTRACTOR).required()
    },
    body: {
        phase: Joi.number()
    }
};

export const VALIDATION_PROJECT_OF_ACCOUNT = {
    options: {allowUnknownBody: false},
    body: {
        phase: Joi.number(),
        name: Joi.string(),
        page: Joi.number()
    }
};
export const VALIDATION_CONTRACTOR_OF_ACCOUNT = {
    options: {allowUnknownBody: false},
    body: {
        name: Joi.string()
    }
};
export const VALIDATION_LOGIN ={
    options: { allowUnknownBody: false },
    body: {
        email: Joi.string().email().when('type', { is: 'normal', then: Joi.required() }),
        password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).when('type', { is: 'normal', then: Joi.required() }),
        type:Joi.string().valid('normal','facebook','google').required(),
        accessToken: Joi.string()
            .when('type', { is: 'facebook', then:Joi.required() })
            .when('type', { is: 'google', then:Joi.required() }),
        idToken: Joi.string()
            .when('type', { is: 'google', then:Joi.required() }),
    }
}
export const VALIDATION_LOGOUT ={
    options: { allowUnknownBody: false },
    body: {
    }
}
export const VALIDATION_BLOCK_USER ={
    options: { allowUnknownBody: false },
    body: {
        [UserFields.id]:Joi.string().required()
    }
}
export const VALIDATION_UN_BLOCK_USER ={
    options: { allowUnknownBody: false },
    body: {
        [UserFields.id]:Joi.string().required()
    }
}
export const VALIDATION_REGISTERS_ACCOUNT ={
    options: { allowUnknownBody: false },
    body: {
        // type: Joi.string().valid(USER_ACCOUNT,COMPANY_ACCOUNT),// User hoặc công ty
        // name: Joi.string().max(200).required(),

        [UserFields.type]: Joi.array().items(Joi.string().valid('ProjectOwner', 'Contractor', 'Supplier')).min(1).required(),
        lastName: Joi.string().max(200).required(),
        firstName: Joi.string().max(200).required(),
        email: Joi.string().email().required(),
        password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),

        // taxCode: Joi.string().when('type', { is: COMPANY_ACCOUNT, then: Joi.required() }),
        // address: Joi.string().max(200),
        // location: Joi.object().keys({
        //   lon:Joi.number().min(-180).max(180),
        //   lat:Joi.number().min(-180).max(180),
        // }),
        // city: Joi.string().max(50).required(),
        // country: Joi.string().max(50).required(),
        // district: Joi.string().max(50),
        // phone: Joi.string().max(15),
        // fax: Joi.string().max(15),
        // website: Joi.string().max(200),

        // language: Validation.LANGUAGE,

        // avatar: Validation.LOGO,
        // platform: Joi.string().required(),
        // os: Joi.string().required(),
        // pushToken: Joi.string().allow('')//.when('platform', { is: PLATFORM.MOBILE, then: Joi.required() }),
    }
}
