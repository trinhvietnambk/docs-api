import {ContactFields} from "../../databases/models/Contact";
var Joi = require('joi');
export const VALIDATION_CONTACTS_CREATE = {
    options: {allowUnknownBody: false},
    body: {
        [ContactFields.name]: Joi.string().required(),
        [ContactFields.firstName]: Joi.string(),
        [ContactFields.lastName]: Joi.string(),
        [ContactFields.email]: Joi.string(),
        [ContactFields.phone]: Joi.string(),
        [ContactFields.avatar]: Joi.string(),
        [ContactFields.position]: Joi.string(),
        [ContactFields.companyTaxCode]: Joi.string(),
        [ContactFields.userId]: Joi.string(),
    }
};
export const VALIDATION_CONTACTS_DELETE = {
    options: {allowUnknownBody: false},
    body: {
        [ContactFields.id]: Joi.string().required(),
    }
};
export const VALIDATION_CONTACTS_LIST = {
    options: {allowUnknownBody: false},
    body: {
        [ContactFields.companyTaxCode]: Joi.string().required()
    }
};
