"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Contact_1 = require("../../databases/models/Contact");
var Joi = require('joi');
exports.VALIDATION_CONTACTS_CREATE = {
    options: { allowUnknownBody: false },
    body: {
        [Contact_1.ContactFields.name]: Joi.string().required(),
        [Contact_1.ContactFields.firstName]: Joi.string(),
        [Contact_1.ContactFields.lastName]: Joi.string(),
        [Contact_1.ContactFields.email]: Joi.string(),
        [Contact_1.ContactFields.phone]: Joi.string(),
        [Contact_1.ContactFields.avatar]: Joi.string(),
        [Contact_1.ContactFields.position]: Joi.string(),
        [Contact_1.ContactFields.companyTaxCode]: Joi.string(),
        [Contact_1.ContactFields.userId]: Joi.string(),
    }
};
exports.VALIDATION_CONTACTS_DELETE = {
    options: { allowUnknownBody: false },
    body: {
        [Contact_1.ContactFields.id]: Joi.string().required(),
    }
};
exports.VALIDATION_CONTACTS_LIST = {
    options: { allowUnknownBody: false },
    body: {
        [Contact_1.ContactFields.companyTaxCode]: Joi.string().required()
    }
};
//# sourceMappingURL=ValidatorContact.js.map