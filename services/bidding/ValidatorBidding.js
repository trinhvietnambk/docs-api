"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Validation_1 = require("../../constants/schema/Validation");
const EntityName_1 = require("../../constants/database/EntityName");
const Quotation_1 = require("../../databases/models/Quotation");
const QuotationDraft_1 = require("../../databases/models/QuotationDraft");
/**
 * Created by nam on 3/29/2017.
 */
var Joi = require('joi');
exports.VALIDATION_BIDDING_PREPARE_QUOTATION = {
    options: { allowUnknownBody: false },
    body: {
        [Quotation_1.QuotationFields.fromId]: Joi.string().required(),
        [Quotation_1.QuotationFields.toId]: Joi.string().required(),
        [Quotation_1.QuotationFields.fromType]: Joi.string().required(),
        [Quotation_1.QuotationFields.toType]: Joi.string().required(),
        [Quotation_1.QuotationFields.contentLetter]: Joi.string(),
        [Quotation_1.QuotationFields.timeSendBidding]: Joi.date(),
        [Quotation_1.QuotationFields.note]: Joi.string(),
        [Quotation_1.QuotationFields.currency]: Joi.string().valid('VND'),
        [Quotation_1.QuotationFields.unitTime]: Joi.string().valid('DAY', 'HOUR', 'MONTH', 'YEAR'),
        [Quotation_1.QuotationFields.projectId]: Joi.string().required(),
        [Quotation_1.QuotationFields.documents]: Joi.array().items(Joi.object()),
        [Quotation_1.QuotationFields.works]: Joi.array().items(Joi.object()),
        [Quotation_1.QuotationFields.materials]: Joi.array().items(Joi.object()),
        [Quotation_1.QuotationFields.alternativeBid]: Joi.object(),
        [Quotation_1.QuotationFields.version]: Joi.number(),
    }
};
exports.VALIDATION_BIDDING_UPDATE_DRAFT_QUOTATION = {
    options: { allowUnknownBody: false },
    body: {
        [Quotation_1.QuotationFields.id]: Joi.string().required(),
        [Quotation_1.QuotationFields.contentLetter]: Joi.string(),
        [Quotation_1.QuotationFields.timeSendBidding]: Joi.date(),
        [Quotation_1.QuotationFields.note]: Joi.string(),
        [Quotation_1.QuotationFields.currency]: Joi.string().valid('VND'),
        [Quotation_1.QuotationFields.unitTime]: Joi.string().valid('DAY', 'HOUR', 'MONTH', 'YEAR'),
        [Quotation_1.QuotationFields.documents]: Joi.array().items(Joi.object()),
        [Quotation_1.QuotationFields.works]: Joi.array().items(Joi.object()),
        [Quotation_1.QuotationFields.materials]: Joi.array().items(Joi.object()),
        [Quotation_1.QuotationFields.alternativeBid]: Joi.object(),
        [Quotation_1.QuotationFields.version]: Joi.number(),
    }
};
exports.VALIDATION_BIDDING_SEND_QUOTATION = {
    options: { allowUnknownBody: false },
    body: {
        [Quotation_1.QuotationFields.fromId]: Joi.string().required(),
        [Quotation_1.QuotationFields.toId]: Joi.string().required(),
        [Quotation_1.QuotationFields.fromType]: Joi.string().required(),
        [Quotation_1.QuotationFields.toType]: Joi.string().required(),
        [Quotation_1.QuotationFields.contentLetter]: Joi.string(),
        [Quotation_1.QuotationFields.timeSendBidding]: Joi.date(),
        [Quotation_1.QuotationFields.note]: Joi.string(),
        [Quotation_1.QuotationFields.currency]: Joi.string().valid('VND').required(),
        [Quotation_1.QuotationFields.unitTime]: Joi.string().valid('DAY', 'HOUR', 'MONTH', 'YEAR').required(),
        [Quotation_1.QuotationFields.projectId]: Joi.string().required(),
        [Quotation_1.QuotationFields.documents]: Joi.array().items(Joi.object()),
        [Quotation_1.QuotationFields.works]: Joi.array().items(Joi.object()).required(),
        [Quotation_1.QuotationFields.materials]: Joi.array().items(Joi.object()),
        [Quotation_1.QuotationFields.alternativeBid]: Joi.object(),
        [Quotation_1.QuotationFields.version]: Joi.number(),
        [QuotationDraft_1.QuotationDraftFields.id]: Joi.string().required() //id of draf quotation
    }
};
exports.VALIDATION_BIDDING_DELETE_QUOTATION = {
    options: { allowUnknownBody: false },
    body: {
        [QuotationDraft_1.QuotationDraftFields.id]: Joi.string().required()
    }
};
exports.VALIDATION_BIDDING_UPDATE_QUOTATION_FEELING_STATUS = {
    options: { allowUnknownBody: false },
    body: {
        [Quotation_1.QuotationFields.id]: Joi.string().required(),
        [Quotation_1.QuotationFields.version]: Joi.number().required(),
        [Quotation_1.QuotationFields.feelingStatus]: Joi.string().valid(null, 'LIKE', 'DISLIKE').required(),
    }
};
exports.VALIDATION_BIDDING_UPDATE_QUOTATION_RESULT_STATUS = {
    options: { allowUnknownBody: false },
    body: {
        [Quotation_1.QuotationFields.id]: Joi.string().required(),
        [Quotation_1.QuotationFields.version]: Joi.number().required(),
        [Quotation_1.QuotationFields.biddingStatus]: Joi.string().valid('ACCEPT').required(),
    }
};
exports.VALIDATION_GET_DETAIL_QUOTATION = {
    options: { allowUnknownBody: false },
    body: {
        [Quotation_1.QuotationFields.id]: Joi.string().required(),
    }
};
exports.VALIDATION_GET_REVERSION_QUOTATION = {
    options: { allowUnknownBody: false },
    body: {
        [Quotation_1.QuotationFields.id]: Joi.string().required(),
        page: Joi.number().min(1),
    }
};
exports.VALIDATION_INVITE_BIDDING = {
    options: { allowUnknownBody: false },
    body: {
        [Quotation_1.QuotationFields.fromId]: Validation_1.Validation.ID.required(),
        [Quotation_1.QuotationFields.fromType]: Joi.string().valid(EntityName_1.PROJECT).required(),
        [Quotation_1.QuotationFields.toId]: Validation_1.Validation.ID.required(),
        [Quotation_1.QuotationFields.toType]: Joi.string().valid(EntityName_1.CONTRACTOR).required(),
        message: Joi.string().max(500).required(),
        messageType: Joi.string().required(),
    }
};
exports.VALIDATION_LIST_QUOTATION_FROM = {
    options: { allowUnknownBody: false },
    body: {
        [Quotation_1.QuotationFields.fromType]: Joi.string().valid(EntityName_1.CONTRACTOR).required(),
        [Quotation_1.QuotationFields.fromId]: Joi.string().required(),
        page: Joi.number().min(1)
    }
};
exports.VALIDATION_LIST_QUOTATION_TO = {
    options: { allowUnknownBody: false },
    body: {
        [Quotation_1.QuotationFields.toId]: Joi.string().required(),
        [Quotation_1.QuotationFields.toType]: Joi.string().valid(EntityName_1.BIDDING_CATEGORY).required(),
        page: Joi.number().min(1)
    }
};
exports.VALIDATION_LIST_RECEIVED_QUOTATION = {
    options: { allowUnknownBody: false },
    body: {
        receiverUserId: Joi.string(),
        receiverContractorId: Joi.string(),
        page: Joi.number().min(1)
    }
};
exports.VALIDATION_LIST_DRAFT_QUOTATION = {
    options: { allowUnknownBody: false },
    body: {
        [QuotationDraft_1.QuotationDraftFields.fromId]: Joi.string().required(),
        [QuotationDraft_1.QuotationDraftFields.toId]: Joi.string(),
        page: Joi.number().min(1)
    }
};
//# sourceMappingURL=ValidatorBidding.js.map