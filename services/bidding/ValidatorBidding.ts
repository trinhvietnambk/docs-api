import {Validation} from "../../constants/schema/Validation";
import {CONTRACTOR, BIDDING_CATEGORY, PROJECT} from "../../constants/database/EntityName";
import {QuotationFields} from "../../databases/models/Quotation";
import {QuotationDraftFields} from "../../databases/models/QuotationDraft";

/**
 * Created by nam on 3/29/2017.
 */
var Joi = require('joi');
export const VALIDATION_BIDDING_PREPARE_QUOTATION = {
    options: {allowUnknownBody: false},
    body: {
        [QuotationFields.fromId]: Joi.string().required(),
        [QuotationFields.toId]: Joi.string().required(),
        [QuotationFields.fromType]: Joi.string().required(),
        [QuotationFields.toType]: Joi.string().required(),
        [QuotationFields.contentLetter]: Joi.string(),
        [QuotationFields.timeSendBidding]: Joi.date(),
        [QuotationFields.note]: Joi.string(),
        [QuotationFields.currency]: Joi.string().valid('VND'),
        [QuotationFields.unitTime]: Joi.string().valid('DAY', 'HOUR', 'MONTH', 'YEAR'),
        [QuotationFields.projectId]: Joi.string().required(),
        [QuotationFields.documents]: Joi.array().items(Joi.object()),
        [QuotationFields.works]: Joi.array().items(Joi.object()),
        [QuotationFields.materials]: Joi.array().items(Joi.object()),
        [QuotationFields.alternativeBid]: Joi.object(),
        [QuotationFields.version]: Joi.number(),
    }
};
export const VALIDATION_BIDDING_UPDATE_DRAFT_QUOTATION = {
    options: {allowUnknownBody: false},
    body: {
        [QuotationFields.id]: Joi.string().required(),
        [QuotationFields.contentLetter]: Joi.string(),
        [QuotationFields.timeSendBidding]: Joi.date(),
        [QuotationFields.note]: Joi.string(),
        [QuotationFields.currency]: Joi.string().valid('VND'),
        [QuotationFields.unitTime]: Joi.string().valid('DAY', 'HOUR', 'MONTH', 'YEAR'),
        [QuotationFields.documents]: Joi.array().items(Joi.object()),
        [QuotationFields.works]: Joi.array().items(Joi.object()),
        [QuotationFields.materials]: Joi.array().items(Joi.object()),
        [QuotationFields.alternativeBid]: Joi.object(),
        [QuotationFields.version]: Joi.number(),
    }
};
export const VALIDATION_BIDDING_SEND_QUOTATION = {
    options: {allowUnknownBody: false},
    body: {
        [QuotationFields.fromId]: Joi.string().required(),
        [QuotationFields.toId]: Joi.string().required(),
        [QuotationFields.fromType]: Joi.string().required(),
        [QuotationFields.toType]: Joi.string().required(),
        [QuotationFields.contentLetter]: Joi.string(),
        [QuotationFields.timeSendBidding]: Joi.date(),
        [QuotationFields.note]: Joi.string(),
        [QuotationFields.currency]: Joi.string().valid('VND').required(),
        [QuotationFields.unitTime]: Joi.string().valid('DAY', 'HOUR', 'MONTH', 'YEAR').required(),
        [QuotationFields.projectId]: Joi.string().required(),
        [QuotationFields.documents]: Joi.array().items(Joi.object()),
        [QuotationFields.works]: Joi.array().items(Joi.object()).required(),
        [QuotationFields.materials]: Joi.array().items(Joi.object()),
        [QuotationFields.alternativeBid]: Joi.object(),
        [QuotationFields.version]: Joi.number(),
        [QuotationDraftFields.id]: Joi.string().required()//id of draf quotation
    }
};
export const VALIDATION_BIDDING_DELETE_QUOTATION = {
    options: {allowUnknownBody: false},
    body: {
        [QuotationDraftFields.id]: Joi.string().required()
    }
};

export const VALIDATION_BIDDING_UPDATE_QUOTATION_FEELING_STATUS = {
    options: {allowUnknownBody: false},
    body: {
        [QuotationFields.id]: Joi.string().required(),
        [QuotationFields.version]: Joi.number().required(),
        [QuotationFields.feelingStatus]: Joi.string().valid(null, 'LIKE', 'DISLIKE').required(),
    }
};
export const VALIDATION_BIDDING_UPDATE_QUOTATION_RESULT_STATUS = {
    options: {allowUnknownBody: false},
    body: {
        [QuotationFields.id]: Joi.string().required(),
        [QuotationFields.version]: Joi.number().required(),
        [QuotationFields.biddingStatus]: Joi.string().valid('ACCEPT').required(),
    }
};
export const VALIDATION_GET_DETAIL_QUOTATION = {
    options: {allowUnknownBody: false},
    body: {
        [QuotationFields.id]: Joi.string().required(),
    }
};
export const VALIDATION_GET_REVERSION_QUOTATION = {
    options: {allowUnknownBody: false},
    body: {
        [QuotationFields.id]: Joi.string().required(),
        page: Joi.number().min(1),
    }
};
export const VALIDATION_INVITE_BIDDING = {
    options: {allowUnknownBody: false},
    body: {
        [QuotationFields.fromId]: Validation.ID.required(),
        [QuotationFields.fromType]: Joi.string().valid(PROJECT).required(),
        [QuotationFields.toId]: Validation.ID.required(),
        [QuotationFields.toType]: Joi.string().valid(CONTRACTOR).required(),
        message: Joi.string().max(500).required(),
        messageType: Joi.string().required(),
    }
};

export const VALIDATION_LIST_QUOTATION_FROM = {
    options: {allowUnknownBody: false},
    body: {
        [QuotationFields.fromType]: Joi.string().valid(CONTRACTOR).required(),
        [QuotationFields.fromId]: Joi.string().required(),
        page: Joi.number().min(1)
    }
};
export const VALIDATION_LIST_QUOTATION_TO = {
    options: {allowUnknownBody: false},
    body: {
        [QuotationFields.toId]: Joi.string().required(),
        [QuotationFields.toType]: Joi.string().valid(BIDDING_CATEGORY).required(),
        page: Joi.number().min(1)
    }
};
export const VALIDATION_LIST_RECEIVED_QUOTATION = {
    options: {allowUnknownBody: false},
    body: {
        receiverUserId: Joi.string(),
        receiverContractorId: Joi.string(),
        page: Joi.number().min(1)
    }
};
export const VALIDATION_LIST_DRAFT_QUOTATION = {
    options: {allowUnknownBody: false},
    body: {
        [QuotationDraftFields.fromId]: Joi.string().required(),
        [QuotationDraftFields.toId]: Joi.string(),
        page: Joi.number().min(1)
    }
};