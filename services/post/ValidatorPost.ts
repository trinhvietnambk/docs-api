import {PostFields} from "../../databases/models/Post";
import {CONTRACTOR, PROJECT} from "../../constants/database/EntityName";

var Joi = require('joi');
export const VALIDATION_POST_CREATE = {
    options: {allowUnknownBody: false},
    body: {
        [PostFields.ownerId]: Joi.string().required(),
        [PostFields.ownerType]: Joi.string().valid(CONTRACTOR).required(),
        [PostFields.content]: Joi.string(),
        [PostFields.images]: Joi.array().items(Joi.string()),
        [PostFields.videos]: Joi.array().items(Joi.string()),
        [PostFields.audios]: Joi.array().items(Joi.string()),
        [PostFields.files]: Joi.array().items(Joi.object()),
        [PostFields.visibility]: Joi.number(),
    }
};
export const VALIDATION_POST_UPDATE = {
    options: {allowUnknownBody: false},
    body: {
        [PostFields.id]: Joi.string().required(),
        [PostFields.ownerId]: Joi.string().required(),
        [PostFields.content]: Joi.string(),
        [PostFields.images]: Joi.array().items(Joi.string()),
        [PostFields.videos]: Joi.array().items(Joi.string()),
        [PostFields.audios]: Joi.array().items(Joi.string()),
        [PostFields.files]: Joi.array().items(Joi.object()),
        [PostFields.visibility]: Joi.number(),
        'updatedAt': Joi.date(),
    }
};
export const VALIDATION_POST_DELETE = {
    options: {allowUnknownBody: false},
    body: {
        [PostFields.id]: Joi.string().required(),
        [PostFields.ownerId]: Joi.string().required(),
    }
};
export const VALIDATION_POST_LIST = {
    options: {allowUnknownBody: false},
    body: {
        [PostFields.ownerId]: Joi.string().required(),
        page: Joi.number()
    }
};
