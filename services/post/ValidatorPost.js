"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Post_1 = require("../../databases/models/Post");
const EntityName_1 = require("../../constants/database/EntityName");
var Joi = require('joi');
exports.VALIDATION_POST_CREATE = {
    options: { allowUnknownBody: false },
    body: {
        [Post_1.PostFields.ownerId]: Joi.string().required(),
        [Post_1.PostFields.ownerType]: Joi.string().valid(EntityName_1.CONTRACTOR).required(),
        [Post_1.PostFields.content]: Joi.string(),
        [Post_1.PostFields.images]: Joi.array().items(Joi.string()),
        [Post_1.PostFields.videos]: Joi.array().items(Joi.string()),
        [Post_1.PostFields.audios]: Joi.array().items(Joi.string()),
        [Post_1.PostFields.files]: Joi.array().items(Joi.object()),
        [Post_1.PostFields.visibility]: Joi.number(),
    }
};
exports.VALIDATION_POST_UPDATE = {
    options: { allowUnknownBody: false },
    body: {
        [Post_1.PostFields.id]: Joi.string().required(),
        [Post_1.PostFields.ownerId]: Joi.string().required(),
        [Post_1.PostFields.content]: Joi.string(),
        [Post_1.PostFields.images]: Joi.array().items(Joi.string()),
        [Post_1.PostFields.videos]: Joi.array().items(Joi.string()),
        [Post_1.PostFields.audios]: Joi.array().items(Joi.string()),
        [Post_1.PostFields.files]: Joi.array().items(Joi.object()),
        [Post_1.PostFields.visibility]: Joi.number(),
        'updatedAt': Joi.date(),
    }
};
exports.VALIDATION_POST_DELETE = {
    options: { allowUnknownBody: false },
    body: {
        [Post_1.PostFields.id]: Joi.string().required(),
        [Post_1.PostFields.ownerId]: Joi.string().required(),
    }
};
exports.VALIDATION_POST_LIST = {
    options: { allowUnknownBody: false },
    body: {
        [Post_1.PostFields.ownerId]: Joi.string().required(),
        page: Joi.number()
    }
};
//# sourceMappingURL=ValidatorPost.js.map