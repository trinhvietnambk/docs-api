import {COMMENT, POST} from "../../constants/database/EntityName";
import {USER_ACCOUNT} from "../../constants/database/Account";
import {CommentFields} from "../../databases/models/Comment";

var Joi = require('joi');

export const VALIDATION_COMMENT = {
    options: {allowUnknownBody: false},
    body: {
        [CommentFields.subjectId]: Joi.string().required(),
        [CommentFields.subjectType]: Joi.string().valid(USER_ACCOUNT).required(),
        [CommentFields.objectId]: Joi.string().required(),
        [CommentFields.objectType]: Joi.string().valid(COMMENT, POST).required(),
        [CommentFields.content]: Joi.string().required(),
        [CommentFields.audios]: Joi.array().items(Joi.string()),
        [CommentFields.files]: Joi.array().items(Joi.object()),
        [CommentFields.videos]: Joi.array().items(Joi.string()),
        [CommentFields.images]: Joi.array().items(Joi.string()),
    }
};
export const VALIDATION_UPDATE_COMMENT = {
    options: {allowUnknownBody: false},
    body: {
        [CommentFields.id]: Joi.string().required(),
        [CommentFields.content]: Joi.string().required(),
        [CommentFields.audios]: Joi.array().items(Joi.string()),
        [CommentFields.files]: Joi.array().items(Joi.object()),
        [CommentFields.videos]: Joi.array().items(Joi.string()),
        [CommentFields.images]: Joi.array().items(Joi.string()),
    }
};
export const VALIDATION_DELETE_COMMENT = {
    options: {allowUnknownBody: false},
    body: {
        [CommentFields.id]: Joi.string().required(),
    }
};
export const VALIDATION_LIST_COMMENTS = {
    options: {allowUnknownBody: false},
    body: {
        [CommentFields.objectId]: Joi.string().required(),
        page: Joi.number()
    }
};