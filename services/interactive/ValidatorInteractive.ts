import {ConnectFields} from "../../databases/models/Connect";

var Joi = require('joi');
export const VALIDATION_CONNECTS_CREATE = {
    options: {allowUnknownBody: false},
    body: {
        [ConnectFields.subjectId]: Joi.string().required(),
        [ConnectFields.subjectType]: Joi.string().required(),
        [ConnectFields.objectId]: Joi.string(),
        [ConnectFields.objectType]: Joi.string(),
        [ConnectFields.action]: Joi.string(),
    }
};
export const VALIDATION_CONNECTS_DELETE = {
    options: {allowUnknownBody: false},
    body: {
        [ConnectFields.subjectId]: Joi.string().required(),
        [ConnectFields.objectType]: Joi.string().required(),
        [ConnectFields.action]: Joi.string().required(),
    }
};
export const VALIDATION_CONNECTS_LIST_SUBJECTS = {
    options: {allowUnknownBody: false},
    body: {
        [ConnectFields.subjectType]: Joi.string().required(),
        [ConnectFields.objectId]: Joi.string().required()
    }
};
export const VALIDATION_CONNECTS_LIST_OBJECTS = {
    options: {allowUnknownBody: false},
    body: {
        [ConnectFields.subjectId]: Joi.string().required(),
        [ConnectFields.objectType]: Joi.string().required()
    }
};
export const VALIDATION_CONNECTS_COUNT_SUBJECTS = {
    options: {allowUnknownBody: false},
    body: {
        [ConnectFields.subjectType]: Joi.string().required(),
        [ConnectFields.objectId]: Joi.string().required()
    }
};
export const VALIDATION_CONNECTS_COUNT_OBJECTS = {
    options: {allowUnknownBody: false},
    body: {
        [ConnectFields.subjectId]: Joi.string().required(),
        [ConnectFields.objectType]: Joi.string().required()
    }
};
