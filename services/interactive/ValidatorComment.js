"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const EntityName_1 = require("../../constants/database/EntityName");
const Account_1 = require("../../constants/database/Account");
const Comment_1 = require("../../databases/models/Comment");
var Joi = require('joi');
exports.VALIDATION_COMMENT = {
    options: { allowUnknownBody: false },
    body: {
        [Comment_1.CommentFields.subjectId]: Joi.string().required(),
        [Comment_1.CommentFields.subjectType]: Joi.string().valid(Account_1.USER_ACCOUNT).required(),
        [Comment_1.CommentFields.objectId]: Joi.string().required(),
        [Comment_1.CommentFields.objectType]: Joi.string().valid(EntityName_1.COMMENT, EntityName_1.POST).required(),
        [Comment_1.CommentFields.content]: Joi.string().required(),
        [Comment_1.CommentFields.audios]: Joi.array().items(Joi.string()),
        [Comment_1.CommentFields.files]: Joi.array().items(Joi.object()),
        [Comment_1.CommentFields.videos]: Joi.array().items(Joi.string()),
        [Comment_1.CommentFields.images]: Joi.array().items(Joi.string()),
    }
};
exports.VALIDATION_UPDATE_COMMENT = {
    options: { allowUnknownBody: false },
    body: {
        [Comment_1.CommentFields.id]: Joi.string().required(),
        [Comment_1.CommentFields.content]: Joi.string().required(),
        [Comment_1.CommentFields.audios]: Joi.array().items(Joi.string()),
        [Comment_1.CommentFields.files]: Joi.array().items(Joi.object()),
        [Comment_1.CommentFields.videos]: Joi.array().items(Joi.string()),
        [Comment_1.CommentFields.images]: Joi.array().items(Joi.string()),
    }
};
exports.VALIDATION_DELETE_COMMENT = {
    options: { allowUnknownBody: false },
    body: {
        [Comment_1.CommentFields.id]: Joi.string().required(),
    }
};
exports.VALIDATION_LIST_COMMENTS = {
    options: { allowUnknownBody: false },
    body: {
        [Comment_1.CommentFields.objectId]: Joi.string().required(),
        page: Joi.number()
    }
};
//# sourceMappingURL=ValidatorComment.js.map