"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Connect_1 = require("../../databases/models/Connect");
var Joi = require('joi');
exports.VALIDATION_LIKE = {
    options: { allowUnknownBody: false },
    body: {
        [Connect_1.ConnectFields.subjectId]: Joi.string().required(),
        [Connect_1.ConnectFields.subjectType]: Joi.string().valid('user').required(),
        [Connect_1.ConnectFields.objectId]: Joi.string().required(),
        [Connect_1.ConnectFields.objectType]: Joi.string().valid('post').required(),
        [Connect_1.ConnectFields.action]: Joi.string().valid('LIKE').required(),
    }
};
//# sourceMappingURL=ValidatorLike.js.map