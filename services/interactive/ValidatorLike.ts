import {ConnectFields} from "../../databases/models/Connect";
import {CommentFields} from "../../databases/models/Comment";

var Joi = require('joi');
export const VALIDATION_LIKE = {
    options: {allowUnknownBody: false},
    body: {
        [ConnectFields.subjectId]: Joi.string().required(),
        [ConnectFields.subjectType]: Joi.string().valid('user').required(),
        [ConnectFields.objectId]: Joi.string().required(),
        [ConnectFields.objectType]: Joi.string().valid('post').required(),
        [ConnectFields.action]: Joi.string().valid('LIKE').required(),
    }
};