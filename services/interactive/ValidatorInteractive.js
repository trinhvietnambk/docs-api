"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Connect_1 = require("../../databases/models/Connect");
var Joi = require('joi');
exports.VALIDATION_CONNECTS_CREATE = {
    options: { allowUnknownBody: false },
    body: {
        [Connect_1.ConnectFields.subjectId]: Joi.string().required(),
        [Connect_1.ConnectFields.subjectType]: Joi.string().required(),
        [Connect_1.ConnectFields.objectId]: Joi.string(),
        [Connect_1.ConnectFields.objectType]: Joi.string(),
        [Connect_1.ConnectFields.action]: Joi.string(),
    }
};
exports.VALIDATION_CONNECTS_DELETE = {
    options: { allowUnknownBody: false },
    body: {
        [Connect_1.ConnectFields.subjectId]: Joi.string().required(),
        [Connect_1.ConnectFields.objectType]: Joi.string().required(),
        [Connect_1.ConnectFields.action]: Joi.string().required(),
    }
};
exports.VALIDATION_CONNECTS_LIST_SUBJECTS = {
    options: { allowUnknownBody: false },
    body: {
        [Connect_1.ConnectFields.subjectType]: Joi.string().required(),
        [Connect_1.ConnectFields.objectId]: Joi.string().required()
    }
};
exports.VALIDATION_CONNECTS_LIST_OBJECTS = {
    options: { allowUnknownBody: false },
    body: {
        [Connect_1.ConnectFields.subjectId]: Joi.string().required(),
        [Connect_1.ConnectFields.objectType]: Joi.string().required()
    }
};
exports.VALIDATION_CONNECTS_COUNT_SUBJECTS = {
    options: { allowUnknownBody: false },
    body: {
        [Connect_1.ConnectFields.subjectType]: Joi.string().required(),
        [Connect_1.ConnectFields.objectId]: Joi.string().required()
    }
};
exports.VALIDATION_CONNECTS_COUNT_OBJECTS = {
    options: { allowUnknownBody: false },
    body: {
        [Connect_1.ConnectFields.subjectId]: Joi.string().required(),
        [Connect_1.ConnectFields.objectType]: Joi.string().required()
    }
};
//# sourceMappingURL=ValidatorInteractive.js.map